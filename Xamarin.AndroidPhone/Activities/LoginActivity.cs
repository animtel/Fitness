using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace Xamarin.AndroidPhone.Activities
{
    [Activity(Label = "Login", MainLauncher = true)]
    public class LoginActivity : Activity
    {
        private Button _loginButton;
        private EditText _userName;
        private EditText _password;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            _loginButton = FindViewById<Button>(Resource.Id.button1);
            _userName = FindViewById<EditText>(Resource.Id.editText1);
            _password = FindViewById<EditText>(Resource.Id.editText2);

            _loginButton.Click += buttonLogin_Click;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            var alertBuilder = new AlertDialog.Builder(this);

            alertBuilder.SetTitle("Login status");
            alertBuilder.SetMessage($"User {_userName.Text} was successfully loged in");

            /*alertBuilder.SetPositiveButton("Cancel", (senderAlert, args) => {
                Toast.MakeText(this, "Welcome!", ToastLength.Short).Show();
            });*/

            Dialog dialog = alertBuilder.Create();
            dialog.Show();

            var intent = new Intent(this, typeof(HomeActivity));
            intent.PutExtra("UserName", _userName.Text);
            StartActivity(intent);
        }
    }
}