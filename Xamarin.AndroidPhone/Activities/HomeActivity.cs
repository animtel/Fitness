using Android.App;
using Android.OS;
using Android.Widget;

namespace Xamarin.AndroidPhone.Activities
{
    [Activity(Label = "Home")]
    public class HomeActivity : Activity
    {
        private TextView _welcomeTextView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Home);

            _welcomeTextView = FindViewById<TextView>(Resource.Id.welcomeText);

            _welcomeTextView.Text = $"Welcome {Intent.GetStringExtra("UserName")}!";
        }
    }
}