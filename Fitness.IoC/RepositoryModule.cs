﻿using Autofac;
using Fitness.DAL;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.DAL.Persistance.Repository;
using Fitness.Data.Persistance.Repository;
using Fitness.Entities;
using Fitness.Data.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Infrastructure.Repository;
using Microsoft.Owin;

namespace Fitness.IoC
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<Context>()
                .AsSelf()
                .InstancePerRequest();
            builder.Register<Microsoft.Owin.Security.IAuthenticationManager>((c, p) => c.Resolve<IOwinContext>()
    .Authentication).InstancePerRequest();
            builder.RegisterModule<UnitOfWorkModule>();

            builder.RegisterType<HealthyFoodBlockRepository>()
                .As<IHealthyFoodBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<HealthyFoodRepository>()
                .As<IHealthyFoodRepository>()
                .InstancePerRequest();

            builder.RegisterType<IngredientRepository>()
                .As<IIngredientRepository>()
                .InstancePerRequest();

            builder.RegisterType<IngredientDescriptionRepository>()
                .As<IIngredientDescriptionRepository>()
                .InstancePerRequest();

            builder.RegisterType<MineralRepository>()
                .As<IMineralRepository>()
                .InstancePerRequest();

            builder.RegisterType<VitaminRepository>()
                .As<IVitaminRepository>()
                .InstancePerRequest();

            builder.RegisterType<NutritionMealCategoryRepository>()
                .As<INutritionMealBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<NutritionMealRepository>()
                .As<INutritionMealRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutCategoryRepository>()
                .As<IWorkoutBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutTypeRepository>()
                .As<IWorkoutTypeRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutRepository>()
                .As<IWorkoutRepository>()
                .InstancePerRequest();

            builder.RegisterType<ErrorRepository>()
                .As<IErrorRepository>()
                .InstancePerRequest();


            builder.RegisterType<MeasureRepository>()
                .As<IMeasureRepository>()
                .InstancePerRequest();

            builder.RegisterType<GenerateNutritionMealRepository>()
                .As<IGenerateNutritionMealRepository>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerRequest();

            builder.RegisterType<ShoppingRepository>()
                .As<IShoppingRepository>()
                .InstancePerRequest();

            builder.RegisterType<ShoppingNutritionRepository>()
                .As<IShoppingNutritionRepository>()
                .InstancePerRequest();

            builder.RegisterType<FitnessProgressRepository>()
                .As<IFitnessProgressRepository>()
                .InstancePerRequest();

            //builder.RegisterType<PickmealsRepository>()
            //    .As<IPickmealsRepository>()
            //    .InstancePerRequest();


            //builder.RegisterInstance<AppUserManager>(new AppUserManager(new AppUserStore(context)))
            //    .As<AppUserManager>();

            //builder.RegisterInstance<AppRoleManager>(new AppRoleManager(new AppRoleStore(context)))
            //    .As<AppRoleManager>();


            //builder.RegisterType<AppUserManager>()
            //    .AsSelf()
            //    .InstancePerRequest();


            //builder.RegisterType<AppRoleManager>()
            //    .AsSelf()
            //    .InstancePerRequest();


            //builder.RegisterType<AppUserStore>()
            //    .AsSelf()

            //    .InstancePerRequest();

            //builder.RegisterType<AppRoleStore>()
            //    .AsSelf()
            //    .InstancePerRequest();
        }
    }
}
