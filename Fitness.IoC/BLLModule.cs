﻿using Autofac;
using Fitness.BLL.Infrastructure;
using Fitness.BLL.Interfaces;
using Fitness.BLL.Services;
using Fitness.Entities;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.DataProtection;


namespace Fitness.IoC
{

    public class BBLModule : Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<RepositoryModule>();


            builder.RegisterType<HealthyFoodService>()
                .As<IHealthyFoodService>()
                .InstancePerRequest();

            builder.RegisterType<ErrorService>()
                .As<IErrorService>()
                .InstancePerRequest();

            builder.RegisterType<ShoppingService>()
                .As<IShoppingService>()
                .InstancePerRequest();

            builder.RegisterType<AccountService>()
                .As<IAccountService>()
                .InstancePerRequest();

            builder.RegisterType<IngredientService>()
                .As<IIngredientService>()
                .InstancePerRequest();

            builder.RegisterType<TicketDataFormat>()
                .As<ISecureDataFormat<AuthenticationTicket>>()
                .InstancePerRequest();

            builder.RegisterType<TicketSerializer>()
                .As<IDataSerializer<AuthenticationTicket>>();

            builder.RegisterType<NutritionService>()
                .As<INutritionService>()
                .InstancePerRequest();

            builder.RegisterType<MeasureService>()
                .As<IMeasureService>().InstancePerRequest();

            builder.RegisterType<GenerateNutritionMealService>()
                .As<IGenerateNutritionMealService>().InstancePerRequest();

            builder.RegisterType<FitnessProgressService>()
                .As<IFitnessProgressService>().InstancePerRequest();

            builder.RegisterType<WorkoutService>().As<IWorkoutService>().InstancePerRequest(); ;
            builder.RegisterType<WorkoutTypeService>().As<IWorkoutTypeService>().InstancePerRequest(); ;
            builder.RegisterType<WorkoutCategoryService>().As<IWorkoutCategoryService>().InstancePerRequest(); ;

            builder.Register(c => new DpapiDataProtectionProvider().Create("ASP.NET Identity"))
                .As<IDataProtector>();
        }
    }
}
