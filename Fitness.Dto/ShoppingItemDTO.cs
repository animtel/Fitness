﻿using Fitness.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DTO
{
    public class ShoppingItemDTO : DTO
    {
        public Guid IngredientId { get; set; }
        public IngredientDTO Ingredient { get; set; }
        public int QuantitativeValue { get; set; }
        public int Count { get; set; }
        public string FitnessUserId { get; set; }
    }
}
