﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DTO
{
    public class HealthyFoodDTO : DTO
    {
        public HealthyFoodDTO() : base()
        {}
        public string CreditTo { get; set; }
        public bool Premium { get; set; }
        public string Name { get; set; }
        public int Yield { get; set; }
        public int PrepTime { get; set; }
        public int CookTime { get; set; }
        public string ImageName { get; set; }

        public string HealthyFoodDirections { get; set; }
        public IEnumerable<IngredientDescriptionDTO> IngredientDescriptions { get; set; }

        public string MealCategory { get; set; }

        public Guid HealthyFoodCategoryId { get; set; }
    }
}
