﻿using System;
using System.Collections.Generic;
using Fitness.Entities;

namespace Fitness.BLL.DTO.Identity
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        
        public decimal Height { get; set; }

        public HeightMeasureUnit HeightMeasuareUnit { get; set; }

        public decimal Weight { get; set; }

        public WeightMeasureUnit WeightMeasuareUnit { get; set; }

        public Sex Sex { get; set; }

        public Goal Goal { get; set; }

        public Activity ActivityLvl { get; set; }

        public ICollection<ShoppingItem> ShoppingItems { get; set; }

        //public ICollection<Pickmeals> Pickmeals { get; set; }
    }
}