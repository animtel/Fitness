﻿using System;
using Fitness.Entities;

namespace Fitness.BLL.DTO.Identity
{
    public class RegisterDTO
    {
        public string Password { get; set; }
        
        public string Email { get; set; }

        public AppRole Role { get; set; }
    }
}