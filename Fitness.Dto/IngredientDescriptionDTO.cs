﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DTO
{
    public class IngredientDescriptionDTO : DTO
    {
        public Guid HealthyFoodId { get; set; }
        public IngredientDTO Ingredient { get; set; }
        public Guid? MeasureId { get; set; }
        public string MeasureTypeabbreviation { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }
}
