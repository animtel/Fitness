﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities.Measure
{
    public class Measure:Entity
    {
        public Measure()
        {
            Id = Guid.NewGuid();
        }
        public string MeasureName { get; set; }
        public double EqualsGram { get; set; }
        public string Abbreviation { get; set; }

    }
}
