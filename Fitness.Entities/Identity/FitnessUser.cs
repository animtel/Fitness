﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class FitnessUser : IdentityUser
    {
        //public FitnessUser()
        //{
        //    this.ShoppingItems = new List<ShoppingItem>();
        //}

        public int Age { get; set; }

        public double Height { get; set; }

        public HeightMeasureUnit HeightMeasuareUnit { get; set; }

        public double Weight { get; set; }

        public WeightMeasureUnit WeightMeasuareUnit { get; set; }

        public Sex Sex { get; set; }

        public Goal Goal { get; set; }

        public Activity ActivityLvl { get; set; }

        public int WeekExpiresMealPlan { get; set; }

        public ICollection<ShoppingItem> ShoppingItems { get; set; }

        public ICollection<ShoppingNutritionItem> ShoppingNutritionItems { get; set; }

        public ICollection<NutritionMeal> Pickmeals { get; set; }

        public ICollection<GenerateNutritionMeal> GenerateNutritionMeals { get; set; }

        public ICollection<FitnessProgressWeek> UsersProgressWeekly { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<FitnessUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            return userIdentity;
        }
    }
}
