﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Entity : IEntity
    {
        public Guid Id { get; set; }

        public Entity()
        {
        }

        public Entity(Guid id)
        {
            this.Id = id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != GetType())
                return false;

            if (this == obj)
                return true;

            var otherEntity = (Entity)obj;

            return otherEntity.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
