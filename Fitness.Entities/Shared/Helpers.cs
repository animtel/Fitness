﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public enum Sex
    {
        Male,
        Female
    }

    public enum HeightMeasureUnit
    {
        FT, M
    }

    public enum WeightMeasureUnit
    {
        LB, KG
    }

    public enum Activity
    {
        Little,
        Light,
        Moderate,
        Hard,
    }

    public enum Goal
    {
        Loss,
        Maintenanse,
        Gain
    }
    public enum MeasureType
    {
        Weight = 0, Quantity
    }
}
