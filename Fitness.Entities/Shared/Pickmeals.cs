﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Pickmeals : Entity
    {
        public Guid NutritionMealId { get; set; }
        public NutritionMeal NutritionMeal { get; set; }
        public string UserId { get; set; }
        public FitnessUser User { get; set; }
    }
}
