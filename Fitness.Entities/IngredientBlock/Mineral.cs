﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Mineral : Entity
    {
        public Mineral() : base() {}
        public string Name { get; set; }

        public Ingredient Ingredient { get; set; }
    }
}
