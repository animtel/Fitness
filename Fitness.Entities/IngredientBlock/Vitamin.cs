﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Vitamin : Entity
    {
        public Vitamin(): base() {}

        public string Name { get; set; }

        public decimal Weight { get; set; }

        public Ingredient Ingredient { get; set; }
    }
}
