﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Ingredient : Entity
    {
        public string Name { get; set; }
        public decimal Calories { get; set; }

        #region Characteristics of ingredient
        public decimal Fats { get; set; }
        public decimal Polyunsaturatedfattyacids { get; set; }
        public decimal Monounsaturatedfattyacids { get; set; }
        public decimal SaturatedFat { get; set; }

        //mg Натрий
        public decimal Sodium { get; set; }

        //mg Калий
        public decimal Potassium { get; set; }
        public decimal Carbohydrates { get; set; }

        //g Пищевые волокна
        public decimal Dietaryfiber { get; set; }
        public decimal Sugar { get; set; }

        public decimal Cholesterol { get; set; }

        public decimal Proteins { get; set; }
        #endregion

        #region Foregin Keys

        public ICollection<Vitamin> Vitamins { get; set; }

        public ICollection<Mineral> Minerals { get; set; }

        #endregion

        public decimal VitaminsWeight
        {
            get
            {
                decimal sum = 0;
                if (Vitamins == null)
                {
                    return 0;
                }
                if (Vitamins.Any())
                {
                    sum = Vitamins.Select(x => x.Weight).Sum();
                };
                return sum;
            }

        }

    }
}

