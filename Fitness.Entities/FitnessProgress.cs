﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class FitnessProgressWeek : Entity
    {
        public string FitnessUserId { get; set; }
        public FitnessUser FitnessUser { get; set; }

        public string Title { get; set; }
        public double WeekNumber { get; set; }

        public DateTime Date { get; set; }
        public string UrlImage { get; set; }

    }
}
