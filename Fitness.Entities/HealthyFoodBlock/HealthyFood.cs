﻿using Fitness.Entities.HealthyFoodBlock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class HealthyFood : Entity
    {

        public string CreditTo { get; set; }

        public bool Premium { get; set; }

        public string Name { get; set; }

        public int Yield { get; set; }

        public string ImageName { get; set; }

        public int PrepTime { get; set; }

        public int CookTime { get; set; }

        public ICollection<IngredientDescription> IngredientDescriptions { get; set; }
        public string HealthyFoodDirections { get; set; }

        public Guid HealthyFoodCategoryId { get; set; }
        public HealthyFoodCategory HealthyFoodCategory { get; set; }
    }
}
