﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class IngredientDescription : Entity
    {
        public virtual Ingredient Ingredient { get; set; }
        public virtual Guid IngredientId { get; set; }
        public string MeasureType { get; set; }
        public int Count { get; set; }
    }
}
