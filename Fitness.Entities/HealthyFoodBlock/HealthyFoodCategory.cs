﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class HealthyFoodCategory : Entity
    {
        public string Name { get; set; }
        public string Image { get; set; }

        public ICollection<HealthyFood> HealthyFoods { get; set; }
    }
}
