﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities.HealthyFoodBlock
{
    public class ReducedIngredient : Entity
    {
        public int Count { get; set; }
        public string MeasureType { get; set; }
        public string Name { get; set; }
    }
}
