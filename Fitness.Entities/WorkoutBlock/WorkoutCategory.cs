﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class WorkoutCategory : Entity
    {
        public string Name { get; set; }
        public string PreviewImageName { get; set; }
        public string ScreenImageName { get; set; }
        public ICollection<Workout> Workouts { get; set; }
        public ICollection<WorkoutType> WorkoutTypes { get; set; }
    }
}
