﻿using System;
using System.Collections.Generic;

namespace Fitness.Entities
{
    public class WorkoutType : Entity
    {
        public WorkoutType() : base()
        { }

        public string Type { get; set; }

        public string Image { get; set; }

        public ICollection<WorkoutCategory> WorkoutCategories{ get; set; }

        public ICollection<Workout> Workouts { get; set; }
    }
}