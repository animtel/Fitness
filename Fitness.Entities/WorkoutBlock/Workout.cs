﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Workout: Entity
    {

        public string Name { get; set; }
        public string StatusWorkout { get; set; }
        public ICollection<Exercise> Exercises { get; set; }

        public TimeSpan WorkoutTimeAbc { get; set; }
        public string WorkoutDescription { get; set; }
        public bool Premium { get; set; }

        public Guid WorkoutTypeId { get; set; }
        public WorkoutType WorkoutType { get; set; }
    }
}
