﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Exercise: Entity
    {
        public string ExerciseName { get; set; }
        public string YouTubeLink { get; set; }
        public ICollection<DifficultyLevel> DifficultyLevels { get; set; }
    }
}
