﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class DifficultyLevel: Entity
    {
        public string DifficultName { get; set; }
        public int Sets { get; set; }
        public string Repo { get; set; }
        public string Rest { get; set; }
        public string Temp { get; set; }
    }
}
