﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class NutritionMeal : Entity
    {
        public string CreditTo { get; set; }

        public string Directions { get; set; }
        public bool Premium { get; set; }

        public string Name { get; set; }

        public int Yield { get; set; }

        public string ImageName { get; set; }

        public int PrepTime { get; set; }

        public int CookTime { get; set; }

        public ICollection<NutritionIngredientDescription> NutritionIngredientDescriptions { get; set; }

        public Guid NutritionMealCategoryId { get; set; }
        public NutritionMealCategory NutritionMealCategory { get; set; }

        public ICollection<FitnessUser> UsersLike { get; set; }

        //public ICollection<GeneratedNutritionMeal.GeneratedNutritionMeal> GeneratedNutritionMeals { get; set; }


    }

    public class GenerateNutritionMeal : Entity
    {
        public double Calories { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public string FitnessUserId { get; set; }
        public FitnessUser FitnessUser { get; set; }

        public string CreditTo { get; set; }



        public bool Premium { get; set; }

        public string Name { get; set; }

        public int Yield { get; set; }

        public string ImageName { get; set; }

        public int PrepTime { get; set; }

        public int CookTime { get; set; }


        public bool Eat { get; set; }

        public int NutritionMealCategory { get; set; }

        public Guid NutritionMealId { get; set; }

        public ICollection<CustomNutritionIngredientDescription> CustomNutritionIngredientDescriptions { get; set; }

        //public ICollection<FitnessUser> GenerateForUsers { get; set; }
    }
}
