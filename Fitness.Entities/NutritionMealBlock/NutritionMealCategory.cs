﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class  NutritionMealCategory : Entity
    {
        public string Name { get; set; }
        public string ImageName { get; set; }
        public ICollection<NutritionMeal> NutritionMeals { get; set; }
    }
}
