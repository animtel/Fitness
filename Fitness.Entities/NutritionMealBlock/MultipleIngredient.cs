﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class IngredientDescription : Entity
    {
        public IngredientDescription(Guid newGuid)
        {
            this.Id = Guid.NewGuid();
        }
        public IngredientDescription():base()
        {
        }
        public Guid IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }

        public Guid HealthyFoodId { get; set; }
        public HealthyFood HealthyFood { get; set; }

        public Guid? MeasureId { get; set; }
        public Measure.Measure Measure { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }


    public class NutritionIngredientDescription : Entity
    {

        public Guid IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }

        public Guid? NutritionMealId { get; set; }
        public NutritionMeal NutritionMeal { get; set; }


        public Guid? MeasureId { get; set; }
        public Measure.Measure Measure { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }



    public class CustomNutritionIngredientDescription : Entity
    {
        public Guid IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }

        public Guid GenerateNutritionMealId { get; set; }
        public GenerateNutritionMeal GenerateNutritionMeal { get; set; }

        public Guid? MeasureId { get; set; }
        public Measure.Measure Measure { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }
}
