﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{

    /// <summary>
    /// Что предпочитает пользователь один итем
    /// </summary>
    public class ShoppingItem : Entity
    {

        public Guid IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }


        public int Count {get;set;}
        public int QuantitativeValue { get; set; }


        public string FitnessUserId { get; set; }
        public FitnessUser FitnessUser { get; set; }
    }

    public class ShoppingNutritionItem : Entity
    {
        public Guid IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }


        public int Count { get; set; }
        public int QuantitativeValue { get; set; }


        public string FitnessUserId { get; set; }
        public FitnessUser FitnessUser { get; set; }
    }
}
