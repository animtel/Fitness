﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    /// <summary>
    /// List for purchasing in shop
    /// </summary>
    public class ShoppingList : Entity
    {
        public FitnessUser FitnessUser { get; set; }

        public ICollection<ShoppingItem> ShoppingItems { get; set; }
    }
}
