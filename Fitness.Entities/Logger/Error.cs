﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Entities
{
    public class Error : Entity
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
