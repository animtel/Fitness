﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Fitness.DAL;
using Fitness.DAL.Infrastructure;

namespace Fitness.Data.Modules
{
    internal class UnitOfWorkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Context>()
                .AsSelf()
                .InstancePerRequest();

            builder.RegisterType<DbFactory>()
                .AsSelf()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();
        }
    }
}
