﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Fitness.Data.Infrastructure.Repository;
using Fitness.Data.Persistance.Repository;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.DAL.Persistance.Repository;

namespace Fitness.Data.Modules
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DbFactory>()
                .AsSelf()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterType<HealthyFoodBlockRepository>()
                .As<IHealthyFoodBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<HealthyFoodRepository>()
                .As<IHealthyFoodRepository>()
                .InstancePerRequest();

            builder.RegisterType<IngredientRepository>()
                .As<IIngredientRepository>()
                .InstancePerRequest();

            builder.RegisterType<IngredientDescriptionRepository>()
                .As<IIngredientDescriptionRepository>()
                .InstancePerRequest();

            builder.RegisterType<MineralRepository>()
                .As<IMineralRepository>()
                .InstancePerRequest();

            builder.RegisterType<VitaminRepository>()
                .As<IVitaminRepository>()
                .InstancePerRequest();

            builder.RegisterType<NutritionMealBlockRepository>()
                .As<INutritionMealBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<NutritionMealRepository>()
                .As<INutritionMealRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutBlockRepository>()
                .As<IWorkoutBlockRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutTypeRepository>()
                .As<IWorkoutTypeRepository>()
                .InstancePerRequest();

            builder.RegisterType<WorkoutRepository>()
                .As<IWorkoutRepository>()
                .InstancePerRequest();

            builder.RegisterType<ErrorRepository>()
                .As<IErrorRepository>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerRequest();

            builder.RegisterType<ShoppingRepository>()
                .As<IShoppingRepository>()
                .InstancePerRequest();

            builder.RegisterType<FavoriteRecipesRepository>()
                .As<IFavoriteRecipesRepository>()
                .InstancePerRequest();

            builder.RegisterModule<UnitOfWorkModule>();
        }
    }
}
