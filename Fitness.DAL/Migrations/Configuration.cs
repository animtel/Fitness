using Fitness.Entities.HealthyFoodBlock;

namespace Fitness.DAL.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Fitness.DAL.Context>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Fitness.DAL.Context context)
        {
            context.HealthyFoodCategories.AddOrUpdate(p => p.Name, new HealthyFoodCategory[] {

                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Breakfast"},
                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Lunch" },
                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Dinner"},
                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Snacks"},
                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Family"},
                new HealthyFoodCategory(Guid.NewGuid()) { Name = "Friendly Meals"},
            });
            
            //context.Vitamins.AddOrUpdate(p => p.Name, new Entities.Vitamin[] {
            //                        new Entities.Vitamin { Name = "A"},
            //                         new Entities.Vitamin { Name = "D"},
            //                          new Entities.Vitamin { Name = "B12"},
            //                           new Entities.Vitamin { Name = "C"},
            //                            new Entities.Vitamin { Name = "B6"}
            //});
            //context.Minerals.AddOrUpdate(p => p.Name, new Entities.Mineral[] {

            //    new Entities.Mineral { Name = "Calcium" },
            //    new Entities.Mineral { Name = "Iron" },
            //    new Entities.Mineral { Name = "Magnesium" },
            //    new Entities.Mineral { Name = "Phosphorus" },
            //    new Entities.Mineral { Name = "Potassium" },
            //    new Entities.Mineral { Name = "Sodium" },
            //    new Entities.Mineral { Name = "Zinc" },
            //    new Entities.Mineral { Name = "Copper" },
            //    new Entities.Mineral { Name = "Manganese" },
            //    new Entities.Mineral { Name = "Selenium" },
            //    new Entities.Mineral { Name = "Fluoride" },
            //});

            //context.Ingredients.AddOrUpdate(p => p.Name, new Entities.Ingredient[] {
            //    new Entities.Ingredient {
            //        Name = "Blackberry",
            //        Calories = 43,
            //        Sugar = 4.9M,
            //        Fats = 0.5M,
            //        Sodium = 1,
            //        Potassium = 162,
            //        Proteins = 1.4M,
            //        Monounsaturatedfattyacids = 0,
            //        Polyunsaturatedfattyacids = 0.3M,
            //        Cholesterol = 0,
            //        SaturatedFat = 0,
            //        Carbohydrates = 0,
            //        Dietaryfiber = 5
            //        },
            //    new Entities.Ingredient {
            //        Name = "Banana",
            //        Calories = 89,
            //        Sugar = 12.0M,
            //        Fats = 0.3M,
            //        Monounsaturatedfattyacids = 0,
            //        Polyunsaturatedfattyacids = 0,
            //        SaturatedFat = 0.1M,
            //        Sodium = 1,
            //        Cholesterol = 0,
            //        Potassium = 358,
            //        Proteins = 1.1M,
            //        Carbohydrates = 0,
            //        Dietaryfiber = 2.6M
            //        },
            //    new Entities.Ingredient {
            //        Name = "Peanut",
            //        Calories = 565,
            //        Sugar = 12.0M,
            //        Fats = 49.1M,
            //        Monounsaturatedfattyacids = 24.4M,
            //        Polyunsaturatedfattyacids = 15.5M,
            //        SaturatedFat = 6.7M,
            //        Sodium = 17.7M,
            //        Cholesterol = 0,
            //        Potassium = 358,
            //        Proteins = 25.8M,
            //        Carbohydrates = 0,
            //        Dietaryfiber = 8.5M
            //        },
            //                        new Entities.Ingredient {
            //                Name = "Oatmeal",
            //                Calories = 70.9M,
            //                Sugar = 12.0M,
            //                Fats = 1.5M,
            //                Monounsaturatedfattyacids = 0.4M,
            //                Polyunsaturatedfattyacids = 0.6M,
            //                SaturatedFat = 0.3M,
            //                Sodium = 3.8M,
            //                Cholesterol = 0,
            //                Potassium = 0,
            //                Proteins = 2.5M,
            //                Carbohydrates = 0,
            //                Dietaryfiber = 1.7M
            //            },

            //});


            //context.MealCategories.AddOrUpdate(p=>p.Name, new Entities.NutritionMealCategory[] {

            //    new Entities.NutritionMealCategory { Name = "Breakfast"},
            //    new Entities.NutritionMealCategory { Name = "Lunch"},
            //    new Entities.NutritionMealCategory { Name = "Dinner"},
            //    new Entities.NutritionMealCategory { Name = "Snacks"},
            //    new Entities.NutritionMealCategory { Name = "Family"},
            //    new Entities.NutritionMealCategory { Name = "Friendly Meals"},
            //});
            //context.Categories.AddOrUpdate(p => p.Name, new Entities.WorkCategory[] {

            //    new Entities.WorkCategory { Name = "Gym Workout"},
            //    new Entities.WorkCategory { Name = "Home workout"},
            //    new Entities.WorkCategory { Name = "Sport Specific Training"}
            //});

            //context.WorkoutTypes.AddOrUpdate(p => p.Type, new Entities.WorkoutType[] {

            //    new Entities.WorkoutType{ Type = "Abs"},
            //    new Entities.WorkoutType{ Type = "Upper body"},
            //    new Entities.WorkoutType{ Type = "Lower body"}

            //});



            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
