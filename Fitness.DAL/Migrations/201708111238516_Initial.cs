namespace Fitness.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Errors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Message = c.String(),
                        StackTrace = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HealthyFoods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Yield = c.Int(nullable: false),
                        ImageName = c.String(),
                        PrepTime = c.Int(nullable: false),
                        CookTime = c.Int(nullable: false),
                        HealthyFoodCategory_Id = c.Guid(),
                        ShoppingList_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HealthyFoodCategories", t => t.HealthyFoodCategory_Id)
                .ForeignKey("dbo.ShoppingLists", t => t.ShoppingList_Id)
                .Index(t => t.HealthyFoodCategory_Id)
                .Index(t => t.ShoppingList_Id);
            
            CreateTable(
                "dbo.Directions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        HealthyFood_Id = c.Guid(),
                        NutritionMeal_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HealthyFoods", t => t.HealthyFood_Id)
                .ForeignKey("dbo.NutritionMeals", t => t.NutritionMeal_Id)
                .Index(t => t.HealthyFood_Id)
                .Index(t => t.NutritionMeal_Id);
            
            CreateTable(
                "dbo.HealthyFoodCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReducedIngredients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Count = c.Int(nullable: false),
                        MeasureType = c.String(),
                        HealthyFood_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HealthyFoods", t => t.HealthyFood_Id)
                .Index(t => t.HealthyFood_Id);
            
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fats = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Polyunsaturatedfattyacids = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Monounsaturatedfattyacids = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sodium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Potassium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Carbohydrates = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Dietaryfiber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sugar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cholesterol = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Proteins = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Minerals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Ingredient_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id)
                .Index(t => t.Ingredient_Id);
            
            CreateTable(
                "dbo.Vitamins",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ingredient_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id)
                .Index(t => t.Ingredient_Id);
            
            CreateTable(
                "dbo.NutritionMealCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NutritionMeals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Yield = c.Int(nullable: false),
                        ImageName = c.String(),
                        PrepTime = c.Int(nullable: false),
                        CookTime = c.Int(nullable: false),
                        NutritionMealCategory_Id = c.Guid(),
                        ShoppingList_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NutritionMealCategories", t => t.NutritionMealCategory_Id)
                .ForeignKey("dbo.ShoppingLists", t => t.ShoppingList_Id)
                .Index(t => t.NutritionMealCategory_Id)
                .Index(t => t.ShoppingList_Id);
            
            CreateTable(
                "dbo.IngredientDescriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        MeasureType = c.String(),
                        Count = c.Int(nullable: false),
                        NutritionMeal_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.NutritionMeals", t => t.NutritionMeal_Id)
                .Index(t => t.IngredientId)
                .Index(t => t.NutritionMeal_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Age = c.Int(nullable: false),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HeightMeasuareUnit = c.Int(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WeightMeasuareUnit = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                        Goal = c.Int(nullable: false),
                        ActivityLvl = c.Int(nullable: false),
                        ShoppingListId = c.Guid(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FavoriteRecipes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        HealthyFoodId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NutritionMeals", t => t.HealthyFoodId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.HealthyFoodId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ShoppingLists",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FitnessUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUser_Id)
                .Index(t => t.FitnessUser_Id);
            
            CreateTable(
                "dbo.ShoppingItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Purchased = c.Boolean(nullable: false),
                        Ingredient_Id = c.Guid(),
                        ShoppingList_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IngredientDescriptions", t => t.Ingredient_Id)
                .ForeignKey("dbo.ShoppingLists", t => t.ShoppingList_Id)
                .Index(t => t.Ingredient_Id)
                .Index(t => t.ShoppingList_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShoppingLists", "FitnessUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppingItems", "ShoppingList_Id", "dbo.ShoppingLists");
            DropForeignKey("dbo.ShoppingItems", "Ingredient_Id", "dbo.IngredientDescriptions");
            DropForeignKey("dbo.NutritionMeals", "ShoppingList_Id", "dbo.ShoppingLists");
            DropForeignKey("dbo.HealthyFoods", "ShoppingList_Id", "dbo.ShoppingLists");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavoriteRecipes", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavoriteRecipes", "HealthyFoodId", "dbo.NutritionMeals");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.NutritionMeals", "NutritionMealCategory_Id", "dbo.NutritionMealCategories");
            DropForeignKey("dbo.IngredientDescriptions", "NutritionMeal_Id", "dbo.NutritionMeals");
            DropForeignKey("dbo.IngredientDescriptions", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.Directions", "NutritionMeal_Id", "dbo.NutritionMeals");
            DropForeignKey("dbo.Vitamins", "Ingredient_Id", "dbo.Ingredients");
            DropForeignKey("dbo.Minerals", "Ingredient_Id", "dbo.Ingredients");
            DropForeignKey("dbo.ReducedIngredients", "HealthyFood_Id", "dbo.HealthyFoods");
            DropForeignKey("dbo.HealthyFoods", "HealthyFoodCategory_Id", "dbo.HealthyFoodCategories");
            DropForeignKey("dbo.Directions", "HealthyFood_Id", "dbo.HealthyFoods");
            DropIndex("dbo.ShoppingItems", new[] { "ShoppingList_Id" });
            DropIndex("dbo.ShoppingItems", new[] { "Ingredient_Id" });
            DropIndex("dbo.ShoppingLists", new[] { "FitnessUser_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.FavoriteRecipes", new[] { "User_Id" });
            DropIndex("dbo.FavoriteRecipes", new[] { "HealthyFoodId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.IngredientDescriptions", new[] { "NutritionMeal_Id" });
            DropIndex("dbo.IngredientDescriptions", new[] { "IngredientId" });
            DropIndex("dbo.NutritionMeals", new[] { "ShoppingList_Id" });
            DropIndex("dbo.NutritionMeals", new[] { "NutritionMealCategory_Id" });
            DropIndex("dbo.Vitamins", new[] { "Ingredient_Id" });
            DropIndex("dbo.Minerals", new[] { "Ingredient_Id" });
            DropIndex("dbo.ReducedIngredients", new[] { "HealthyFood_Id" });
            DropIndex("dbo.Directions", new[] { "NutritionMeal_Id" });
            DropIndex("dbo.Directions", new[] { "HealthyFood_Id" });
            DropIndex("dbo.HealthyFoods", new[] { "ShoppingList_Id" });
            DropIndex("dbo.HealthyFoods", new[] { "HealthyFoodCategory_Id" });
            DropTable("dbo.ShoppingItems");
            DropTable("dbo.ShoppingLists");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.FavoriteRecipes");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.IngredientDescriptions");
            DropTable("dbo.NutritionMeals");
            DropTable("dbo.NutritionMealCategories");
            DropTable("dbo.Vitamins");
            DropTable("dbo.Minerals");
            DropTable("dbo.Ingredients");
            DropTable("dbo.ReducedIngredients");
            DropTable("dbo.HealthyFoodCategories");
            DropTable("dbo.Directions");
            DropTable("dbo.HealthyFoods");
            DropTable("dbo.Errors");
        }
    }
}
