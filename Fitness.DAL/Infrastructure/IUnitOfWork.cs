﻿using Fitness.DAL.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Infrastructure.Repository;

namespace Fitness.DAL.Infrastructure
{
    public interface IUnitOfWork
    {
        IHealthyFoodBlockRepository HealthyFoodBlockRepository { get; set; }
        INutritionMealBlockRepository NutrtionBlockRepository { get; set; }

        IHealthyFoodRepository HealthyFoodRepository { get; set; }
        INutritionMealRepository NutritionMealRepository { get; set; }

        IIngredientRepository IngredientRepository { get; set; }
        IIngredientDescriptionRepository IngredientDescriptionRepository { get; set; }
        IMineralRepository MineralRepository { get; set; }
        IVitaminRepository VitaminRepository { get; set; }

        IWorkoutBlockRepository WorkoutBlockRepository { get; set; }
        IWorkoutRepository WorkoutRepository { get; set; }
        IWorkoutTypeRepository WorkoutTypeRepository { get; set; }

        IShoppingRepository ShoppingListRepository { get; set; }

        IFavoriteRecipesRepository FavoriteRepository { get; set; }

        IErrorRepository ErrorRepository { get; set; }

        int SaveChanges();
    }
}
