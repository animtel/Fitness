﻿using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.NutritionMealBlock;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface INutritionMealBlockRepository : IRepository<NutritionMealCategory>
    {
        NutritionMealCategory FindByName(string name);
        void AddUsingModel(NutritionMealCategoryModel category);
        IEnumerable<NutritionMealCategoryModel> GetAllUsingModel();
        void UpdateUsingModel(NutritionMealCategoryModel category);
    }
}
