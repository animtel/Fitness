﻿using Fitness.Entities;

namespace Fitness.DAL.Infrastructure
{
    public interface IVitaminRepository : IRepository<Vitamin>
    {
    }
}