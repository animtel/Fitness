﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities.WorkoutBlock;

namespace Fitness.Data.Infrastructure.Repository
{
    public interface IWorkoutTypeRepository : IRepository<WorkoutType>
    {
    }
}
