﻿using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using System;
using System.Collections.Generic;
using Fitness.Entities.NutritionMealBlock;

namespace Fitness.DAL.Infrastructure
{
    public interface INutritionMealRepository : IRepository<NutritionMeal>
    {
        void AddUsingModel(NutritionMealModel model);
        void UpdateUsingModel(NutritionMealModel model);
        IEnumerable<NutritionMealModel> GetMealsByCategoryUsingModel(NutritionMealCategoryModel model);
        IEnumerable<NutritionMealModel> GetAllUsingModel();
        void AddToCategory(NutritionMealCategoryModel model, Guid guid);
    }
}