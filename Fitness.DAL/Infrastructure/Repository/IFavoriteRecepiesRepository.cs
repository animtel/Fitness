﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IFavoriteRecipesRepository : IRepository<FavoriteRecipe>
    {
    }
}
