﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IHealthyFoodBlockRepository : IRepository<HealthyFoodCategory>
    {
        HealthyFoodCategory FindByName(string name);
    }
}
