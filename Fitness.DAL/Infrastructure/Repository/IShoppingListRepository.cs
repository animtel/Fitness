﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.Shopping;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IShoppingRepository : IRepository<ShoppingItem>
    {
    }
}
