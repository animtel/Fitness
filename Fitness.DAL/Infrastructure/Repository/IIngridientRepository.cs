﻿using System.Collections.Generic;
using Fitness.Entities;
using Fitness.ViewModels.Shared;

namespace Fitness.DAL.Infrastructure
{
    public interface IIngredientRepository : IRepository<Ingredient>
    {
        Ingredient FindByName(string name);
        void AddUsingModel(IngredientModel ingred);
        void UpdateUsingModel(IngredientModel ingred);
        IEnumerable<IngredientModel> GetAllUsingModel();
    }
}