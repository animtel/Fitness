﻿
using System;
using System.Data.Entity;

namespace Fitness.DAL.Infrastructure
{
    public interface IDbFactory
    {
        Context Init();
    }
}