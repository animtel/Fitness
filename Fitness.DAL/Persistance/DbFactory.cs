﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        Context _dbContext;

        public Context Init()
        {
            return _dbContext ?? (_dbContext = new Context());
        }

        protected override void DisposeCore()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }
    }
}
