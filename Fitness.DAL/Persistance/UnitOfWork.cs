﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Infrastructure.Repository;
using Fitness.DAL.Infrastructure.Repository;

namespace Fitness.DAL.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory _dbFactory;
        private Context dbContext;

        public IHealthyFoodBlockRepository HealthyFoodBlockRepository { get; set; }
        public IHealthyFoodRepository HealthyFoodRepository { get; set; }
        public INutritionMealBlockRepository NutrtionBlockRepository { get; set; }
        public INutritionMealRepository NutritionMealRepository { get; set; }
        public IIngredientRepository IngredientRepository { get; set; }
        public IIngredientDescriptionRepository IngredientDescriptionRepository { get; set; }
        public IMineralRepository MineralRepository { get; set; }
        public IVitaminRepository VitaminRepository { get; set; }
        public IWorkoutBlockRepository WorkoutBlockRepository { get; set; }
        public IWorkoutRepository WorkoutRepository { get; set; }
        public IWorkoutTypeRepository WorkoutTypeRepository { get; set; }

        public IShoppingRepository ShoppingListRepository { get; set; }
        public IFavoriteRecipesRepository FavoriteRepository { get; set; }
        public IErrorRepository ErrorRepository { get; set; }

        public UnitOfWork()
        {}

        public UnitOfWork(IHealthyFoodBlockRepository healthyFoodBlockRepository,
            IDbFactory dbFactory,
            IHealthyFoodRepository healthyFoodRepository,
            INutritionMealBlockRepository nutrtionBlockRepository,
            INutritionMealRepository nutritionMealRepository,
            IIngredientRepository ingredientRepository,
            IIngredientDescriptionRepository ingredientDescriptionRepository,
            IMineralRepository mineralRepository,
            IVitaminRepository vitaminRepository,
            IWorkoutBlockRepository workoutBlockRepository,
            IWorkoutRepository workoutRepository,
            IWorkoutTypeRepository workoutTypeRepository,
            IShoppingRepository shoppingListRepository,
            IFavoriteRecipesRepository favoriteRepository,
            IErrorRepository errorRepository)
        {
            this._dbFactory = dbFactory;
            HealthyFoodBlockRepository = healthyFoodBlockRepository;
            HealthyFoodRepository = healthyFoodRepository;
            NutrtionBlockRepository = nutrtionBlockRepository;
            NutritionMealRepository = nutritionMealRepository;
            IngredientRepository = ingredientRepository;
            IngredientDescriptionRepository = ingredientDescriptionRepository;
            MineralRepository = mineralRepository;
            VitaminRepository = vitaminRepository;
            WorkoutBlockRepository = workoutBlockRepository;
            WorkoutRepository = workoutRepository;
            WorkoutTypeRepository = workoutTypeRepository;
            ShoppingListRepository = shoppingListRepository;
            FavoriteRepository = favoriteRepository;
            ErrorRepository = errorRepository;
        }



        public Context DbContext
        {
            get { return dbContext ?? (dbContext = _dbFactory.Init()); }
        }


        public int SaveChanges()
        {
            return DbContext.SaveChanges();
        }
    }
}
