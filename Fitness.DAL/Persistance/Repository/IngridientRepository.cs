﻿
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using AutoMapper;
using Fitness.ViewModels.Shared;

namespace Fitness.DAL.Persistance.Repository
{
    public class IngredientRepository : Repository<Ingredient>, IIngredientRepository
    {
        public IngredientRepository(DbFactory factory) : base(factory)
        {}

        public void AddUsingModel(IngredientModel ingred)
        {
            base.Add(Mapper.Map<IngredientModel, Ingredient>(ingred));
        }

        public Ingredient FindByName(string name)
        {
            return GetAll().SingleOrDefault(s => s.Name == name);
        }

        public IEnumerable<IngredientModel> GetAllUsingModel()
        {
            return DbContext.Set<Ingredient>().ProjectToArray<IngredientModel>();
        }

        public void UpdateUsingModel(IngredientModel ingred)
        {
            base.Update(Mapper.Map<IngredientModel, Ingredient>(ingred));
        }
    }
}
