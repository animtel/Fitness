﻿using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Persistance.Repository
{
    public class HealthyFoodBlockRepository
        : Repository<HealthyFoodCategory>, IHealthyFoodBlockRepository
    {
        public HealthyFoodBlockRepository(DbFactory factory) : base(factory)
        {}


        public HealthyFoodCategory FindByName(string name)
        {
            return DbContext.Set<HealthyFoodCategory>().FirstOrDefault(x => x.Name == name);
        }
    }
}
