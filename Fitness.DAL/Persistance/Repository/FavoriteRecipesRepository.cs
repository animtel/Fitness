﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;

namespace Fitness.DAL.Persistance.Repository
{
    public class FavoriteRecipesRepository : Repository<FavoriteRecipe>, IFavoriteRecipesRepository
    {
        public FavoriteRecipesRepository(DbFactory factory) : base(factory)
        {
        }
    }
}
