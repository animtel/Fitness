﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.ViewModels;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Persistance.Repository
{
    public class HealthyFoodRepository : Repository<HealthyFood>, IHealthyFoodRepository
    {
        public HealthyFoodRepository(DbFactory factory) : base(factory)
        {}


        public HealthyFood FindByName(string name)
        {
            return DbContext.Set<HealthyFood>().FirstOrDefault(x => x.Name == name);
        }

        public new IEnumerable<HealthyFood> GetAll()
        {
            return DbContext.Set<HealthyFood>()
                .Include(x => x.Directions)
                .Include(x => x.IngredientDescriptions)
                .Include(x => x.HealthyFoodCategory);
        }

        public new HealthyFood FindById(Guid id)
        {
            return DbContext.Set<HealthyFood>()
                .Include(x => x.Directions)
                .Include(x => x.IngredientDescriptions)
                .Include(x => x.HealthyFoodCategory).SingleOrDefault(entity => entity.Id == id);
        }
    }
}
