﻿using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.Shopping;

namespace Fitness.DAL.Persistance.Repository
{
    public class ShoppingRepository : Repository<ShoppingItem>, IShoppingRepository
    {
        public ShoppingRepository(DbFactory factory) : base(factory)
        {}
    }
}
