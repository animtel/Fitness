﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.DAL.Persistance.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;

namespace Fitness.Data.Persistance.Repository
{
    public class ErrorRepository : Repository<Error>, IErrorRepository
    {
        public ErrorRepository(DbFactory factory) : base(factory)
        {
            
        }
    }
}
