﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.ViewModels.Nutrition;
using AutoMapper;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.Entities.NutritionMealBlock;
using Fitness.ViewModels;
using Fitness.ViewModels.Shared;

namespace Fitness.DAL.Persistance.Repository
{
    public class NutritionMealRepository : Repository<NutritionMeal>, INutritionMealRepository
    {
        public NutritionMealRepository(DbFactory factory) : base(factory)
        {}

        public void AddToCategory(NutritionMealCategoryModel model, Guid guid)
        {
            var category = DbContext.Set<NutritionMealCategory>().SingleOrDefault(n => n.Name == model.Name);
            var meal = base.FindById(guid);
            meal.NutritionMealCategory = category;
            base.Update(meal);
        }

        public void AddUsingModel(NutritionMealModel model)
        {
            base.Add(this.ToEntity(model));
        }

        public IEnumerable<NutritionMealModel> GetAllUsingModel()
        {
            return DbContext.Set<NutritionMeal>().Select(x => ToModel(x));
        }

        public IEnumerable<NutritionMealModel> GetMealsByCategoryUsingModel(NutritionMealCategoryModel model)
        {
            var meals = DbContext.Set<NutritionMealCategory>()
                .Include("NutritionMeal").SingleOrDefault(x => x.Name == model.Name).Meals;

            var modelsMeal = from meal in meals select ToModel(meal);

            return modelsMeal;
        }

        public void UpdateUsingModel(NutritionMealModel model)
        {
            var meal = ToEntity(model);
            base.Update(meal);
        }

        private NutritionMeal ToEntity(NutritionMealModel model)
        {
            var entity = Mapper.Map<NutritionMealModel, NutritionMeal>(model);

            Func<IngredientDescriptionViewModel, IngredientDescription> modelToDesc = (ingred) => new IngredientDescription(Guid.NewGuid())
            {
                Ingredient = DbContext.Set<Ingredient>().FirstOrDefault(i => i.Name == ingred.Name),
                Count = ingred.Count,
                MeasureType = ingred.MeasureType
            };

            entity.MultipleIngredients = (from ingred in model.Ingredients select modelToDesc(ingred)).ToList();
            entity.Directions = (from el in model.Directions select new HealthyFoodDirection() { Name = el }).ToList();
            entity.NutritionMealCategory = DbContext.Set<NutritionMealCategory>().SingleOrDefault(c => c.Name == model.Name);

            return entity;
        }

        private static NutritionMealModel ToModel(NutritionMeal meal)
        {
            var model = Mapper.Map<NutritionMeal, NutritionMealModel>(meal);
            var stringDirections = (from el in meal.Directions select el.Name).ToArray();

            var ingredients = (from el in meal.MultipleIngredients
                               select new IngredientDescriptionViewModel
                               {
                                   Name = el.Ingredient.Name,
                                   Count = el.Count,
                                   MeasureType = el.MeasureType,
                               }).ToArray();

            model.MealCategory = meal.NutritionMealCategory.Name;
            model.Ingredients = ingredients;
            model.Directions = stringDirections;

            return model;
        }
    }
}
