﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.Entities.NutritionMealBlock;

namespace Fitness.DAL.Persistance.Repository
{
    public class IngredientDescriptionRepository : Repository<IngredientDescription>, IIngredientDescriptionRepository
    {
        public IngredientDescriptionRepository(DbFactory factory) : base(factory)
        {
        }
    }
}
