﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Persistance.Repository
{
    public class Repository<T> : IRepository<T> where T: Entity
    {
        protected Context _dbContext;
        
        protected DbFactory _dbFactory {
            get;
            private set;
        }

        protected Context DbContext
        {
            get { return _dbContext ?? _dbFactory.Init(); }
            set { _dbContext = value; }
        }

        public Repository(DbFactory factory)
        {
            _dbFactory = factory;
        }

        public void Add(T item)
        {
            DbContext.Set<T>().Add(item);
        }

        public T FindById(Guid item)
        {
            return DbContext.Set<T>().FirstOrDefault(x => x.Id == item);
        }

        public IEnumerable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public void AddRange(IEnumerable<T> item)
        {
            DbContext.Set<T>().AddRange(item);
        }

        public void Delete(T item)
        {
            DbContext.Set<T>().Remove(item);
        }

        public void DeleteById(Guid id)
        {
            DbContext.Entry(new Entity(Guid.NewGuid())).State = EntityState.Deleted;
        }

        public void DeleteRange(IEnumerable<T> items)
        {
            DbContext.Set<T>().RemoveRange(items);
        }

        public void Update(T item)
        {
            DbContext.Entry(item).State = EntityState.Modified;
        }
    }
}
