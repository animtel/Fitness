﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.Entities.WorkoutBlock;

namespace Fitness.DAL.Persistance.Repository
{
    public class WorkoutRepository : Repository<Workout>, IWorkoutRepository
    {
        public WorkoutRepository(DbFactory factory) : base(factory)
        {
        }
    }
}
