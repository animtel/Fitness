﻿using Fitness.Data.Infrastructure.Repository;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Persistance.Repository;
using Fitness.Entities.WorkoutBlock;

namespace Fitness.Data.Persistance.Repository
{
    public class WorkoutBlockRepository : Repository<WorkoutCategory>, IWorkoutBlockRepository
    {
        public WorkoutBlockRepository(DbFactory factory) : base(factory)
        {}
    }
}
