﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.ViewModels.Nutrition;
using AutoMapper;
using Fitness.Entities.NutritionMealBlock;

namespace Fitness.DAL.Persistance.Repository
{
    public class NutritionMealBlockRepository : Repository<NutritionMealCategory>, INutritionMealBlockRepository
    {
        public NutritionMealBlockRepository(DbFactory factory) : base(factory)
        {}

        public void AddUsingModel(NutritionMealCategoryModel model)
        {
            var category = Mapper.Map<NutritionMealCategoryModel, NutritionMealCategory>(model);
            
            base.Add(category);
        }

        public NutritionMealCategory FindByName(string name)
        {
            return DbContext.Set<NutritionMealCategory>().FirstOrDefault(x => x.Name == name);
        }
        public IEnumerable<NutritionMealCategoryModel> GetAllUsingModel()
        {
            return DbContext.Set<NutritionMealCategory>().ProjectToArray<NutritionMealCategoryModel>();
        }

        public void UpdateUsingModel(NutritionMealCategoryModel category)
        {
            base.Update(Mapper.Map<NutritionMealCategoryModel, NutritionMealCategory>(category));
        }

        
    }
}
