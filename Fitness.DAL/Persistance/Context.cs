﻿using Fitness.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.Entities.Identity;
using Fitness.Entities.NutritionMealBlock;

namespace Fitness.DAL
{
    public class Context : IdentityDbContext<FitnessUser>
    {
        public Context()
            : base("FitnessDB", throwIfV1Schema: false)
        {
        }

        public IDbSet<HealthyFood> Foods { get; set; }

        public IDbSet<Ingredient> Ingredients { get; set; }

        public IDbSet<Vitamin> Vitamins { get; set; }

        public IDbSet<Error> Errors { get; set; }

       // public IDbSet<ShoppingItem> ShopingItems { get; set; }

        public IDbSet<NutritionMealCategory> MealCategories { get; set; }

        public IDbSet<HealthyFoodCategory> HealthyFoodCategories { get; set; }

       // public IDbSet<WorkoutCategory> Categories { get; set; }

       // public IDbSet<Workout> Workouts { get; set; }

       // public IDbSet<WorkoutType> WorkoutTypes { get; set; }

        public IDbSet<Mineral> Minerals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FitnessUser>()
                .HasOptional(c => c.ShoppingList)
                .WithRequired(d => d.FitnessUser);

            base.OnModelCreating(modelBuilder);
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        public static Context Create()
        {
            return new Context();
        }
    }
}
