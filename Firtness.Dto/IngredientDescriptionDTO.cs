﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.DTO
{
    public class IngredientDescriptionDTO : DTO
    {
        public string Name { get; set; }
        public string MeasureType { get; set; }
        public int Count { get; set; }
    }
}
