﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.DTO
{
    public class ShoppingItemDTO : DTO
    {
        public IngredientDescriptionDTO IngredientDescription { get; set; }
    }
}
