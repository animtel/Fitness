﻿(function () {
    console.log("LOAD S");
    $.ajax({
        type: 'GET',
        url: '/api/Account/ExternalLogins?returnUrl=%2F&generateState=true'
    }).success(function (data) {

        var html = '';
        for (var index = 0; index < data.length; index++) {
            html += '<a href=' + data[index].url + '>' + data[index].name + '</a>';
        }
        $('#message-bar').html(html);

    }).fail(function (data) {
        alert('Error');
        });

    function addApiKeyAuthorization() {
        var key = $('#input_apiKey')[0].value;
        if (key && key.trim() != "") {
            var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization(swashbuckleConfig.apiKeyName, key, swashbuckleConfig.apiKeyIn);
            window.swaggerUi.api.clientAuthorizations.add("Autorization", 'Bearer ' +apiKeyAuth);
            log("added key " + key);
        }

        var newWin = window.open("about:blank", "hello", "width=400,height=400");

        if (key && key.trim() !== "") {
            window.authorizations.add("key", new ApiKeyAuthorization("Authorization", key, "header"));
        }
    }
    $('#input_apiKey').change(addApiKeyAuthorization);
})();

