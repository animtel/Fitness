﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fitness.Entities;

namespace Fitness.Calculator
{
    public class NutritionCalculator
    {
        public enum NutritionCategories
        {
            Breakfast,
            Lunch,
            Dinner,
            Supper,
            Snack
        }

        public enum Sex
        {
            Male,
            Female
        }

        public List<List<T>> CartesianProduct<T>(
            IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };
            var product = sequences.Aggregate(
                emptyProduct,
                (accumulator, sequence) =>
                    from accseq in accumulator
                    from item in sequence
                    select accseq.Concat(new[] {item}));
            var productList = product.Select(x => x.ToList()).ToList(); 
            return productList;
        }

        public List<NutritionMeal> ClosestTo(List<List<NutritionMeal>> collection, decimal target)
        {
            List<NutritionMeal> closest = null;
            var minDiffrence = decimal.MaxValue;

            foreach (var element in collection)
            {
                var diff = Math.Abs(element
                                        .Sum(x => x.NutritionIngredientDescriptions.Sum(l => l.Count * l.Ingredient.Calories)
                                        ) - target);

                if (minDiffrence > diff)
                {
                    minDiffrence = (int)diff;
                    closest = element;
                }
            }

            return closest;
        }

        public static double CalculateBMR(FitnessUser user)
        {
            var activ = 1.2;
            int goal = 0;
            switch (user.ActivityLvl)
            {
                case Activity.Little:
                    activ = 1.2;
                    break;
                case Activity.Light:
                    activ = 1.375;
                    break;
                case Activity.Moderate:
                    activ = 1.55;
                    break;
                case Activity.Hard:
                    activ = 1.95;
                    break;
                default:
                    break;
            }
            switch (user.Goal)
            {
                case Goal.Loss:
                    goal = -500;
                    break;
                case Goal.Maintenanse:
                    goal = 0;
                    break;
                case Goal.Gain:
                    goal = 500;
                    break;

                default:
                    break;
            }
            if (user.Sex == Entities.Sex.Male)
            {

                
                return goal + (10.0 * user.Weight + 6.25 * user.Height - 5.0 * user.Age + 5.0) * activ;
            }
            else
            {
                return goal + (10.0 * user.Weight + 6.25 * user.Height - 5.0 * user.Age - 161.0) * activ;
            }
        }
    }
}
