﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using Fitness.Providers;
using Fitness.DAL;
using Microsoft.Owin.Cors;

namespace Fitness
{
    using System.Configuration;
    using Owin;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.Google;
    using Fitness.Entities;
    using Microsoft.Owin.Security.DataProtection;
    using Microsoft.Owin;
    using Fitness.Data.Identity;
    using Microsoft.Owin.Security.Infrastructure;
    using Owin.Security.Providers.Facebook;

    public partial class Startup
    {
        AppUserManager UserManagerFabric() =>  new AppUserManager(new AppUserStore(new Context()));
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static string PublicClientId { get; private set; }

        public static string FaceBookClientId { get; private set; }
        protected static  string FaceBookSecretKey { get; private set; }
        public static  string GoogleClientId { get; private set; }
        protected static  string GoogleSecretKey { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            //var dataProtectionProvider = app.GetDataProtectionProvider();

            //builder.RegisterType<AppUserManager>().AsSelf().InstancePerRequest();
            //builder.Register(c => new ApplicationOAuthProvider(c.Resolve<AppUserManager>())).AsImplementedInterfaces().InstancePerRequest();
            //builder.Register(c => new UserStore<FitnessUser>(c.Resolve<Context>())).As<IUserStore<FitnessUser>>().InstancePerRequest();
            //builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>();
            //builder.Register(c => new AppOAuthAuthorizationServerOptions(c.Resolve<ApplicationOAuthProvider>()),  null).AsSelf().InstancePerRequest();
            var container = AutofacConfiguration.RegisterServices(builder);
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            #region Logins

            app.UseAutofacMiddleware(container);
            //app.UseAutofacWebApi(GlobalConfiguration.Configuration);
            app.UseCors(CorsOptions.AllowAll);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(UserManagerFabric()),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            app.UseOAuthBearerTokens(OAuthOptions);

            app.UseOAuthAuthorizationServer(OAuthOptions);
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            #endregion

            #region Socials

            FaceBookClientId = ConfigurationManager.AppSettings["facebookAppId"];
            FaceBookSecretKey = ConfigurationManager.AppSettings["facebookAppSecret"];
            app.UseFacebookAuthentication(new FacebookAuthenticationOptions()
            {
                AppId = FaceBookClientId,
                AppSecret = FaceBookSecretKey
            });
            GoogleClientId = ConfigurationManager.AppSettings["googleClientId"];
            GoogleSecretKey = ConfigurationManager.AppSettings["googleClientSecret"];
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = GoogleClientId,
                ClientSecret = GoogleSecretKey,

            });

            #endregion
           

        }
        private Data.Identity.AppUserManager BuildUserManager(
        IComponentContext context,
        IEnumerable<Autofac.Core.Parameter> parameters,
        IDataProtectionProvider dataProtectionProvider)
        {

            var manager = context.Resolve<Data.Identity.AppUserManager>(parameters);
            //var manager = new Data.Identity.AppUserManager(context.Resolve<AppUserStore>());

            // Register two factor authentication providers.
            // This application uses Phone and Emails as a step of receiving a code
            // for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code",
                new PhoneNumberTokenProvider<FitnessUser>
                {
                    MessageFormat = "Your security code is {0}"
                });
            manager.RegisterTwoFactorProvider("Email Code",
                new EmailTokenProvider<FitnessUser>
                {
                    Subject = "Security Code",
                    BodyFormat = "Your security code is {0}"
                });


            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new Microsoft.AspNet.Identity.Owin.DataProtectorTokenProvider<FitnessUser>(
                    dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}
