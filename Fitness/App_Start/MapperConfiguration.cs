﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Fitness.BLL.Infrastructure;
using Fitness.Entities;
using Fitness.MapperProfiles;
using Fitness.ViewModels.Nutrition;
using Fitness.ViewModels.Shared;

namespace Fitness.App_Start
{
    public class MapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfiles(MapperBussinessLayer.GetProfiles());
                cfg.AddProfile<PLHealthyFoodProfile>();
                cfg.AddProfile<IngredientDescriptionProfile>();
                cfg.AddProfile<NutritionProfile>();
                cfg.AddProfile<AccountProfile>();
                cfg.AddProfile<IngredientProfile>();
                cfg.AddProfile<ProfileEditProfile>();
                cfg.AddProfile<WorkoutProfile>();
                cfg.AddProfile<GenerateNutritionProfile>();
            });
        }
    }

    public class GenerateNutritionProfile : Profile
    {
        public GenerateNutritionProfile()
        {

            CreateMap<GenerateNutritionMealViewModel, GenerateNutritionMeal>()
                .ForMember(src=>src.FitnessUser, opt=>opt.Ignore())
                .ForMember(src => src.NutritionMealCategory,
                    opt => opt.MapFrom(src => src.MealIndex));
            ;
            CreateMap<GenerateNutritionMeal, GenerateNutritionMealViewModel>().ForMember(src => src.MealIndex,
                opt => opt.MapFrom(src => src.NutritionMealCategory));


            CreateMap<CustomNutritionIngredientDescription, CustomNutritionIngredientDescriptionViewModel>().ForMember(src=>src.GenerateNutritionMeal, opt=>opt.Ignore()).ForMember(src => src.MeasureTypeabbreviation, opt => opt.MapFrom(src => src.Measure.Abbreviation));
            CreateMap<CustomNutritionIngredientDescriptionViewModel, CustomNutritionIngredientDescription>();

            //CreateMap<NutritionIngredientDescriptionViewModel, NutritionIngredientDescription>()
            //    .ForMember(src => src.Ingredient, opt => opt.Ignore())
            //    .ForMember(src => src.NutritionMeal, opt => opt.Ignore())
            //    .ForMember(src => src.NutritionMealId, opt => opt.Ignore())
            //                .ForMember(src => src.IngredientId, opt => opt.MapFrom(src => src.NutritionIngredient.Id));

            //CreateMap<NutritionIngredientDescription, NutritionIngredientDescriptionViewModel>()
            //    .ForMember(src => src.NutritionIngredient, opt => opt.MapFrom(src => src.Ingredient))
            //    .ForMember(src => src.MeasureType, opt => opt.MapFrom(src => src.MeasureType))
            //    .ForMember(src => src.Count, opt => opt.MapFrom(src => src.Count));

            //CreateMap<NutritionMealViewModel, NutritionMeal>()
            //    .ForMember(d => d.NutritionIngredientDescriptions, opt => opt.MapFrom(src => src.NutritionIngredientDescriptions))
            //    .ForMember(d => d.NutritionMealCategoryId, opt => opt.MapFrom(r => r.NutritionMealCategoryId))
            //    .ForMember(d => d.NutritionMealCategory, opt => opt.Ignore())

            //    //.ForMember(d => d.NutritionMealCategoryId, opt => opt.Ignore())
            //    .ForMember(d => d.UsersLike, opt => opt.Ignore())
            //    .ForMember(d => d.ImageName, opt => opt.MapFrom(src => src.ImageName))
            //    ;


            //CreateMap<NutritionMeal, NutritionMealViewModel>()
            //    .ForMember(d => d.NutritionMealCategory, opt => opt.MapFrom(src => src.NutritionMealCategory.Name))
            //    .ForMember(d => d.NutritionMealCategoryId, opt => opt.MapFrom(src => src.NutritionMealCategoryId))
            //    .ForMember(d => d.NutritionIngredientDescriptions, opt => opt.MapFrom(src => src.NutritionIngredientDescriptions))

            //    ;


            //CreateMap<Ingredient, NutritionIngredientViewModel>().ReverseMap();

            ////.ForMember((d) => d.NutritionMealDirections, opt =>
            ////    opt.Ignore()); 

            //CreateMap<NutritionMealCategoryViewModel, NutritionMealCategory>()
            //    .ForMember(src => src.NutritionMeals, opt => opt.Ignore())
            //    .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(src => src.ImageName, opt => opt.MapFrom(src => src.ImageUrl));

            //CreateMap<NutritionMealCategory, NutritionMealCategoryViewModel>()
            //    .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(src => src.ImageUrl, opt => opt.MapFrom(src => src.ImageName))
            //    ;
        }
    }
}