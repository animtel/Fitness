﻿using System.Web;
using System.Web.Mvc;
using Fitness.Filters;

namespace Fitness
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
