﻿using System.Reflection;
using Fitness.IoC;
using Autofac;
using Autofac.Integration.WebApi;

namespace Fitness
{
    public class AutofacConfiguration
    {
        public static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule<BBLModule>();

            return builder.Build();
        }
    }
}