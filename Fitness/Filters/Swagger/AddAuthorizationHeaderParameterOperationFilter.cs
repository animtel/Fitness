﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Web.Http.Filters;
using System.Web.Mvc;
using Swashbuckle.Swagger;

namespace Fitness.Filters.Swagger
{
    /// <summary>
    /// TODO Swagger autorization to header 
    /// </summary>
    public class AddAuthorizationHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var filterPipeline = apiDescription.ActionDescriptor.GetFilterPipeline();
            var isAuthorized = filterPipeline
                .Select(filterInfo => filterInfo.Instance)
                .Any(filter => filter is IAuthenticationFilter || filter is System.Web.Http.Filters.IAuthorizationFilter);//TODO IAuthorizationFilter

            var allowAnonymous = apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
            var allowautorize = apiDescription.ActionDescriptor.GetCustomAttributes<AuthorizeAttribute>().Any();
            

            if (isAuthorized && operation.parameters != null)
            {
                operation.parameters.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    description = "access token",
                    required = false,
                    type = "string"
                });
            }
        }
    }
}
