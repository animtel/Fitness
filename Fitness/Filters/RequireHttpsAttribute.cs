﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fitness.Filters
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response =
                    new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "HTTPS Required" };
            }

            base.OnAuthorization(actionContext);
        }
    }
}