﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Fitness.BLL.DTO;
using Fitness.BLL.Infrastructure;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.ViewModels;
using Fitness.ViewModels.Healthy;
using System.Net.Http.Headers;
using System.IO;
using System.Drawing;
using MultipartDataMediaFormatter.Infrastructure;
using System.Web.Hosting;
using System.Drawing.Imaging;
using Fitness.DTO;
using Fitness.ViewModels.Shared;

namespace Fitness.Controllers
{
    [AllowAnonymous]
    public class HealthyFoodController : BaseController
    {
        private IHealthyFoodService _healthyFoodService;
        private INutritionService _nutritionService;
        public HealthyFoodController(
            IErrorService errorService,
            IAccountService accountService,
            IHealthyFoodService healthyFoodService,
            INutritionService nutritionService
            )
            : base(errorService, accountService)
        {
            _nutritionService = nutritionService;
            _healthyFoodService = healthyFoodService;
        }



        [HttpPost]
        [Route("healthyfood/food/create")]
        public HttpResponseMessage Add(HttpRequestMessage request,
                                        HealthyFoodViewModel food)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var dto = Mapper.Map<HealthyFoodViewModel, HealthyFoodDTO>(food);

                this._healthyFoodService.AddFood(dto);
                //var id = _healthyFoodService.GetAllHealthyFoods().FirstOrDefault(x => x.Name == food.Name).Id;
                response = request.CreateResponse(HttpStatusCode.Created);

                return response;
            });
        }

        [HttpPost]
        [Route("healthyfood/food/createimage")]
        public HttpResponseMessage CreateImage(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                if (HttpContext.Current.Request.Files.Count == 0)
                {
                    return request.CreateResponse("Image not Select");
                }

                HttpPostedFile image = HttpContext.Current.Request.Files[0];
                var imageId = Guid.NewGuid();
                var imageFormat = '.' + image.ContentType.Split('/')[1];
                var returnPath = $"Content/{imageId}{imageFormat}";
                var imagePath = HttpContext.Current.Server.MapPath("~/Content/" + imageId + imageFormat);
                image.SaveAs(imagePath);

                return request.CreateResponse(HttpStatusCode.OK, returnPath);

            });
        }

        [HttpGet]
        [Route("healthyfood/food/getfoodbycategory/{categoryId:guid}")]
        public HttpResponseMessage GetFoodbyCategory(HttpRequestMessage request, Guid categoryId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;


                IEnumerable<dynamic> result = null;

                result = premium ? _healthyFoodService.GetFoodByCategoryId(categoryId) : _healthyFoodService.GetFoodByCategoryIdNotPremium(categoryId);
                

                response = request.CreateResponse(HttpStatusCode.Created, result);

                return response;
            });
        }

        [HttpPost]
        [Route("healthyfood/food/addingredienttofood/{foodId:guid}")]
        public HttpResponseMessage AddIngredientToFood(HttpRequestMessage request, Guid ingredient, Guid foodId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Guid> ingredients = new List<Guid>();
                ingredients.Add(ingredient);
                //var s = Mapper.Map<IEnumerable<IngredientViewModel>, IEnumerable<IngredientDTO>>(model.IngredientDescriptions.Select(x=>x.Ingredient));
                //var result = _healthyFoodService.GetFoodById(foodId);

                _healthyFoodService.AddRangeIngredientToFood(ingredients, foodId);
                response = request.CreateResponse(HttpStatusCode.Created, true);

                return response;
            });
        }



        /// <summary>
        /// GetAll healthy by Category
        /// filtering by premium (user)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("healthyfood/category/getallbycategories")]
        public HttpResponseMessage GetAllbyCategories(HttpRequestMessage request)
        {
            return this.CreateHttpResponse(request, () =>
            {

                IEnumerable<dynamic> categories = null;

                categories = this._healthyFoodService.GetAllFoodByCategories(premium);
                return request.CreateResponse(HttpStatusCode.OK, categories);
            });
        }

        [HttpGet]
        [Route("healthyfood/food/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request = null)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var foods = this._healthyFoodService.GetAllHealthyFoods();
                var viewModels = Mapper.Map<IEnumerable<HealthyFoodDTO>, IEnumerable<HealthyFoodViewModel>>(foods);

                response = request.CreateResponse(HttpStatusCode.OK, foods);

                return response;
            });
        }

        [HttpGet]
        [Route("healthyfood/food/get/{foodId:guid}")]
        public HttpResponseMessage GetById(HttpRequestMessage request, Guid foodId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var food = this._healthyFoodService.GetFoodById(foodId);

                var viewModel = Mapper.Map<HealthyFoodDTO, HealthyFoodViewModel>(food);

                viewModel.ImageName = _healthyFoodService.GetImageNameById(foodId);

                response = request.CreateResponse(HttpStatusCode.OK, viewModel);

                return response;
            });
        }

        [HttpPut]
        [Route("healthyfood/food/update")]
        public HttpResponseMessage Update(HttpRequestMessage request, HealthyFoodViewModel food)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                this._healthyFoodService.UpdateFood(Mapper.Map<HealthyFoodViewModel, HealthyFoodDTO>(food));

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }

        [HttpDelete]
        [Route("healthyfood/food/delete/{foodId:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid foodId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                this._healthyFoodService.DeleteFoodById(foodId);

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }

        //[HttpGet]
        //[Route("healthyfood/food/getimagebyfoodid/{foodId:guid}")]
        //public HttpResponseMessage GetImageByUserId(HttpRequestMessage request, Guid foodId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        //string filepath = HostingEnvironment.MapPath("~/Content/"
        //        //    + _healthyFoodService.Fin(foodId));
        //        var food = _healthyFoodService.GetFoodById(foodId);

        //        food.ImageName = "~/Content/" + food.ImageName;
        //        //FileStream stream = new FileStream(filepath, FileMode.Open);

        //        //Image image = Image.FromStream(stream);
        //        //var result = new MemoryStream();
        //        //image.Save(result, ImageFormat.Jpeg);

        //        //response.Content = new ByteArrayContent(File.ReadAllBytes(filepath));
        //        //response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");


        //        return request.CreateResponse<string>(HttpStatusCode.OK, food.ImageName);
        //    });
        //}
    }
}
