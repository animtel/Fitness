﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using Fitness.ViewModels;
using Fitness.ViewModels.Nutrition;
using Fitness.BLL.Interfaces;
using Fitness.BLL.Services;
using Fitness.Calculator;
using Fitness.Data.Infrastructure.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Infrastructure;

namespace Fitness.Controllers
{
    
    public class NutritionController : BaseController
    {
        private readonly INutritionService mealService;

        private readonly IGenerateNutritionMealService generateService;


        //private readonly IUnitOfWork unitOfWork;

        public NutritionController(
             IErrorService errorService,
            //IUnitOfWork unitOfWork,
             IGenerateNutritionMealService generateService,
            IAccountService accountService,
            INutritionService mealService
        ) : base(errorService, accountService)
        {
            this.generateService = generateService;
            this.mealService = mealService;
            //this.unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("nutritionmeal/createimage")]
        public HttpResponseMessage CreateImage(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                if (HttpContext.Current.Request.Files.Count == 0)
                {
                    return request.CreateResponse("Image not Select");
                }

                HttpPostedFile image = HttpContext.Current.Request.Files[0];
                var imageId = Guid.NewGuid();
                var imageFormat = '.' + image.ContentType.Split('/')[1];
                var returnPath = $"Content/{imageId}{imageFormat}";
                var imagePath = HttpContext.Current.Server.MapPath("~/Content/" + imageId + imageFormat);
                image.SaveAs(imagePath);

                return request.CreateResponse(HttpStatusCode.OK, returnPath);

            });
        }

        /// <summary>
        /// generate meals plan for one week for autorize user, NOT SAVE
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("nutritionmeal/generate")]
        public HttpResponseMessage Generate(HttpRequestMessage request)
        {
            
            return this.CreateHttpResponse(request, () =>
            {
                //var userId = User.Identity.GetUserId();

                var user = _accountService. GetCurrentUserById(User.Identity.GetUserId());


                var guids = generateService.GetAll(user).Select(x=> x.Id ).ToArray();
                generateService.DeleteRange(guids);

                return request.CreateResponse(HttpStatusCode.OK, Mapper.Map< IEnumerable<GenerateNutritionMeal>, IEnumerable<GenerateNutritionMealViewModel>>(generateService.GenerateMealPlan(user)));

            });
        }

        /// <summary>
        /// Add Nutritionmeals (not ingredientDescription)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="meal"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        [HttpPost]
        [Route("nutritionmeal/create")]
        public HttpResponseMessage CreateMeal(HttpRequestMessage request, NutritionMealViewModel meal)
        {
            return this.CreateHttpResponse(request, () =>
            {
                //unitOfWork.NutritionMealRepository.Add(meal);


                mealService.CreateNutrition(meal);
                return request.CreateResponse(HttpStatusCode.OK, meal);
            });
        }


        [Authorize]
        [HttpGet]
        [Route("nutritionmeal/getbyid/{id:guid}")]
        public HttpResponseMessage GetMealById(HttpRequestMessage request, Guid id)
        {
            return this.CreateHttpResponse(request, () => request.CreateResponse(HttpStatusCode.OK, mealService.GetNutritionById(id)));
            
        }




        /// <summary>
        /// Delete Nutritionmeals (not ingredientDescription), input Model nutritionmeal
        /// </summary>
        /// <param name="request"></param>
        /// <param name="meal"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("nutritionmeal/delete/{id:guid}")]
        public HttpResponseMessage DeleteMeal(HttpRequestMessage request, Guid id)
        {
            return this.CreateHttpResponse(request, () =>
            {
                //unitOfWork.NutritionMealRepository.Delete(meal);

                mealService.DeleteNutrition(id);
                
                return request.CreateResponse(HttpStatusCode.Accepted);
            });
        }
        /// <summary>
        /// Update Nutritionmeals (not ingredientDescription), input Model nutritionmeal
        /// </summary>
        /// <param name="request"></param>
        /// <param name="meal"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [Route("nutritionmeal/update")]
        public HttpResponseMessage UpdateMeal(HttpRequestMessage request, NutritionMealViewModel meal)
        {
            return this.CreateHttpResponse(request, () =>
            {
                //unitOfWork.NutritionMealRepository.Update(meal);
                mealService.UpdateNutrition(meal);
                return request.CreateResponse(HttpStatusCode.OK, meal);
            });
        }


        /// <summary>
        /// Nutritionmeals (not ingredientDescription), input Model null, request null (default)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("nutritionmeal/getall")]
        public HttpResponseMessage AllMeal(HttpRequestMessage request = null)
        {
            var getall = mealService.GetAllNutrition();
            return this.CreateHttpResponse(request, () => request.CreateResponse(HttpStatusCode.OK, getall));
        }


        /// <summary>
        /// return nutrition three params name, image and Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("nutritionmeal/getonlynameimageId")]
        public HttpResponseMessage GetOnlyNameImageId()
        {
            HttpRequestMessage request = Request;
            return this.CreateHttpResponse(request, () => request.CreateResponse(HttpStatusCode.OK, mealService.GetOnlyNameImageId()));
        }
        ///// <summary>
        ///// Add Ingredient Description
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="nutrition"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpPost]
        //[Route("create")]
        //public HttpResponseMessage Create(HttpRequestMessage request, IngredientDescription nutrition)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        mealService.Add(nutrition);
        //        return request.CreateResponse(HttpStatusCode.OK, nutrition);
        //    });
        //}
        ///// <summary>
        ///// Create image ingredient desc...
        ///// add image url (string) to entity in db
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="nutrition"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpPost]
        //[Route("createimage")]
        //public HttpResponseMessage CreateImage(HttpRequestMessage request, IngredientDescription nutrition)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        mealService.Add(nutrition);
        //        return request.CreateResponse(HttpStatusCode.OK, nutrition);
        //    });
        //}

        ///// <summary>
        ///// add single ingredient to ingredient description
        ///// return ingredientdescription
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="ingredient"></param>
        ///// <param name="nutritionId"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpPost]
        //[Route("add-ingredient")]
        //public HttpResponseMessage AddIngredient(HttpRequestMessage request, Ingredient ingredient,Guid nutritionId)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        //IngredientDescription object = Nutrition
        //        var nutrition = mealService.Get(nutritionId);
        //        nutrition.Ingredient = ingredient;

        //        mealService.Add(nutrition);
        //        return request.CreateResponse(HttpStatusCode.OK, nutrition);
        //    });
        //}

        ///// <summary>
        ///// GetAll all ingredient description in database (ALL)
        ///// return all => iEnumerable IngredientDesc
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpGet]
        //[Route("getall")]
        //public HttpResponseMessage GetAll(HttpRequestMessage request)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        return request.CreateResponse(HttpStatusCode.OK, mealService.GetAll());
        //    });
        //}
        ///// <summary>
        ///// Update IngredientDescription
        ///// Return Ok always :)
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="nutrition"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpPut]
        //[Route("update")]
        //public HttpResponseMessage Update(HttpRequestMessage request, IngredientDescription nutrition)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        mealService.Update(nutrition);

        //        return request.CreateResponse(HttpStatusCode.OK);
        //    });
        //}
        ///// <summary>
        ///// Return Ok always :)
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="nutrition"></param>
        ///// <returns></returns>
        //[Authorize]
        //[HttpDelete]
        //[Route("delete")]
        //public HttpResponseMessage Delete(HttpRequestMessage request, IngredientDescription nutrition)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        mealService.Delete(nutrition);

        //        return request.CreateResponse(HttpStatusCode.OK);
        //    });
        //}

    }
}







