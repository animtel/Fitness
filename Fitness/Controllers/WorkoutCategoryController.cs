﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure.Repository;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Newtonsoft.Json;
using Fitness.ViewModels.Workout;

namespace Fitness.Controllers
{
    [AllowAnonymous]
    public class WorkoutCategoryController : BaseController
    {
        private readonly IWorkoutCategoryService _workoutCategoryService;

        public WorkoutCategoryController(
            IWorkoutCategoryService workoutcategoryService,
            IErrorService errorService,
            IAccountService accountService
        ) : base(errorService, accountService)
        {
            _workoutCategoryService = workoutcategoryService;
        }

        [Route("workout-category/all")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                response = request.CreateResponse(HttpStatusCode.OK,_workoutCategoryService.GetAllCategories());
                         

                return response;
            });
        }

        [Route("workout-category/add")]
        [HttpPost]
        public HttpResponseMessage WorkoutAdd(WorkoutCategoryViewModel workout)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _workoutCategoryService.Create(workout);


                response = request.CreateResponse<bool>(HttpStatusCode.OK, true);

                return response;
            });
        }


        [Route("workout-category/delete/{id:guid}")]
        [HttpDelete]
        public HttpResponseMessage DeleteWorkout(Guid id)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _workoutCategoryService.Delete(id);


                response = request.CreateResponse(HttpStatusCode.OK, "Removed");

                return response;
            });
        }

        /// <summary>
        /// Get all subcategory for single workout category
        /// </summary>
        /// <param name="workoutCategoryName"></param>
        /// <param name="getworkouts"></param>
        /// <returns></returns>
        [Route("workout-category/{workoutCategoryName}/subcategories")]
        [HttpGet]
        public HttpResponseMessage GetAllSubcategory(string workoutCategoryName)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                response = request.CreateResponse(HttpStatusCode.OK, _workoutCategoryService.GetSingle(workoutCategoryName.Replace('-',' ')));
                return response;
            });
        }



        //[Route("info")]
        //[HttpGet]
        //public HttpResponseMessage Info(HttpRequestMessage request, Guid categoryId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        var category = _categoryRepository.FindById(categoryId);


        //        response = request.CreateResponse<WorkoutCategory>(HttpStatusCode.OK, category);

        //        return response;
        //    });
        //}


        //[Route("create")]
        //[HttpPost]
        //public HttpResponseMessage Create(HttpRequestMessage request, WorkoutCategory obj)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        _categoryRepository.Add(obj);
        //        _unitOfWork.SaveChanges();
        //        response = request.CreateResponse<WorkoutCategory>(HttpStatusCode.Created, obj);

        //        return response;
        //    });

        //}

    }
}
