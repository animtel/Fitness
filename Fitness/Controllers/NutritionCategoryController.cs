﻿using System;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.ViewModels.Nutrition;
using System.Collections.Generic;
using Fitness.BLL.Infrastructure;
using Fitness.BLL.Interfaces;

namespace Fitness.Controllers
{
    public class NutritionCategoryController : BaseController
    {
        private readonly INutritionService _categoryService;

        public NutritionCategoryController(
            IErrorService errorService,
            IAccountService accountService,
            INutritionService categoryService) : base(errorService, accountService)
        {
            _categoryService = categoryService;
        }


        //[HttpPost]
        //[Route("nutrition/category/create")]
        //public HttpResponseMessage Create(HttpRequestMessage request, NutritionMealCategoryModel category)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        _categoryRepository.AddUsingModel(category);

        //        base._unitOfWork.SaveChanges();

        //        return request.CreateResponse(HttpStatusCode.OK);
        //    });
        //}


        [HttpGet]
        [Route("nutritioncategories/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return this.CreateHttpResponse(request, () =>
            {

                var categories = _categoryService.GetAllCategories();

                return request.CreateResponse(HttpStatusCode.OK, categories);
            });
        }

        //[HttpPut]
        //[Route("nutrition/category/update")]
        //public HttpResponseMessage Update(HttpRequestMessage request, NutritionMealCategoryModel category)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        _categoryRepository.UpdateUsingModel(category);

        //        base._unitOfWork.SaveChanges();

        //        return request.CreateResponse(HttpStatusCode.OK);
        //    });
        //}

        //[HttpDelete]
        //[Route("nutrition/category/delete/{categoryId:guid}")]
        //public HttpResponseMessage Delete(HttpRequestMessage request, Guid categoryId)
        //{
        //    return this.CreateHttpResponse(request, () =>
        //    {
        //        _categoryRepository.DeleteById(categoryId);

        //        base._unitOfWork.SaveChanges();

        //        return request.CreateResponse(HttpStatusCode.OK);
        //    });
        //}
    }
}
