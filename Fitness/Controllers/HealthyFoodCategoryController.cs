﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Fitness.Entities;
using Fitness.ViewModels;
using Fitness.ViewModels.Healthy;
using Fitness.BLL.Infrastructure;
using Fitness.BLL.Interfaces;
using Fitness.BLL.DTO;
using Fitness.DTO;
using Microsoft.AspNet.Identity;

namespace Fitness.Controllers
{
    public class HealthyFoodCategoryController : BaseController
    {
        private IHealthyFoodService _healthyFoodService;

        public HealthyFoodCategoryController(
            IErrorService errorService, 
            IAccountService accountService, 
            IHealthyFoodService healthyFoodService) : base(errorService, accountService)
        {
            _healthyFoodService = healthyFoodService;
        }

        [HttpPost]
        [Route("healthyfood/category/create")]
        public HttpResponseMessage Create(HttpRequestMessage request, HealthyFoodCategoryViewModel category)
        {
            return this.CreateHttpResponse(request, () =>
            {
                this._healthyFoodService.AddCategory(
                    Mapper.Map<HealthyFoodCategoryViewModel, HealthyFoodCategoryDTO>(category));

                return request.CreateResponse(HttpStatusCode.OK);
            });
        }





        [HttpPost]
        [Route("healthyfood/category/createimage")]
        public HttpResponseMessage CreateImage(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                var files = HttpContext.Current.Request.Files;
                if (files == null)
                {
                    return request.CreateResponse("Image not Select");
                }
                else
                {
                    HttpPostedFile image = HttpContext.Current.Request.Files[0];
                    var name = Guid.NewGuid();
                    var imageFormat = '.' + image.ContentType.Split('/')[1];
                    var returnPath = $"Content/{name}{imageFormat}";
                    var imagePath = HttpContext.Current.Server.MapPath("~/Content/" + name + imageFormat);
                    image.SaveAs(imagePath);

                    return request.CreateResponse(HttpStatusCode.OK, returnPath);
                }

            });
        }

        [HttpGet]
        [Route("healthyfood/category/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return this.CreateHttpResponse(request, () =>
            {
                var categories = this._healthyFoodService.GetAllCategories();
                return request.CreateResponse(HttpStatusCode.OK, categories);
            });
        }

        /// <summary>
        /// Get AllCategories and for each categories return List Foods related to it
        /// </summary>
        /// <param name="request">NULL</param>
        /// <returns></returns>
        [HttpGet]
        [Route("healthyfood/category/getallfoodforcategories")]
        public HttpResponseMessage GetAllfoodforcategories(HttpRequestMessage request = null)
        {
            return this.CreateHttpResponse(request, () =>
            {
                //var user = this._accountService.GetCurrentUserById(User.Identity.GetUserId());
                var categories = this._healthyFoodService.GetAllFoodByCategories(premium);
                return request.CreateResponse(HttpStatusCode.OK, categories);
            });
        }



        [HttpPut]
        [Route("healthyfood/category/update")]
        public HttpResponseMessage Update(HttpRequestMessage request, HealthyFoodCategoryViewModel category)
        {
            return this.CreateHttpResponse(request, () =>
            {
                _healthyFoodService.UpdateCategory(Mapper.Map<HealthyFoodCategoryViewModel, HealthyFoodCategoryDTO>(category));
                return request.CreateResponse(HttpStatusCode.OK);
            });
        }

        [HttpDelete]
        [Route("healthyfood/category/delete/{categoryId:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid categoryId)
        {
            return this.CreateHttpResponse(request, () =>
            {
                this._healthyFoodService.DeleteCategoryById(categoryId);
                return request.CreateResponse(HttpStatusCode.OK);
            });
        }

        
    }
}