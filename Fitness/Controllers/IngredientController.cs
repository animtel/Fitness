﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Fitness.ViewModels.Shared;
using Fitness.BLL.Interfaces;
using Fitness.DTO;
using Fitness.Infrastructure;

namespace Fitness.Controllers
{
    [AllowAnonymous]
    public class IngredientController : BaseController
    {
        private readonly IIngredientService _ingredientService;

        public IngredientController(
            IErrorService _errorService,
            IIngredientService ingredientService,
            IAccountService _accountService
        ) : base(_errorService, _accountService)
        {
            this._errorService = _errorService;
            this._ingredientService = ingredientService;
            this._accountService = _accountService;
        }


        [HttpPost]
        [Route("ingredient/create")]
        public HttpResponseMessage Create(HttpRequestMessage request, IngredientViewModel ingredient)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                ingredient.Id = Guid.NewGuid();
                _ingredientService.Create(Mapper.Map<IngredientViewModel, IngredientDTO>(ingredient));

                response = request.CreateResponse<IngredientViewModel>(HttpStatusCode.Created, ingredient);
                
                return response;
            });
        }

        [HttpGet]
        [Route("ingredient/get/{ingredientId:guid}")]
        public HttpResponseMessage GetIngredient(HttpRequestMessage request, Guid ingredientId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var ingredient = _ingredientService.Find(ingredientId);

                response = request.CreateResponse<IngredientDTO>(HttpStatusCode.OK, ingredient);

                return response;
            });

        }



        [HttpGet]
        [Route("ingredient/search/{page:int=0}/{pageSize=4}/{filter?}")]
        public HttpResponseMessage Search(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                List<IngredientDTO> ingredients = null;
                int totalIngredients = new int();

                if (!string.IsNullOrEmpty(filter))
                {
                    filter = filter.Trim().ToLower();

                    ingredients = _ingredientService.GetAll(c => c.Name.ToLower().Contains(filter))
                        .OrderBy(c => c.Name)
                        .ToList();
                }
                else
                {
                    ingredients = _ingredientService.GetAll().ToList();
                }

                totalIngredients = ingredients.Count();
                ingredients = ingredients.Skip(currentPage * currentPageSize)
                    .Take(currentPageSize)
                    .ToList();

                IEnumerable<IngredientDTO> customersVM = ingredients;

                PaginationSet<IngredientDTO> pagedSet = new PaginationSet<IngredientDTO>()
                {
                    Page = currentPage,
                    TotalCount = totalIngredients,
                    TotalPages = (int)Math.Ceiling((decimal)totalIngredients / currentPageSize),
                    Items = customersVM
                };

                response = request.CreateResponse<PaginationSet<IngredientDTO>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }
        [HttpGet]
        [Route("ingredient/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var ingredients = _ingredientService.GetAll();

                response = request.CreateResponse(HttpStatusCode.OK, ingredients);

                return response;
            });
        }

        [HttpPut]
        [Route("ingredient/update")]
        public HttpResponseMessage Update(HttpRequestMessage request, IngredientViewModel ingredient)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _ingredientService.Update(Mapper.Map<IngredientViewModel,IngredientDTO> (ingredient));

                response = request.CreateResponse(HttpStatusCode.OK, ingredient);

                return response;
            });
        }


        [HttpDelete]
        [Route("ingredient/delete/{ingredientId:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid ingredientId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _ingredientService.Delete(new IngredientDTO(){
                    Id = ingredientId
                });

                response = request.CreateResponse(HttpStatusCode.OK, ingredientId);

                return response;
            });
        }
    }
}
