﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Fitness.BLL.Interfaces;
using Fitness.ViewModels.Shopping;
using Fitness.DTO;
using System.Linq;
using Fitness.ViewModels.Shared;
using Fitness.Entities;
using Fitness.ViewModels;

namespace Fitness.Controllers
{
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    //[Authorize]
    //[AllowAnonymous]
    public class ShoppingController : BaseController
    {


        private IShoppingService _service;

        public ShoppingController(IErrorService errorService, IAccountService accountService, IShoppingService service) : base(errorService, accountService)
        {
            _service = service;
        }
        private FitnessUser CurrentUser => _accountService.GetCurrentUserById(User.Identity.GetUserId());

        /// <summary>
        /// Add shoping item
        /// template
        /// {
        ///  "ingredientDesc": [
        ///    {
        ///      "ingredient": {
        ///        "id": "de6bac51-5208-41f9-9e5b-03a3784cf1d5"
        ///      },
        ///"measureType": 1,
        ///"count":176
        ///    }
        ///  ],
        ///  "id": "string" GENERATE IN CLIENT
        ///}
        ///if ingredientdescription count > 1, Add only the first element
        ///
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("shopping/add")]

        public HttpResponseMessage Add(HttpRequestMessage request, ShoppingItemVModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var user = CurrentUser;

                _service.AddToList(model, user);



                response = request.CreateResponse(HttpStatusCode.Created);

                return response;
            });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="shoppingitemId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("shopping/get/{shoppingitemId:guid}")]
        public HttpResponseMessage Get(HttpRequestMessage request, Guid shoppingitemId)
        {

            HttpResponseMessage response = null;
            var user = CurrentUser;
            response = request.CreateResponse(HttpStatusCode.OK, _service.Get(user, shoppingitemId));

            return response;

        }
        [HttpGet]
        [Route("shopping/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {

            HttpResponseMessage response = null;
            var user = CurrentUser;
            response = request.CreateResponse(HttpStatusCode.OK, _service.GetFromList(user));

            return response;

        }

        #region Update
        /// <summary>
        /// {
        ///  "ingredientDesc": [
        ///    {
        ///      "ingredient": {
        ///        "id": "de6bac51-5208-41f9-9e5b-03a3784cf1d5"
        ///      },
        ///"measureType": 1,
        ///"count":176
        ///    }
        ///  ],
        ///  "id": "string" GENERATE IN CLIENT AND Insert for update shoppingitem
        ///}
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("shopping/update")]
        public HttpResponseMessage Edit(HttpRequestMessage request, ShoppingItemVModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var user = CurrentUser;

                _service.Update(user, model);

                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        #endregion

        [HttpDelete]
        [Route("shopping/delete/{id:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _service.DeleteFromListById(id);

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }
        [HttpDelete]
        [Route("shopping-nutrition/delete/{id:guid}")]
        public HttpResponseMessage DeleteByid(HttpRequestMessage request, Guid id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _service.DeleteById(id);

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }



        #region Update
        /// <summary>
        /// {
        ///  "ingredientDesc": [
        ///    {
        ///      "ingredient": {
        ///        "id": "de6bac51-5208-41f9-9e5b-03a3784cf1d5"
        ///      },
        ///"measureType": 1,
        ///"count":176
        ///    }
        ///  ],
        ///  "id": "string" GENERATE IN CLIENT AND Insert for update shoppingitem
        ///}
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("shopping-nutrition/update")]
        public HttpResponseMessage EditItem(HttpRequestMessage request, ShoppingItemVModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var user = CurrentUser;

                _service.Update(user, model);

                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        #endregion

    }
}
