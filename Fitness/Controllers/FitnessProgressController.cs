﻿using Fitness.BLL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Fitness.Entities;
using Fitness.ViewModels.FitnessProgress;
using Microsoft.Ajax.Utilities;
using SkiaSharp;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Fitness.Controllers
{
    [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
    //[Authorize]
    //[AllowAnonymous]
    public class FitnessProgressController : BaseController
    {


        private readonly IFitnessProgressService _progressService;

        public FitnessProgressController(IErrorService errorService, IAccountService accountService, IFitnessProgressService progressService) : base(errorService, accountService)
        {
            _progressService = progressService;
        }
        private FitnessUser CurrentUser => _accountService.GetCurrentUserById(User.Identity.GetUserId());


        [HttpPost]
        [Route("progress/photoupload")]
        public HttpResponseMessage PhotoUpload(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }

                HttpPostedFile image = null;


                image = HttpContext.Current.Request.Files[0];

                //catch (Exception ex)
                //{

                //    return await System.Threading.Tasks.Task.Run(() => request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.Message));
                //}

                //var imageResize =  CompressionImage(image.InputStream);
                var imageId = Guid.NewGuid();
                var imageFormatResult = '.' + "jpg";
                string returnUrl = "";
                //var imagePath = HttpContext.Current.Server.MapPath("~/Content/" + imageId + imageFormatResult);



                using (Bitmap bmp1 = new Bitmap(image.InputStream))
                {

                    System.Drawing.Imaging.Encoder myEncoder =
                    System.Drawing.Imaging.Encoder.Quality;

                    ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);
                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);



                    myEncoderParameters.Param[0] = myEncoderParameter;
                    bmp1.Save(HttpContext.Current.Server.MapPath("~/Content/" + imageId + imageFormatResult), jpgEncoder, myEncoderParameters);
             
                    var size = 500;
                    int width, height;
                    if (bmp1.Width > bmp1.Height)
                    {
                        width = size;
                        height = Convert.ToInt32(bmp1.Height * size / (double)bmp1.Width);
                    }
                    else
                    {
                        width = Convert.ToInt32(bmp1.Width * size / (double)bmp1.Height);
                        height = size;
                    }
                    var resized = new Bitmap(bmp1, width, height);

                        resized.Save(HttpContext.Current.Server.MapPath("~/Content/" + imageId + "-small" + imageFormatResult), jpgEncoder, myEncoderParameters);
                   

                    returnUrl = "Content/" + imageId;
                }

                return request.CreateResponse(HttpStatusCode.OK, returnUrl);

            });
        }
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        [HttpPost]
        [Route("progress/add")]

        public HttpResponseMessage Add(HttpRequestMessage request, FitnessProgressWeekViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var user = CurrentUser;
                _progressService.CreateOrUpdate(model, user.Id);
                response = request.CreateResponse(HttpStatusCode.Created);

                return response;
            });
        }

        //[HttpGet]
        //[Route("progress/get/{shoppingitemId:guid}")]
        //public HttpResponseMessage Get(HttpRequestMessage request, Guid Id)
        //{

        //    HttpResponseMessage response = null;
        //    var user = CurrentUser;
        //    response = request.CreateResponse(HttpStatusCode.OK, _progressService.Get(user,Id));

        //    return response;

        //}
        [HttpGet]
        [Route("progress/getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {

            HttpResponseMessage response = null;
            var user = CurrentUser;

            response = request.CreateResponse(HttpStatusCode.OK, _progressService.GetAllFitnessProgressWeeks(user.Id));

            return response;

        }

        #region Update
        [HttpPut]
        [Route("shopping/update")]
        public HttpResponseMessage Edit(HttpRequestMessage request, FitnessProgressWeekViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var user = CurrentUser;
                _progressService.CreateOrUpdate(model, user.Id);

                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        #endregion

        [HttpDelete]
        [Route("progress/delete/{id:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _progressService.Delete(id);

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }

    }
}
