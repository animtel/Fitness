﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.BLL.Interfaces;
using Fitness.ViewModels.Workout;

namespace Fitness.Controllers
{

    [AllowAnonymous]
    public class WorkoutController : BaseController
    {
        private readonly IWorkoutCategoryService _workoutCategoryService;
        private readonly IWorkoutTypeService _workoutTypeService;
        private readonly IWorkoutService _workoutService;

        public WorkoutController(
            IWorkoutCategoryService workoutCategoryService,
            IWorkoutTypeService workoutTypeService,
            IWorkoutService workoutService,
            IErrorService errorService,
            IAccountService accountService
        ) : base(errorService, accountService)
        {
            _workoutCategoryService = workoutCategoryService;
            _workoutService = workoutService;
            _workoutTypeService = workoutTypeService;
        }








        [Route("workouttype/getall")]
        [HttpGet]
        public HttpResponseMessage GetAllWorkoutTypes()
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                response = request.CreateResponse(HttpStatusCode.OK, _workoutTypeService.GetAllWorkoutTypes());

                return response;
            });
        }



        [Route("workout/add")]
        [HttpPost]
        public HttpResponseMessage WorkoutAdd(WorkoutViewModel workout)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _workoutService.Create(workout);


                response = request.CreateResponse<bool>(HttpStatusCode.OK, true);

                return response;
            });
        }
        [Route("workout/getall")]
        [HttpGet]
        public HttpResponseMessage GetAllWorkouts()
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                


                response = request.CreateResponse(HttpStatusCode.OK, _workoutService.GetAllWorkouts());

                return response;
            });
        }

        [Route("workout/delete/{id:guid}")]
        [HttpDelete]
        public HttpResponseMessage DeleteWorkout(Guid id)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                _workoutService.Delete(id);


                response = request.CreateResponse(HttpStatusCode.OK, "Removed");

                return response;
            });
        }

        [Route("workout/findbyid/{id:guid}")]
        [HttpGet]
        public HttpResponseMessage FindWorkout(Guid id)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                 var workout = _workoutService.FindById(id);


                response = request.CreateResponse(HttpStatusCode.OK, workout);

                return response;
            });
        }

        [Route("workout/update")]
        [HttpPut]
        public HttpResponseMessage UpdateWorkout(WorkoutViewModel workout)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(Request, () =>
            {
                HttpResponseMessage response = null;

                _workoutService.Update(workout);


                response = request.CreateResponse(HttpStatusCode.OK, "Updated");

                return response;
            });
        }
        //}

        //[Route("type/tocategory")]
        //[HttpPut]
        //public HttpResponseMessage ToCategory(HttpRequestMessage request, Guid categoryId, Guid workoutTypeId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        var category = _categoryRepository.FindById(categoryId);

        //        var type = _workoutTypeRepository.FindById(workoutTypeId);

        //        category.WorkoutTypes.Add(type);

        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<bool>(HttpStatusCode.OK, true);

        //        return response;
        //    });

        //}

        //[Route("type/deletetocategory")]
        //[HttpDelete]
        //public HttpResponseMessage DeleteToCategory(HttpRequestMessage request, Guid categoryId, Guid workoutTypeId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        HttpResponseMessage response = null;


        //        var category = _categoryRepository.FindById(categoryId);

        //        var type = _workoutTypeRepository.FindById(workoutTypeId);

        //        category.WorkoutTypes.Remove(type);

        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<bool>(HttpStatusCode.OK, true);

        //        return response;
        //    });
        //}

        //[Route("type/all")]
        //public HttpResponseMessage GetAllType(HttpRequestMessage request)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        var types = JsonConvert.SerializeObject(_workoutTypeRepository.GetAll(), Formatting.Indented,
        //            new JsonSerializerSettings()
        //            {
        //                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        //            }
        //        );

        //        response = request.CreateResponse<IEnumerable<WorkoutType>>(HttpStatusCode.OK, JsonConvert.DeserializeObject<IEnumerable<WorkoutType>>(types));

        //        return response;
        //    });

        //}

        //[Route("type/create")]
        //[HttpPost]
        //public HttpResponseMessage CreateType(HttpRequestMessage request, WorkoutType obj)
        //{
        //    return CreateHttpResponse(request, () =>
        //        {
        //            HttpResponseMessage response = null;

        //            _workoutTypeRepository.Add(obj);
        //            _unitOfWork.SaveChanges();
        //            response = request.CreateResponse<WorkoutType>(HttpStatusCode.Created, obj);

        //            return response;
        //        });

        //}

        //[Route("type/delete")]
        //public HttpResponseMessage DeleteType(HttpRequestMessage request, Guid workoutTypeId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        HttpResponseMessage response = null;

        //        var work = _workoutTypeRepository.FindById(workoutTypeId);
        //        _workoutTypeRepository.Delete(work);
        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<WorkoutType>(HttpStatusCode.OK, work);

        //        return response;
        //    });
        //}

        //[Route("type/edit")]
        //public HttpResponseMessage EditType(HttpRequestMessage request, WorkoutType work)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        _workoutTypeRepository.Update(work);
        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<WorkoutType>(HttpStatusCode.Accepted, work);

        //        return response;
        //    });
        //}


        //[Route("workout/totype")]
        //[HttpPut]
        //public HttpResponseMessage ToType(HttpRequestMessage request, Guid workoutTypeId, Guid workoutGuid)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        HttpResponseMessage response = null;

        //        var type = _workoutTypeRepository.FindById(workoutTypeId);

        //        var workout = _workoutRepository.FindById(workoutGuid);
        //        type.Workouts.Add(workout);

        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<bool>(HttpStatusCode.OK, true);

        //        return response;
        //    });

        //}

        //[Route("workout/all")]
        //[HttpGet]
        //public HttpResponseMessage GetAll(HttpRequestMessage request)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        var workouts = JsonConvert.SerializeObject(_workoutRepository.GetAll(), Formatting.Indented,
        //                                            new JsonSerializerSettings()
        //                                            {
        //                                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        //                                            }
        //                                        );
        //        response = request.CreateResponse<IEnumerable<Workout>>(HttpStatusCode.OK, JsonConvert.DeserializeObject<IEnumerable<Workout>>(workouts));

        //        return response;
        //    });

        //}

        //[Route("workout/info")]
        //[HttpGet]
        //public HttpResponseMessage Info(HttpRequestMessage request, Guid workoutId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        var work = _workoutRepository.FindById(workoutId);


        //        response = request.CreateResponse<Workout>(HttpStatusCode.OK, work);

        //        return response;
        //    });

        //}

        //[Route("workout/create")]
        //[HttpPost]
        //public HttpResponseMessage Create(HttpRequestMessage request, string WorkoutName, Guid typeId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        var type = _workoutTypeRepository.FindById(typeId);
        //        var obj = new Workout { Name = WorkoutName };
        //        type.Workouts.Add(obj);
        //        _unitOfWork.SaveChanges();
        //        response = request.CreateResponse<Workout>(HttpStatusCode.Created, obj);

        //        return response;
        //    });

        //}

        //[Route("workout/delete")]
        //public HttpResponseMessage Delete(HttpRequestMessage request, Guid workoutId)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        var work = _workoutRepository.FindById(workoutId);
        //        _workoutRepository.Delete(work);
        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<Workout>(HttpStatusCode.OK, work);

        //        return response;
        //    });
        //}

        //[Route("workout/edit")]
        //public HttpResponseMessage Edit(HttpRequestMessage request, Workout work)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;

        //        _workoutRepository.Update(work);
        //        _unitOfWork.SaveChanges();

        //        response = request.CreateResponse<Workout>(HttpStatusCode.Accepted, work);

        //        return response;
        //    });

        //}

    }
}
