﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using Microsoft.AspNet.Identity;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.BLL.Interfaces;
using Fitness.BLL.Services;

namespace Fitness.Controllers
{
    public class PickmealsController : BaseController
    {
        private readonly IPickmealsService _pickmealsService;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="favoriteRecipeRepository"></param>
        /// <param name="errorService"></param>
        /// <param name="accountService"></param>
        /// <param name="mealRecipeRepository"></param>
        public PickmealsController(
            IPickmealsService pickmealsService,
            IErrorService errorService,
            IAccountService accountService,
            INutritionMealRepository mealRecipeRepository
            ) : base(errorService, accountService)
        {
            _pickmealsService = pickmealsService;
        }



        [Route("pickmeals/getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                response = request.CreateResponse(HttpStatusCode.OK, _pickmealsService.GetAll(CurrentUser.Id));

                return response;
            });
        }

        [HttpGet]
        [Route("pickmeals/info")]
        public HttpResponseMessage Info(HttpRequestMessage request, Guid pickmealsId)
        {
            return CreateHttpResponse(request, () =>
            {

                HttpResponseMessage response = null;

                //var favoriteRecipe = _pickmealsService.Get(null);

                response = request.CreateResponse(HttpStatusCode.OK/*, favoriteRecipe*/);

                return response;
            });
        }

        [HttpPut]
        [Route("pickmeals/add")]
        public HttpResponseMessage Add(HttpRequestMessage request, Guid nutritionMealId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var user = CurrentUser;
                _pickmealsService.Add(new Pickmeals{ Id = Guid.NewGuid(), NutritionMealId = nutritionMealId, UserId = user.Id });

                response = request.CreateResponse(HttpStatusCode.OK, true);

                return response;
            });
        }

        [HttpDelete]
        [Route("pickmeals/delete/{favoriteRecipeId:guid}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, Guid pickmealsId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                //_pickmealsRepository.Delete(pickmealsId);

                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });

        }
    }
}
