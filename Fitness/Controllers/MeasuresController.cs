﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Fitness.BLL.Interfaces;
using Fitness.Entities.Measure;

namespace Fitness.Controllers
{

    public class MeasuresController : BaseController
    {


        private readonly IMeasureService _measureService;



        /// <summary>
        /// //////
        /// </summary>
        /// <param name="errorService"></param>
        /// <param name="accountService"></param>
        /// <param name="measureService"></param>
        public MeasuresController(
            IErrorService errorService,
            IAccountService accountService,
            IMeasureService measureService
        ) : base(errorService, accountService)
        {
            this._measureService = measureService;
            //this.unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("measures/create")]
        public HttpResponseMessage Create(Measure measure)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request,
                () => {
                    _measureService.Create(measure);
                    return request.CreateResponse(HttpStatusCode.OK);
                });
        }

        [HttpGet]
        [Route("measures/getall")]
        public HttpResponseMessage GetAll()
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request,
                () => request.CreateResponse(HttpStatusCode.OK, _measureService.GetAll()));
        }

        [HttpDelete]
        [Route("measures/delete/{id:guid}")]
        public HttpResponseMessage delete(Guid id)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {

                _measureService.DeleteById(id);
                return request.CreateResponse(HttpStatusCode.OK);
            });
        }
    }
}