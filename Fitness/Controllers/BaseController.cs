﻿using Fitness.Entities;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.Data.Identity;
using Fitness.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Fitness.Controllers
{

    public class BaseController : ApiController
    {

        protected bool premium = true;//TODO FOR TEST





        protected IErrorService _errorService;
        protected IAccountService _accountService;
        public BaseController()
        {}
        
        public BaseController(IErrorService errorService, 
            IAccountService accountService)
        {
            _errorService = errorService;
            _accountService = accountService;
            
        }
        //UserManager<FitnessUser> m => new UserManager<FitnessUser>(new UserStore<FitnessUser>(new Context()));
        //protected FitnessUser CurrentUser => m.FindById(TESTUSERID );
        //protected FitnessUser CurrentUser => _accountService.GetCurrentUserById(User.Identity.GetUserId());

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;

            try
            {
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                this._errorService.LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                this._errorService.LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }
        
        protected async Task<HttpResponseMessage> CreateHttpResponseAsync(
            HttpRequestMessage request, Func<Task<HttpResponseMessage>> function)
        {
            HttpResponseMessage response = null;

            try
            {
                response = await function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                this._errorService.LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                this._errorService.LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }
    }
}