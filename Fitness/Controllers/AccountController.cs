﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;
using Autofac.Core;
using AutoMapper;
using Fitness.BLL.DTO.Identity;
using Fitness.BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Fitness.Models;
using Fitness.Providers;
using Fitness.Results;
using Fitness.Entities;
using Fitness.Calculator;
using Fitness.Data.Identity;
using Fitness.ViewModels;
using Fitness.Filters;
using Fitness.BLL.Services;
using Fitness.ViewModels.Nutrition;

namespace Fitness.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : BaseController
    {
        private const string LocalLoginProvider = "Local";

        private ISecureDataFormat<AuthenticationTicket> _accessTokenFormat { get; set; }
        private readonly IGenerateNutritionMealService generateNutritionMealService;

        private AppUserManager _userManager => _accountService.UserManager;

        public AccountController(
            //IPickmealsService pickmealsService,
            IErrorService errorService,
            IAccountService accountService,
            IGenerateNutritionMealService generateNutritionMealService,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat) : base(errorService, accountService)
        {
            this.generateNutritionMealService = generateNutritionMealService;
            this._accessTokenFormat = accessTokenFormat;
            //this.pickmealsService = pickmealsService;
        }

        private FitnessUser CurrentUser => _accountService.GetCurrentUserById(User.Identity.GetUserId());


        /// <summary>
        /// Get generated meal plan based on pre-selected nutritionMeal in nutritionMealSection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("checked/{id:guid}/{check:bool}")]
        [HttpGet]
        public HttpResponseMessage GetGenerateNutritionMeal(Guid id, bool check)
        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                //var currentuser = CurrentUser;
                generateNutritionMealService.Check(id, check);

       
                response = request.CreateResponse(HttpStatusCode.OK);

                return response;
            });
        }
        [Route("get-generatedMealPlan")]
        [HttpGet]
        public HttpResponseMessage GetGenerateNutritionMeal(HttpRequestMessage request = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var currentuser = CurrentUser;

                if(currentuser.WeekExpiresMealPlan < CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(DateTime.UtcNow, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday))
                {
                    var guids = generateNutritionMealService.GetAll(currentuser).Select(x => x.Id).ToArray();
                    generateNutritionMealService.DeleteRange(guids);

                    generateNutritionMealService.GenerateMealPlan(currentuser);

                }
                

                var dict = generateNutritionMealService.GetAll(currentuser);


                response = request.CreateResponse(HttpStatusCode.OK, dict);

                return response;
            });
        }

        /// <summary>
        /// GetALL pick meals for THIS user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("pickmeals-getall-ids")]
        [HttpGet]
        public HttpResponseMessage GetAllIds(HttpRequestMessage request = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var currentuser = CurrentUser;

                response = request.CreateResponse(HttpStatusCode.OK, _accountService.PickMeals(currentuser.Id).Select(x=>x.Id));

                return response;
            });
        }

        /// <summary>
        /// GetALL pick meals for THIS user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("pickmeals-getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var currentuser = CurrentUser;

                response = request.CreateResponse(HttpStatusCode.OK, _accountService.PickMeals(currentuser.Id));

                return response;
            });
        }

        /// <summary>
        /// Many ids nutrition /// delete to future______
        /// </summary>
        /// <param name="request"></param>
        /// <param name="nutritionMealIds"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("pickmeals")]
        public HttpResponseMessage Add(HttpRequestMessage request,  List<Guid> nutritionMealIds)
        {
            return CreateHttpResponse(request, () =>
            {

                
                HttpResponseMessage response = null;

                var user = CurrentUser;

                var result = _accountService.PickMeal(user.Id, nutritionMealIds);
       
                response = request.CreateResponse(result ? HttpStatusCode.OK: HttpStatusCode.NotModified, result);

                return response;
            });
        }
        /// <summary>
        /// Many ids nutrition /// delete to future______
        /// </summary>
        /// <param name="request"></param>
        /// <param name="mealId"></param>
        /// <param name="picked"></param>
        /// <param name="nutritionMealIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("pick-meal/{mealId:guid}/{picked:bool}")]
        public HttpResponseMessage PickMeal(Guid mealId, bool picked)

        {
            HttpRequestMessage request = Request;
            return CreateHttpResponse(request, () =>
            {


                HttpResponseMessage response = null;

                var user = CurrentUser;

                response = request.CreateResponse(_accountService.PickMeal(user.Id, mealId, picked));

                return response;
            });
        }



        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("GetProfile")]
        [HttpGet]

        public HttpResponseMessage GetProfile(HttpRequestMessage request)
        {


            var userid = User.Identity.GetUserId();
            FitnessUser user = base._accountService.GetCurrentUserById(userid);

            HttpResponseMessage response = null;

            return response = request?.CreateResponse<ProfileViewModel>(HttpStatusCode.OK, new ProfileViewModel
            {

                UserName = user.UserName,

                Age = user.Age,
                Height = user.Height,
                Weight = user.Weight,
                Calories = NutritionCalculator.CalculateBMR(user),
                ActivityLvl = user.ActivityLvl,
                Goal = user.Goal,
                PhoneNumber = user.PhoneNumber,
                Email = user.Email,
                Sex = user.Sex,
                HeightMeasuareUnit = user.HeightMeasuareUnit,
                WeightMeasuareUnit = user.WeightMeasuareUnit,

            });

        }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("SetProfile")]
        [HttpPost]
        public HttpResponseMessage SetProfile(HttpRequestMessage request, ProfileViewModel model)
        {

            HttpResponseMessage response = null;
            return CreateHttpResponse(request, () =>
            {
                FitnessUser user = base._accountService.GetCurrentUserById(User.Identity.GetUserId());

                user.Height = model.Height;
                user.Weight = model.Weight;
                user.Age = model.Age;
                user.ActivityLvl = model.ActivityLvl;
                user.Email = model.Email;
                user.Goal = model.Goal;
                user.HeightMeasuareUnit = model.HeightMeasuareUnit;
                user.Sex = model.Sex;
                user.WeightMeasuareUnit = model.WeightMeasuareUnit;

                model.Calories = NutritionCalculator.CalculateBMR(user);
                bool check = _accountService.Update(user);
                response = request.CreateResponse(HttpStatusCode.Accepted, model);
                if (!check)
                {
                    response = request.CreateResponse(HttpStatusCode.NotModified, model);
                }


               return response;
   
            });


        }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            //var user = base._accountService.GetCurrentUserById(User.Identity.GetUserId() ?? TESTUSERID);
            
            return new UserInfoViewModel
            {
                //ActivityLvl = user.ActivityLvl.GetType().GetProperty(user.ActivityLvl.ToString()).GetValue(user.ActivityLvl) as string,

                //Goal = user.Sex.GetType().GetProperty(user.Goal.ToString()).GetValue(user.Goal) as string,

                //Sex = user.Sex.GetType().GetProperty(user.Sex.ToString()).GetValue(user.Sex) as string,
                UserId = User.Identity.GetUserId(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin?.LoginProvider
            };
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await _userManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _userManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [AllowAnonymous]
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = _accessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await _userManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await _userManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await _userManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            FitnessUser user = await _userManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;


            //            if (user == null)
            //            {
            //                await RegisterExternal(new RegisterExternalBindingModel()
            //                {
            //                    Email = externalLogin.UserName
            //                });
            //;            }
            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(_userManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(_userManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);

                var info = await Authentication.GetExternalLoginInfoAsync();
                user = new FitnessUser() {
                    Id = Guid.NewGuid().ToString(),
                    UserName = info.Email,
                    Email = info.Email ?? $"{claims.FirstOrDefault().Value }@email.{externalLogin.LoginProvider}" };

                IdentityResult createResult = await _userManager.CreateAsync(user);
                if (!createResult.Succeeded)
                {

                    return new NotFoundWithMessageResult(createResult);
    
                }
                IdentityResult addLoginResult = await _userManager.AddLoginAsync(user.Id, info.Login);

                if (!addLoginResult.Succeeded)
                {
                    return GetErrorResult(addLoginResult);
                }

                await _userManager.AddClaimAsync(user.Id, claims.FirstOrDefault());

                Authentication.SignIn(identity);

            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
       {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public HttpResponseMessage Register(HttpRequestMessage request, RegisterViewModel model)
        {
            return CreateHttpResponse(request,() =>
            {
                _accountService.CreateAsync(Mapper.Map<RegisterViewModel, RegisterDTO>(model));
                //request.CreateResponse(HttpStatusCode.Accepted)
                return request.CreateResponse(HttpStatusCode.Accepted);
                
            });
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new FitnessUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await _userManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return new NotFoundWithMessageResult(result);
            }

            IdentityResult resultresult = await _userManager.AddLoginAsync(user.Id, info.Login);
            if (!resultresult.Succeeded)
            {
                return GetErrorResult(result); 
            }
            return Ok();
        }

        #region Helpers

        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {

                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
     
                        ModelState.AddModelError("", error);
                    }

                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        public class NotFoundWithMessageResult : IHttpActionResult
        {
            private IdentityResult result;

            public NotFoundWithMessageResult(IdentityResult result)
            {
                this.result = result;
            }

            public Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
            {

                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                if (!result.Succeeded)
                {

                    if (result.Errors != null)
                    {
                 
                        string errorTemp = "";
                        foreach (string error in result.Errors)
                        {

                            errorTemp += error;
                            
                            
                        }
                        response.Content = new StringContent(errorTemp);

                    }
                }
                return Task.FromResult(response);
            }
        }
        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }


        // POST api/Account/Profile
        [AllowAnonymous]
        [Route("Profile")]
        public Task<IHttpActionResult> Profile(UserInfoViewModel model)
        {
            throw new NotImplementedException();
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var user = await _userManager.FindByEmailAsync(model.Email);
//                user.Age = model.Age;
//                user.ActivityLvl = model.ActivityLvl;
//                user.Goal = model.Goal;
//                user.Sex = model.Sex;
//                user.Height = model.Height;
//                user.Weight = model.Weight;
//                user.HeightMeasuareUnit = model.HeightMeasuareUnit;
//                user.WeightMeasuareUnit = model.WeightMeasuareUnit;
//
//            IdentityResult result = await _userManager.UpdateAsync(user);
//
//            if (!result.Succeeded)
//            {
//                return GetErrorResult(result);
//            }
//
//            return Ok(NutritionCalculator.CalculateBMR(user));


        }

        #endregion
    }
}
