﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Fitness.DAL;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using Fitness.ViewModels;
using Fitness.ViewModels.Healthy;
using Fitness.Extensions.MapperExtenstions;
using Fitness.BLL.DTO;
using Fitness.ViewModels.Shared;
using Fitness.ViewModels.Shopping;
using Fitness.DTO;

namespace Fitness.MapperProfiles
{
    public class PLHealthyFoodProfile : Profile
    {
        public PLHealthyFoodProfile()
        {
            this.CreateMap<HealthyFoodCategoryViewModel, HealthyFoodCategoryDTO>().ReverseMap();
            this.CreateMap<IngredientDescriptionViewModel, IngredientDescriptionDTO>();
            this.CreateMap<IngredientDescriptionDTO, IngredientDescriptionViewModel>();
     
            this.CreateMap<HealthyFoodViewModel, HealthyFoodDTO>();
            this.CreateMap<HealthyFoodDTO, HealthyFoodViewModel>();

            //this.CreateMap<ShoppingItemViewModel, ShoppingItemDTO>().ReverseMap();

        }
    }
}