﻿using AutoMapper;
using Fitness.Entities;
using Fitness.Extensions.MapperExtenstions;
using Fitness.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fitness.MapperProfiles
{
    public class SharedProfile : Profile
    {
        public SharedProfile()
        {
            CreateMap<IngredientViewModel, Ingredient>().ReverseMap();
  
        }
    }
}