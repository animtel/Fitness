﻿using AutoMapper;
using Fitness.BLL.Services;
using Fitness.Entities;
using Fitness.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fitness.MapperProfiles
{
    public class ProfileEditProfile : Profile
    {
        public ProfileEditProfile()
        {

            CreateMap<ProfileViewModel, ProfileDTO>().ReverseMap();
            CreateMap<FitnessUser, ProfileDTO>().ReverseMap();
        }
        
    }
}