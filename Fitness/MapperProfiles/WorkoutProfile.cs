﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Fitness.BLL.Services;
using Fitness.Entities;
using Fitness.ViewModels;
using Fitness.ViewModels.Workout;

namespace Fitness.MapperProfiles
{

    public class WorkoutProfile : Profile
    {
        public WorkoutProfile()
        {

            CreateMap<Workout, WorkoutViewModel>().ForMember(src=>src.WorkoutType, opt=>opt.Ignore());
            CreateMap<WorkoutViewModel,Workout>();
            CreateMap<WorkoutType, WorkoutTypeViewModel>()
                .ForMember(src=>src.WorkoutCategories, opt=>opt.MapFrom(src=>src.WorkoutCategories));
            CreateMap<WorkoutTypeViewModel, WorkoutType>()
                .ForMember(src=>src.WorkoutCategories, opt=>opt.MapFrom(src=>src.WorkoutCategories));
            CreateMap<WorkoutCategory, WorkoutCategoryViewModel>()
                .ForMember(src => src. Name, opt => opt.MapFrom(src => src.Name));
            CreateMap<WorkoutCategoryViewModel, WorkoutCategory>()
                .ForMember(src=>src.WorkoutTypes, opt=>opt.Ignore())
                .ForMember(src=>src.Name, opt=>opt.MapFrom(src=>src.Name));


            CreateMap<WorkoutExtCategoryViewModel, WorkoutCategory>()
            .ForMember(src => src.WorkoutTypes, opt => opt.MapFrom(src => src.WorkoutTypes))
            .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<WorkoutCategory, WorkoutExtCategoryViewModel>()
            .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(src => src.WorkoutTypes, opt => opt.MapFrom(src => src.WorkoutTypes));
            ;
        }

    }
}