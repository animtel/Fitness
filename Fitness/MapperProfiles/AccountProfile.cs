﻿using AutoMapper;
using Fitness.BLL.DTO.Identity;
using Fitness.Entities;
using Fitness.Models;

namespace Fitness.MapperProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<RegisterViewModel, RegisterDTO>()
                .ForMember(src => src.Role, 
                    opt => new AppRole(){ Name = "User"});
        }
    }
}