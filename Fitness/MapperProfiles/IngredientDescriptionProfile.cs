﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Fitness.Entities;
using Fitness.ViewModels;
using Fitness.ViewModels.Shared;
using Fitness.BLL.DTO;
using Fitness.DTO;

namespace Fitness.MapperProfiles
{
    public class IngredientDescriptionProfile : Profile
    {
        public IngredientDescriptionProfile()
        {
            CreateMap<IngredientDescriptionDTO, IngredientDescriptionViewModel>().ReverseMap();
            CreateMap<IngredientDescriptionDTO, IngredientDescription>().ReverseMap();
        }
    }
}