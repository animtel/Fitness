﻿
using Fitness.ViewModels;

namespace Fitness.MapperProfiles
{
    using AutoMapper;
    using Fitness.BLL.DTO;
    using Fitness.BLL.DTO.Identity;
    using Fitness.DTO;
    using Fitness.Entities;
    using Fitness.Models;
    using Fitness.ViewModels.Shared;

    public class IngredientProfile : Profile
    {
        public IngredientProfile()
        {

            CreateMap<Ingredient, IngredientDTO>().ReverseMap();
            CreateMap<IngredientDTO, IngredientViewModel>().ReverseMap();
            CreateMap<Ingredient, IngredientVModel>().ReverseMap();
            CreateMap<Ingredient, NutritionIngredientViewModel>().ReverseMap();
        }
    }
}