﻿using AutoMapper;
using Fitness.Entities;
using Fitness.Extensions.MapperExtenstions;
using Fitness.ViewModels;
using Fitness.ViewModels.Nutrition;
using Fitness.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fitness.MapperProfiles
{
    public class NutritionProfile : Profile
    {
        public NutritionProfile()
        {

            CreateMap<NutritionIngredientDescriptionViewModel, NutritionIngredientDescription>()
                .ForMember(src => src.Ingredient, opt => opt.Ignore())
                .ForMember(src => src.NutritionMeal, opt => opt.Ignore())
                .ForMember(src => src.NutritionMealId, opt => opt.Ignore())
                            .ForMember(src => src.IngredientId, opt => opt.MapFrom(src=>src.NutritionIngredient.Id));

            CreateMap<NutritionIngredientDescription, NutritionIngredientDescriptionViewModel>()
                .ForMember(src => src.NutritionIngredient, opt => opt.MapFrom(src => src.Ingredient))
                .ForMember(src => src.MeasureType, opt => opt.MapFrom(src => src.MeasureType))
                .ForMember(src => src.MeasureTypeabbreviation, opt => opt.MapFrom(src => src.Measure.Abbreviation))
                .ForMember(src=>src.Count, opt=>opt.MapFrom(src=>src.Count));

            CreateMap<NutritionMealViewModel, NutritionMeal>()
                .ForMember(d => d.NutritionIngredientDescriptions, opt => opt.MapFrom(src=>src.NutritionIngredientDescriptions))
                .ForMember(d => d.NutritionMealCategoryId, opt => opt.MapFrom(r => r.NutritionMealCategoryId))
                .ForMember(d=>d.NutritionMealCategory, opt=>opt.Ignore())

                //.ForMember(d => d.NutritionMealCategoryId, opt => opt.Ignore())
                .ForMember(d => d.UsersLike, opt => opt.Ignore())
                .ForMember(d => d.ImageName, opt => opt.MapFrom(src => src.ImageName))
                ;


            CreateMap<NutritionMeal, NutritionMealViewModel>()
                .ForMember(d => d.NutritionMealCategory, opt => opt.MapFrom(src => src.NutritionMealCategory.Name))
                .ForMember(d => d.NutritionMealCategoryId, opt => opt.MapFrom(src => src.NutritionMealCategoryId))
                .ForMember(d=>d.NutritionIngredientDescriptions, opt=>opt.MapFrom(src=>src.NutritionIngredientDescriptions))

                ;


      

            //.ForMember((d) => d.NutritionMealDirections, opt =>
            //    opt.Ignore()); 

            CreateMap<NutritionMealCategoryViewModel, NutritionMealCategory>()
                .ForMember(src => src.NutritionMeals, opt => opt.Ignore())
                .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(src => src.ImageName, opt => opt.MapFrom(src => src.ImageUrl));

            CreateMap<NutritionMealCategory, NutritionMealCategoryViewModel>()
                .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(src => src.ImageUrl, opt => opt.MapFrom(src => src.ImageName))
                ;
        }
    }
}