﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Extensions.MapperExtenstions
{
    public static class MapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreAllVirtualsProps<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> expression)
        {
            var desType = typeof(TDestination);

            foreach (var property in desType.GetProperties().Where(p => p.GetGetMethod().IsVirtual))
            {
                expression.ForMember(property.Name, opt => opt.Ignore());
            }

            return expression;
        }
    }
}
