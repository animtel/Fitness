import { Component, OnInit } from '@angular/core';

import { Workout } from '../models/workout';
import { WorkoutCategoryService } from '../workout-category.service';
import { WorkoutType } from '../models/Workout/workouttype';
import { WorkoutCategory } from '../models/Workout/workoutcategory';
import { WorkoutTypeService } from '../workout-type.service';
import { NotificationsService } from 'angular2-notifications';
import { WorkoutServiceService } from '../services/workout-service/workout-service.service';


@Component({
  selector: 'app-workout-add',
  templateUrl: './workout-add.component.html',
  styleUrls: ['./workout-add.component.css']
})
export class WorkoutAddComponent implements OnInit {
  createMode: boolean = true;
  editMode: boolean;
  selectCategory: string;
  workout: Workout;
  workoutCategories: WorkoutCategory[];
  workouttypes: WorkoutType[];
  constructor(private workoutServiceService: WorkoutServiceService,
    private workoutCategoryService: WorkoutCategoryService,
    private workoutTypeService: WorkoutTypeService,
    private alertService: NotificationsService) {

  }
  Load(model: Workout) {
    // console.log("Load ", model);
    this.workoutCategories.forEach(category => {

      let type = {};
      category.workoutTypes.forEach(type => {
        if (type.id == model.workoutTypeId) {
          model.workoutTypeId = type.id;
          this.selectCategory = category.id;
          this.workouttypes = category.workoutTypes;
        }

      });

    });

    this.workout = model;
    this.createMode = false;
    this.editMode = true;
  }

  ngOnInit() {
    this.workout = {
      id: "",
      name: "",
      premium: false,
      workoutDescription: "",
      workoutTimeAbc: "",
      workoutType: undefined,
      workoutTypeId: ""
    };
    this.workoutCategories = [{
      id: "",
      name: "",
      workoutTypes: []
    }];
    this.workoutCategoryService.getall().subscribe((workoutCategories: WorkoutCategory[]) => {
      this.workoutCategories = workoutCategories;
      console.log("Getall categories", this.workoutCategories);
    }, error => {
      this.alertService.warn("Failed load");
    });
  }

  change(id: String): void {
    this.workouttypes = this.workoutCategories.find(x => x.id == id).workoutTypes;
  }

  addWorkout() {
    this.workoutServiceService.create(this.workout).subscribe(r => {
      this.alertService.success("Created");
      this.createMode = false;
    }, err => {
      this.alertService.warn("Failed to create");
    });
  }
  saveWorkout() {
    this.workoutServiceService.updateWorkout(this.workout).subscribe(r => {
      this.alertService.success("Saved");
      this.cleanIngredientForm();
    }, err => {
      this.alertService.warn("Failed to saved");
    });
  }
  cleanIngredientForm(): void {
    this.workout = {
      name: undefined,
      id: undefined,
      premium: false,
      workoutDescription: "",
      workoutTimeAbc: undefined,
      workoutType: undefined,
      workoutTypeId: undefined
    };
    this.editMode = false;
    this.createMode = true;
    // this.alertService.info("Cleaned");
  }
  // deleteWorkout() {
  //   this.workoutServiceService.delete(this.workout.id).subscribe(r => {
  //     this.alertService.success("Deleted");
  //     this.cleanIngredientForm()
  //   }, err => {
  //     this.alertService.warn("Failed deleted");
  //   });
  // }

}
