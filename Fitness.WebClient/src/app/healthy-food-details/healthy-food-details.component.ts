import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HealthyFood } from '../models/healthy-food';
import { AppSettings } from '../app-settings';


@Component({
  selector: 'app-healthy-food-details',
  templateUrl: './healthy-food-details.component.html',
  styleUrls: ['./healthy-food-details.component.css']
})
export class HealthyFoodDetailsComponent implements OnInit {


  healthy: HealthyFood;
  host: string = AppSettings.serverUrl;
  @Output() out: EventEmitter<HealthyFood>;

  constructor() { 

    this.out = new EventEmitter<HealthyFood>();
  }
  Load(healthyDto: any) {
    // this.healthy = model;
    this.healthy = healthyDto;
    console.log("Load ", healthyDto);
  };

  ngOnInit() {

  }
  clear(){
    this.healthy =  undefined;
  }

}
