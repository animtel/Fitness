import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthyFoodDetailsComponent } from './healthy-food-details.component';

describe('HealthyFoodDetailsComponent', () => {
  let component: HealthyFoodDetailsComponent;
  let fixture: ComponentFixture<HealthyFoodDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthyFoodDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthyFoodDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
