import { Http } from '@angular/http';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpHelper } from "../common/http-helper.common";
import { HealthyFood, HealthyFoodFactory, HealthyFoodGrouped } from "../models/healthy-food";
import { MeasureType } from "../pipes/typefilter.pipe";
import { DndModule } from "ng2-dnd";
import { AppSettings } from "../app-settings";
import { Ingredient, ExtentedIngredient, IngredientFactory } from "../models/ingredient";
import { Category } from "../models/category";
import { IngredientHealthyFoodDescriptionFactory, IngredientHealthyFoodDescription } from "../models/ingredient-description";
import { FileUploader } from "ng2-file-upload";
import { Measure } from '../models/measure';
import { MeasureService } from '../measure.service';
import { NotificationsService } from 'angular2-notifications';
import { HealthyCategoryService } from '../services/healthy-service/healthy-category.service';

@Component({
  selector: 'app-healthy-food-result',
  templateUrl: './healthy-food-result.component.html',
  styles: [`

.label-custom{

  width: 100px;
}
  .card-title .list-group-item{

    float:left;
  }
  .list-group-item {
    height:100%;
    
  }
  .panel-body {
    padding: 15px;
  }
  .drag-panel {
    content: "Ingredient";
  }
  .food .btn-group {
    
        display: flex; 
      }
    
  .food .btn-group button {

    width: 100%; 
  }
.card-img-top{
  height:80px;
  overflow: hidden;
  object-fit: cover;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: 50% 50%;
}
.card-img-top img {
  width: 500px;
  margin: -50% auto -25%;
}
  `]
})
export class HealthyFoodResultComponent extends HttpHelper implements OnInit {


  // uploader: FileUploader = new FileUploader(
  //   {
  //     method: 'POST',
  //     url: AppSettings.serverUrl + `healthyfood/food/createimage`,
  //     itemAlias: 'image'
  //   }
  // );
  @Output() out: EventEmitter<HealthyFood>;
  
  createMode: boolean = true;
  editMode: boolean = false;
  imageUrl: string;
  ingredientdescr: IngredientHealthyFoodDescription;
  food: HealthyFood;
  ingredients: Array<ExtentedIngredient>;
  categories: Array<Category>;
  host: string = AppSettings.serverUrl;
  foods: Array<HealthyFood>;
  //measures: Measure[];
  groupedFoods: Array<HealthyFoodGrouped>;


  constructor(protected http: Http, protected alertService: NotificationsService, private healthyCategoryService: HealthyCategoryService) {
    super('healthyfood/food', http);
    this.getUrl += '/getall';
    this.getUrlById += '/get/';
    this.out = new EventEmitter<HealthyFood>();
  }
  // private setImage(image:string):void {
  //   this.imageUrl = `${AppSettings.serverUrl}${image}`;
  //   this.food.imageName = image;
  // }
  ngOnInit(): void {
    this.getFoods();
  }
  detail(healthyId:string): void {
 
    this.getBydIdObservable(healthyId).map(m=>m.json()).subscribe(res=>
      {
        this.out.emit(res as HealthyFood); 
        this.alertService.success("Ok",res);
      },
       error=>
       {
        this.alertService.error("Error");
       })
  }

  // waitResponse(observable): void {
  //   observable
  //     .map(res => res.json())
  //     .subscribe(res => res, err => {
  //       this.alertService.error("Error", err);
  //     });
  // }
  // groupBy(myArray: HealthyFood[]) {

  //   var groups = {};
  //   for (var i = 0; i < myArray.length; i++) {
  //     var groupName = myArray[i].mealCategory;
  //     if (!groups[groupName]) {
  //       groups[groupName] = [];
  //     }
  //     groups[groupName].push(myArray[i]);
  //   }
  //   myArray = [];
  //   this.groupedFoods = new Array<HealthyFoodGrouped>();
  //   for (let groupName in groups) {
  //     this.groupedFoods.push({ group: groupName, arr: groups[groupName], hiden: false , imageUrl: ""  } as HealthyFoodGrouped);
  //   }

  // }

  // getFoods(): void {
  //   // this.spinner = this.rotateSpinner();
  //   this.getObservable()
  //     .map(res => res.json())
  //     .subscribe(res => { this.foods = res; this.groupBy(this.foods); console.log(this.groupedFoods); },
  //     err => {
  //       this.alertService.error("Error", err);
  //     });
  // }

  getFoods(): void {
    // this.spinner = this.rotateSpinner();
    this.healthyCategoryService.getallfoodforcategories().subscribe(res => {
      this.groupedFoods = res;
      console.log(res);
      this.alertService.success("Ok");
    },
      err => {
        this.alertService.error("Error", err);
      });

  }
  // getImage(index) {
  //   return this.images[index];
  // }
  hide(index): void {

    if (this.groupedFoods[index].hiden)
      this.groupedFoods[index].hiden = false
    else
      this.groupedFoods[index].hiden = true;
    console.log(this.groupedFoods[index]);
  }

}
