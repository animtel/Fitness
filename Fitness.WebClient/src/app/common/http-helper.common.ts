﻿import { AppSettings } from './../app-settings';
import { Http, Headers, RequestOptions } from '@angular/http';
import { authHttpServiceFactory } from '../auth/auth.module';
import { NotificationsService } from 'angular2-notifications';
export class HttpHelper {

    protected createUrl: string;
    protected updateUrl: string;
    protected getUrl: string;
    protected getUrlById: string;
    protected deleteUrl: string;
    protected searchUrl: string;

    constructor(protected prefix: string, protected http: Http) {
        this.createUrl = this.updateUrl = this.getUrl = this.deleteUrl = this.getUrlById = this.searchUrl = AppSettings.serverUrl + prefix;
        
    }

    protected getConsoleDefaultDelegateResponse() {
        return res => {this.update(); console.log(res.statusText); return res; };
    }

    protected getConsoleDefaultDelegateError() {
        return  err =>  {this.update(); console.log(err); return err; };
    }

    protected update() {}

    protected consoleMap(http) {
       return http.map(this.getConsoleDefaultDelegateResponse(), this.getConsoleDefaultDelegateError());
    }
    protected getAuthHeaders(){
        var headers = new Headers();
        headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');

        var token = localStorage.getItem("token");
        if (token !== "") {
            let tokenValue = 'Bearer ' + token;
            headers.append('Authorization', tokenValue);
        }
        return headers;
    }
    getObservable() {

        return this.http.get(this.getUrl, new RequestOptions({headers:this.getAuthHeaders()}));
    }

    getBydIdObservable(id) {
        return this.http.get(this.getUrlById + id,new RequestOptions({headers:this.getAuthHeaders()}));
    }

    createObservable(data) {
        return this.consoleMap(this.http.post(this.createUrl, data,new RequestOptions({headers:this.getAuthHeaders()})));
    }

    updateObservable(data) {
        return this.consoleMap(this.http.put(this.updateUrl, data,new RequestOptions({headers:this.getAuthHeaders()})));
    }

    deleteObservable(id) {
        return this.consoleMap(this.http.delete(this.deleteUrl + '/' + id,new RequestOptions({headers:this.getAuthHeaders()})));
    }

}
