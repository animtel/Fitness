import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Meal } from "../models/meal";
import { HttpHelper } from '../common/http-helper.common';

@Component({
  selector: 'app-nutrition-result',
  templateUrl: './nutrition-result.component.html',
  styleUrls: ['./nutrition-result.component.css']
})
export class NutritionResultComponent extends HttpHelper{

meals: Meal[];

  ngOnInit() {
    this.getObservable().subscribe(res => {this.meals = res.json(); console.log(res.json()); });
  }

  constructor(protected http: Http) {
    super('nutritionmeal', http);
    this.getUrl += '/getall';
  }
}
