import { TestBed, inject } from '@angular/core/testing';

import { WorkoutCategoryService } from './workout-category.service';

describe('WorkoutCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkoutCategoryService]
    });
  });

  it('should be created', inject([WorkoutCategoryService], (service: WorkoutCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
