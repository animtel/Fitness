import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AuthGuardService } from './auth-guard.service';
import { IdentityService } from './identity.service';
import { HttpModule, RequestOptions, Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { IdentityComponent } from './identity/identity.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({headerName:'Authorization',
   tokenName: 'Bearer ',
   tokenGetter: (() => localStorage.getItem('token')),
   globalHeaders: [{'Content-Type':'application/json'}]  }),
   http,
   options);
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
  ],
  providers: [IdentityService, {
          provide: AuthHttp,
          useFactory: authHttpServiceFactory,
          deps: [Http]
      }, AuthGuardService],
  declarations: [IdentityComponent]
})
export class AuthModule { }
