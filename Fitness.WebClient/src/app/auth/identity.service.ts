import { AppSettings } from './../app-settings';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthHttp, JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class IdentityService {

    public redirectUrl: string|null;

    private headers: Headers;
    private options: RequestOptions;
    private user: any;

    constructor (private authHttp: AuthHttp, private http: Http) {
      this.headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded' });
    }

    public encodeParams (params: any): string {
      let body = '';

      for (let key in params) {

        if (body.length) {
          body += '&';
        }
        body += key + '=' + encodeURIComponent(params[key]); 
      }

        return body;
    }

    public signin(username: string, password: string) {
        let tokenEndpoint: string = AppSettings.loginUrl;
  
        let params: any = {
            grant_type: AppSettings.GRANT_TYPE,
            username: username,
            password: password
        };

        let body: string = this.encodeParams(params);

        return this.http.post(AppSettings.loginUrl, body, this.options).map((res) => {
          const result: any = res.json();
          if (result.access_token) {
            this.store(result);
            console.log(result.access_token);
          }
        });
    }

    public isAuth(): boolean {
        return !!localStorage.getItem('token');
    }

    public register(email: string, password: string): any {
        console.log(email);
        return this.http.post(AppSettings.registerUrl, {email: email, password: password});
    }

    private store(body: any) {
      localStorage.setItem('token', body.access_token);
      localStorage.setItem('userName', body.userName);
     // localStorage.setItem('refresh_token', body.refresh_token);
      //this.decodeToken();
    }
}
