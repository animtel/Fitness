import { IdentityService } from './../identity.service';
import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http/http";
import { AppSettings } from "../../app-settings";

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['./identity.component.css']
})
export class IdentityComponent implements OnInit {
  registerMode:boolean;
  user: any;
  registerUser: any;

   constructor(private identity: IdentityService) { }   
   ngOnInit() {
    // this.http.post(AppSettings.serverUrl + 'api/Account/ExternalLogins?returnUrl=%2F&generateState=true',null).map(res=>res.json()).subscribe(
    //   res => { console.log(res); },
    //   err => err
    // );
    

    this.user =  {
      username: 'TestTest@gmail.com',
      password: 'Test1234',
    };
    this.registerUser = {
      email: '',
      password: ''
    };
  }

  signin(): void {
    console.log(this.user);
    this.identity.signin(this.user.username, this.user.password)
      .subscribe();
  }

  register(): void {
    console.log(this.registerUser);
    this.identity.register(this.registerUser.email, this.registerUser.password)
      .subscribe();
   }

   isAuth() {
     alert('Authenticated ' + this.identity.isAuth());
   }
}
