import { Injectable } from '@angular/core';
import { HttpHelper } from './common/http-helper.common';
import { Http } from '@angular/http';
import { WorkoutType } from './models/Workout/workouttype';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkoutTypeService extends HttpHelper {

  constructor(protected http: Http) {
    super('workouttype', http);
    this.getUrl += '/getall';

  }
  getall(): Observable<WorkoutType[]> {
    return super.getObservable().map(res => res.json());
  }

}
