import { Ingredient, IngredientFactory } from './ingredient';
import { IngredientNutritionMealDescription, NutritionIngredientDescription } from "./ingredient-description";

export interface Meal {
    name?: string;
    yield?: number;
    directions?: string;
    imageName?: string;
    prepTime?: number; // minutes
    cookTime?: number; // minutes

    nutritionIngredientDescriptions?: Array<NutritionIngredientDescription>;
    nutritionMealCategory ?: string;
    nutritionMealCategoryId ?: string;
    creditTo?:string;
}

export class MealFactory {
    static createMeal(): Meal {
        return {
            creditTo:"",
            name: null,
            yield: null,
            imageName: null,
            directions: "",
            nutritionIngredientDescriptions: [{
                count:0,
                check:false, measure:undefined,
                measureId: "",
                measureType:null,
                nutritionIngredient: IngredientFactory.initHealthyFood()
            }],
            prepTime: null,
            cookTime: null,
            nutritionMealCategory : null,
            nutritionMealCategoryId : null
            
        };
    }
}
