import { WorkoutType } from "./workouttype";

export interface WorkoutCategory {
    name: string;
    id:string;
    workoutTypes:WorkoutType[];
}