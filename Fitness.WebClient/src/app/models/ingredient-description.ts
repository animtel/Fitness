import { ExtentedIngredient} from "./ingredient";
import { Measure } from "./measure";


export interface NutritionIngredientDescription {
    nutritionIngredient: ExtentedIngredient;
    measureType: number;
    measure: Measure;
    measureId:string;
    count: number;
    check: boolean;
}

export interface IngredientHealthyFoodDescription {
    ingredient: ExtentedIngredient;
    measureType: number;
    count: number;
    check: boolean;
}
export class IngredientHealthyFoodDescriptionFactory {
    public static initDescriptionFood(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): IngredientHealthyFoodDescription {
        return {
            ingredient: ingredient,
            measureType: measureType,
            count: count,
            check:true
        };
    }

    public static initNutritionDescriptionMeal(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): NutritionIngredientDescription {
        return {
            nutritionIngredient: ingredient,
            measureType: measureType,
            measure:undefined,
            measureId:undefined,
            count: count,
            check:true
        };
    }
    public static initNutritionDescriptionMealArray(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): Array<NutritionIngredientDescription> {
        return [{
            nutritionIngredient: ingredient,
            measureType: measureType,
            count: count,
            check:true,
            measure:undefined,
            measureId:undefined
        }];
    }
}
export interface IngredientNutritionMealDescription {
    nutritionIngredient : ExtentedIngredient;
    measureType: number;
    count: number;
    id: string;
}
