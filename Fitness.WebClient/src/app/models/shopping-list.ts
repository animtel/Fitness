import { Meal } from './meal';

export interface ShoppingList {
    purchased: boolean;
    meal: Meal;
}



