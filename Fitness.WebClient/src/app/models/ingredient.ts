export class Ingredient {
    name: string;
}


export interface ExtentedIngredient {
    id: string;
    name: string;
    calories: number;
    fats: number;
    polyunsaturatedfattyacids: number;
    monounsaturatedfattyacids: number;
    saturatedFat: number;
    sodium: number;
    potassium: number;
    carbohydrates: number;
    dietaryfiber: number;
    sugar: number;
    cholesterol: number;
    proteins: number;
}

export class IngredientFactory {
    public static initHealthyFoodArray(): Array<ExtentedIngredient> {


        return [ this.initHealthyFood()];
    }
    public static initHealthyFood(): ExtentedIngredient {

                return {
                    id: '',
                    name: '',
                    calories: 0,
                    fats: 0,
                    polyunsaturatedfattyacids: 0,
                    monounsaturatedfattyacids: 0,
                    saturatedFat: 0,
                    sodium: 0,
                    potassium: 0,
                    carbohydrates: 0,
                    dietaryfiber: 0,
                    sugar: 0,
                    cholesterol: 0,
                    proteins: 0
                };
            }
}



