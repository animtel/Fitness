﻿
import { HealthyFoodResultComponent } from './healthy-food-result/healthy-food-result.component';
import { NutritionResultComponent } from './nutrition-result/nutrition-result.component';
import { CanActivate, Routes } from '@angular/router/';
import { AuthGuardService } from './auth/auth-guard.service';
import { IdentityComponent } from './auth/identity/identity.component';
import { Component } from '@angular/core';
import { WorkoutResultComponent } from './workout-result/workout-result.component';
import { WorkoutAddComponent } from './workout-add/workout-add.component';
import { HealthyFoodDetailsComponent } from './healthy-food-details/healthy-food-details.component';



export class AppSettings {

    static routes:Routes = [{
        path: 'nutrition-result',
        component: NutritionResultComponent
    },
    {
        path: 'healthy-result',
        component: HealthyFoodResultComponent

    },
    {
        path: 'healthy-detail',
        component: HealthyFoodDetailsComponent

    },
    {
        path: 'healthy-detail/:name',
        component: HealthyFoodDetailsComponent

    },
    {
        path: 'identity',
        component: IdentityComponent
    },
    
     {
        path: '**',
        redirectTo: '/identity',
        pathMatch: 'full'
    }
];

    //static serverUrl = `http://localhost:61171/`;
    //static serverUrl = 'http://localhost:59999/';
    static serverUrl = 'http://duxteamfitnessapplication.azurewebsites.net/';
    static loginUrl = AppSettings.serverUrl + 'Token';
    static registerUrl = AppSettings.serverUrl + 'api/Account/Register';
    static GRANT_TYPE = 'password';

}
