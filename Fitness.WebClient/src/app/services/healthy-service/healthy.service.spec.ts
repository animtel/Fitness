import { TestBed, inject } from '@angular/core/testing';

import { HealthyService } from './healthy.service';

describe('HealthyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HealthyService]
    });
  });

  it('should be created', inject([HealthyService], (service: HealthyService) => {
    expect(service).toBeTruthy();
  }));
});
