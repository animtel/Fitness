import { TestBed, inject } from '@angular/core/testing';

import { HealthyCategoryService } from './healthy-category.service';

describe('HealthyCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HealthyCategoryService]
    });
  });

  it('should be created', inject([HealthyCategoryService], (service: HealthyCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
