import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHelper } from '../../common/http-helper.common';
import { HealthyFoodGrouped } from '../../models/healthy-food';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HealthyCategoryService extends HttpHelper  {

  constructor(protected http: Http) {
    super('healthyfood/category', http);
    this.getUrl += '/getallfoodforcategories';
  }
  getall(): Observable<HealthyFoodGrouped[]> {
    return super.getObservable().map(res => res.json());
  }
  getallfoodforcategories(): Observable<HealthyFoodGrouped[]> {
    return super.getObservable().map(res => res.json());
  }
}
