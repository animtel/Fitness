import { Injectable } from '@angular/core';
import { HttpHelper } from '../../common/http-helper.common';
import { Http } from '@angular/http';
import { HealthyFood } from '../../models/healthy-food';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HealthyService extends HttpHelper {

  constructor(protected http: Http) {
    super('category', http);
    this.getUrl += '/getall';
    this.getUrlById += '/getbyid';
  }
  getall(): Observable<HealthyFood[]> {
    return super.getObservable().map(res => res.json());
  }
  
}
