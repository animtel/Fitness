import { IdentityService } from './auth/identity.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/';
import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable() export class Guard implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return !!localStorage.getItem('id_token');        
    }
}