﻿import { SafeHtml } from './pipes/safe-html.pipe';
import { MeasureType } from "./pipes/typefilter.pipe";
import { IdentityService } from './auth/identity.service';
import { Guard } from './app-auth';
import { AuthGuardService } from './auth/auth-guard.service';
import { AuthModule } from './auth/auth.module';
// import { NutritionCategoryComponent } from './nutrition-category/nutrition-category.component';
// import { HealthyCategoryComponent } from './healthy-category/healthy-category.component';

import { AppSettings } from './app-settings';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AppComponent } from './app.component';
import { NutritionResultComponent } from './nutrition-result/nutrition-result.component';
import { HealthyFoodResultComponent } from './healthy-food-result/healthy-food-result.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import {DndModule} from 'ng2-dnd';
import { PagerService } from './services/index';
import { WorkoutResultComponent } from './workout-result/workout-result.component';
import { WorkoutServiceService } from './services/workout-service/workout-service.service';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { HealthyCategoryService } from './services/healthy-service/healthy-category.service';
import { HealthyFoodDetailsComponent } from './healthy-food-details/healthy-food-details.component';
@NgModule({
  declarations: [ 
    AppComponent,
    NutritionResultComponent,
    HealthyFoodResultComponent,
    FileUploadComponent,
    SafeHtml,
    MeasureType,
    WorkoutResultComponent,
    HealthyFoodDetailsComponent

],
  imports: [
    DndModule.forRoot(),
    BrowserModule,
    FileUploadModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(AppSettings.routes),
    AuthModule,
    Ng2ImgMaxModule,
    SimpleNotificationsModule.forRoot()
  ],
  providers: [
    Guard,
    PagerService,
    WorkoutServiceService,
    HealthyCategoryService,
    // WorkoutCategoryService,WorkoutTypeService,
    // IngredientService,
    // MeasureService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
