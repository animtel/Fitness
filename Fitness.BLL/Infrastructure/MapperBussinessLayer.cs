﻿using Fitness.BLL.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Infrastructure
{
    public static class MapperBussinessLayer
    {
       public static IEnumerable<Type> GetProfiles()
       {
            return new List<Type>()
            {
             typeof(HealthyFoodDTOProfile),
             typeof(ShoppingDTOPProfile),
            };
        } 
    }
}
