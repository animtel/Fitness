﻿using Fitness.BLL.DTO;
using Fitness.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Infrastructure
{
    public interface IHealthyFoodService
    {
        IEnumerable<HealthyFoodCategoryDTO> GetAllCategories();

        void AddCategory(HealthyFoodCategoryDTO categoryDto);

        void UpdateCategory(HealthyFoodCategoryDTO foodDto);

        void DeleteCategoryById(Guid guid);

        /*-----------------------------------*/

        void ToCategory(HealthyFoodDTO food, HealthyFoodCategoryDTO category);
        void Edit(HealthyFoodDTO foodDto);
        void AddFood(HealthyFoodDTO foodDto);
        void AddRangeIngredientToFood(IEnumerable<Guid> ingredients, Guid foodId);
        HealthyFoodDTO GetFoodById(Guid guid);

        void UpdateFood(HealthyFoodDTO foodDto);

        void DeleteFoodById(Guid guid);

        IEnumerable<HealthyFoodDTO> GetAllHealthyFoods();

        IEnumerable<HealthyFoodDTO> GetAllNotPremiumHealthyFoods();
        void AddPictureToFood(Guid guid, string fileName);

        string GetImageNameById(Guid guid);

        //IEnumerable<HealthyFoodDTO> GetFoodByCategoryId(Guid guid);
        IEnumerable<dynamic> GetFoodByCategoryId(Guid guid);



        IEnumerable<dynamic> GetFoodByCategoryIdNotPremium(Guid guid);
        IEnumerable<dynamic> GetAllFoodByCategories(bool premium);
    }
}
