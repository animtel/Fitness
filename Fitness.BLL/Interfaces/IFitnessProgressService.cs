﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;
using Fitness.ViewModels.FitnessProgress;

namespace Fitness.BLL.Interfaces
{
    public interface IFitnessProgressService
    {
        void CreateOrUpdate(FitnessProgressWeekViewModel week, string userId);
        void Delete(Guid id);

        IEnumerable<FitnessProgressWeekViewModel> GetAllFitnessProgressWeeks(string userId);

    }
}
