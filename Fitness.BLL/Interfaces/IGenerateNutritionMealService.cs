﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;
using Fitness.ViewModels.Nutrition;

namespace Fitness.BLL.Interfaces
{
    public interface IGenerateNutritionMealService
    {
        void Create(GenerateNutritionMeal meal);
        void Check(Guid id, bool check);
        IEnumerable<GenerateNutritionMealViewModel> GetAll(FitnessUser user);
        void DeleteRange(Guid[] arr);
        IEnumerable<GenerateNutritionMeal> GenerateMealPlan(FitnessUser user);
    }
}
