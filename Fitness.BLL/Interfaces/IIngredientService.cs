﻿using Fitness.DTO;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Interfaces
{
    public interface IIngredientService
    {
        void Create(IngredientDTO ingredient);
        void Update(IngredientDTO ingredient);
        void Delete(IngredientDTO item);
        IngredientDTO Find(Guid Id);
        IEnumerable<IngredientDTO> GetAll(System.Linq.Expressions.Expression<Func<Ingredient, bool>> predicate);
        IEnumerable<IngredientDTO> GetAll();
    }
}
