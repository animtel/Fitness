﻿using Fitness.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.DTO.Identity;
using Fitness.Entities;
using Fitness.Models;
using Microsoft.AspNet.Identity;
using Fitness.BLL.Services;
using Fitness.Data.Identity;
using Fitness.ViewModels.Nutrition;

namespace Fitness.BLL.Interfaces
{
    public interface IAccountService
    {
        void CreateAsync(RegisterDTO dto);
        AppUserManager UserManager { get; set; }
        FitnessUser FindUser(string userName);
        FitnessUser FindUser(string userName, string password);
        Task SetDefaultData(RegisterDTO dto, IList<string> rolesListNames);
        bool Update(FitnessUser dto);
        FitnessUser GetCurrentUserById(string id);

        IEnumerable<NutritionMealViewModel> PickMeals(string userId);

        bool PickMeal(string userId, IEnumerable<Guid> mealId);
        bool PickMeal(string userId, Guid mealId, bool picked);

    }
}
