﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Interfaces
{
   public  interface IWorkoutTypeService
   {
       WorkoutTypeViewModel GetById(Guid Id);
       IEnumerable<WorkoutTypeViewModel> GetAllWorkoutTypes();
   }
}
