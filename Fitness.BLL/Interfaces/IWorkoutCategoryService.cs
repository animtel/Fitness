﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Interfaces
{
    public interface IWorkoutCategoryService
    {
        IEnumerable<WorkoutCategory> GetAllCategories();
        IEnumerable<WorkoutCategory> GetAllOnlyCategories();
        void Update(WorkoutCategoryViewModel workout);
        void Create(WorkoutCategoryViewModel workout);
        void Delete(Guid id);
        WorkoutExtCategoryViewModel GetSingle(string workoutCategoryName);
        //WorkoutCategory FindById(Guid id);
        //void Create(Workout workout);
        //void Delete(Workout workout);
        //void Update(Workout workout);
    }
}
