﻿
using Fitness.DTO;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Interfaces
{
    public interface IPickmealsService
    {
        void Get(string userId, Guid mealId);

        void Delete(string userId, Guid mealId);

        void Add(string userId, Guid mealId);

        IEnumerable<NutritionMeal> GetAll(string userId);
    }
}
