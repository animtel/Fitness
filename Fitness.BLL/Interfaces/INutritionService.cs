﻿using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels;

namespace Fitness.BLL.Interfaces
{
    public interface INutritionService
    {
        IEnumerable<IngredientDescription> GetAll();
        IngredientDescription Get(Guid Id);
        void Add(IngredientDescription nutrition);
        void Update(IngredientDescription nutrition);
        void Delete(IngredientDescription nutrition);
        void Delete(IEnumerable<IngredientDescription> nutritions);



        NutritionMealViewModel GetNutritionById(Guid Id);
        IEnumerable<NutritionMealCategoryViewModel> GetAllCategories();

        IDictionary<string, IEnumerable<NutritionMealViewModelMeta>> GetOnlyNameImageId();
        //IEnumerable<IDictionary<string, dynamic>> GenerateMealPlan(FitnessUser user);
        void UpdateNutrition(NutritionMealViewModel meal);
        IEnumerable<NutritionMealViewModel> GetAllNutrition();
        IEnumerable<NutritionMealViewModel> GetAllNotPremiumNutrition();
        void DeleteNutrition(Guid id);
        void CreateNutrition(NutritionMealViewModel meal);

    }
}
