﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DTO;
using Fitness.Entities;

namespace Fitness.BLL.Interfaces
{
    public interface IShoppingNutritionService
    {

            void DeleteById(Guid id);
            void Add(ShoppingNutritionItem shopDTO, FitnessUser user);
            IEnumerable<ShoppingNutritionItem> GetAll(FitnessUser currentUser);
            ShoppingNutritionItem GetById(FitnessUser user, Guid shopingItemId);
            void Update(FitnessUser user, ShoppingNutritionItem shopingitem);
            ShoppingNutritionItem ContainsIngredient(FitnessUser user, Guid ingredientId, Guid shopingItemId);
        
    }
}
