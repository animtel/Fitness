﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.Measure;

namespace Fitness.BLL.Interfaces
{
    public interface IMeasureService
    {

        void Create(Measure measure);
        void DeleteById(Guid id);
        IEnumerable<Measure> GetAll();
        void Update(Measure measure);

        Measure Get(Guid id);


    }
}
