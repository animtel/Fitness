﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Interfaces
{
    public interface IWorkoutService
    {
        IEnumerable<WorkoutViewModel> GetAllWorkouts();
        WorkoutViewModel FindById(Guid id);
        void Create(WorkoutViewModel workout);
        void Delete(Guid id);
        void Update(WorkoutViewModel workout);
    }
}
