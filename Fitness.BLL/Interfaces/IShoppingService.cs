﻿
using Fitness.DTO;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels.Shopping;

namespace Fitness.BLL.Interfaces
{
    public interface IShoppingService
    {

        #region Healthy
        void DeleteFromListById(Guid id);
        void AddToList(ShoppingItemVModel shop, FitnessUser user);
        //IEnumerable<ShoppingItemDTO> GetFromList(FitnessUser currentUser);

        ShoppingItemsViewModel GetFromList(FitnessUser user);
        ShoppingItemVModel Get(FitnessUser user, Guid shopingItemId);
        void Update(FitnessUser user, ShoppingItemVModel shopingitem);

        ShoppingItemVModel ContainsIngredient(FitnessUser user, Guid ingredientId, Guid shopingItemId);
        #endregion

        #region Nutrition

        void DeleteById(Guid id);
        void AddShoppingNutritionItem(ShoppingNutritionItem shopDTO, FitnessUser user);
        IEnumerable<ShoppingNutritionItem> GetAllShoppingNutritionItems(FitnessUser currentUser);
        ShoppingNutritionItem GetById(FitnessUser user, Guid shopingItemId);
        void Update(FitnessUser user, ShoppingNutritionItem shopingitem);

        ShoppingNutritionItem ContainsIngredientFormShoppingNutritionItem(FitnessUser user, Guid ingredientId, Guid shopingItemId);

        #endregion
    }
}
