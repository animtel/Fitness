﻿using Fitness.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.DTO.Identity;
using Fitness.DAL.Infrastructure;
using Fitness.Models;
using Fitness.Entities;
using Fitness.Exceptions;
using Fitness.Exceptions.BusinessLoginExceptions;
using Microsoft.AspNet.Identity;
using AutoMapper;
using Fitness.Data.Identity;
using Fitness.ViewModels.Nutrition;

namespace Fitness.BLL.Services
{
    public class AccountService : IAccountService
    {
        public IUnitOfWork _unitOfWork { get; set; }

        public AccountService()
        {
            
        }

        public AppUserManager UserManager {
            get => _unitOfWork?.UserManager;

            set => value = _unitOfWork?.UserManager;
        }
        public IEnumerable<NutritionMealViewModel> PickMeals(string userId)
        {

            return Mapper.Map<IEnumerable<NutritionMeal>, IEnumerable<NutritionMealViewModel>>(_unitOfWork.UserManager.PickMeals(userId));

        }
        public bool PickMeal(string userId, Guid mealId, bool picked)
        {

            _unitOfWork.UserManager.Pickmeal(userId, mealId, picked);
            
            _unitOfWork.SaveChanges();

            return picked;


        }

        public bool PickMeal(string userId, IEnumerable<Guid> mealIds)
        {

            bool picked = true;
            for (int i = 0; i < mealIds.Count(); i++)
            {
                _unitOfWork.UserManager.Pickmeal(userId, mealIds.ElementAt(i), picked);
            }



            _unitOfWork.SaveChanges();

            return picked;
        }

        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public FitnessUser FindUser(string userName)
        {
            FitnessUser user = _unitOfWork.UserManager.FindByName(userName);
            if (user == null)
            {
                throw new NullReferenceException(nameof(user));
            }
            return user;
        }

        //public static Data.Identity.AppUserManager Pickmeal(this Data.Identity.AppUserManager t, Guid mealId, string userId) {

        //    t.Pikc
        //    FitnessUser user = _unitOfWork.UserManager.FindById(userId);
        //    return false;
        //    //user.Pickmeals.Add();
        //}

        public FitnessUser FindUser(string userName, string password) {
            FitnessUser user = _unitOfWork.UserManager.Find(userName, password);
            if (user == null)
            {
                throw new NullReferenceException(nameof(user));
            }
            return user;
        }
        public void CreateAsync(RegisterDTO dto)
        {
            FitnessUser user = _unitOfWork.UserManager.FindByEmail(dto.Email);

            if (user == null)
            {
                user = new FitnessUser
                {
                    UserName = dto.Email,
                    Email = dto.Email
                };

                IdentityResult result =
                    _unitOfWork.UserManager.CreateAsync(user, dto.Password).Result;

                if (!result.Succeeded)
                {
                    throw IdentityException.IdentityRegisterFailure(dto, result.Errors);
                }
            }
            else
            {
                throw IdentityException.IdentityRegisterUserIsExist(dto);
            }
        }
        public bool Update(FitnessUser dto)
        {
            
            if (dto == null)
            {

                throw new NullReferenceException("user" + dto.Id);
            }
            //var USERPROFILE = Mapper.Map<ProfileDTO, FitnessUser>(dto);
                IdentityResult result =
                    _unitOfWork.UserManager.Update(dto);

                if (!result.Succeeded)
                {
                new IdentityException("Fail update profile");
            }
            _unitOfWork.SaveChanges();
            return true;
        }
        public async Task SetDefaultData(RegisterDTO dto, IList<string> roleListNames)
        {
            foreach (var roleName in roleListNames)
            {
                var role = 
                    await this._unitOfWork.RoleManager.FindByNameAsync(roleName);
                
                if (role == null)
                {
                    role = new AppRole()
                    {
                        Name = roleName
                    };

                    IdentityResult result = await _unitOfWork.RoleManager.CreateAsync(role);

                    if (!result.Succeeded)
                    {
                        throw IdentityException.IdentityRegisterFailure(dto, result.Errors);
                    }
                }
            }
            
            CreateAsync(dto);
        }

        public FitnessUser GetCurrentUserById(string id)
        {
            return _unitOfWork.UserManager.FindById(id);
        }


    }
}
