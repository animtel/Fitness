﻿using Fitness.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Fitness.DAL.Infrastructure;
using AutoMapper;
using Fitness.Entities;
using Fitness.DTO;

namespace Fitness.BLL.Services
{
    public class ShoppingNutritionService : IShoppingNutritionService
    {
        public IUnitOfWork _unitOfWork;

        public ShoppingNutritionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(ShoppingNutritionItem shopDTO, FitnessUser user)
        {

            if (shopDTO == null)
            {
                throw new ArgumentNullException(nameof(shopDTO));
            }

            this._unitOfWork.ShoppingNutritionRepository.Add(new ShoppingNutritionItem()
            {
                Id = shopDTO.Id,
                FitnessUserId = user.Id,
                QuantitativeValue = shopDTO.QuantitativeValue,
                Count = shopDTO.Count,
                //Ingredient = Mapper.Map<IngredientDTO, Ingredient>(shopDTO.Ingredient),
                IngredientId = shopDTO.Ingredient.Id
                //Ingredient = Mapper.Map<IngredientDTO, Ingredient>(shopDTO.Ingredient)
            });
            this._unitOfWork.SaveChanges();


        }


        public void DeleteById(Guid id)
        {
            this._unitOfWork.ShoppingNutritionRepository.Delete(_unitOfWork.ShoppingNutritionRepository.FindById(id));
            this._unitOfWork.SaveChanges();
        }


        public IEnumerable<ShoppingNutritionItem> GetAll(FitnessUser user)
        {


            return this._unitOfWork.ShoppingNutritionRepository.GetAllShoppingItemsByUser(user.Id);

            //return Mapper.Map<IEnumerable<ShoppingItem>, IEnumerable<ShoppingItemDTO>>(foods); 
        }



        public ShoppingNutritionItem GetById(FitnessUser user, Guid shopingItemId)
        {
            return this._unitOfWork.ShoppingNutritionRepository.GetShoppingNutritionItemByUser(user.Id, shopingItemId);

            //return Mapper.Map<ShoppingItem, ShoppingItemDTO>(food);
        }
        public ShoppingNutritionItem ContainsIngredient(FitnessUser user, Guid ingredientId, Guid shopingItemId)
        {
            return this._unitOfWork.ShoppingNutritionRepository.ContainsIngredient(user.Id, ingredientId, shopingItemId);

            //return Mapper.Map<ShoppingItem>(shop);
        }

        public void Update(FitnessUser user, ShoppingNutritionItem shopingitem)
        {
            var shop = this._unitOfWork.ShoppingListRepository.GetShoppingItemByUser(user.Id, shopingitem.Id);
            shop.Count = shopingitem.Count;
            shop.QuantitativeValue = shopingitem.QuantitativeValue;
            this._unitOfWork.ShoppingListRepository.Update(shop);
            this._unitOfWork.SaveChanges();


        }
    }
}
