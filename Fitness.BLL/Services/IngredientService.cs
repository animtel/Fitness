﻿using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.DTO;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Services
{
    public class IngredientService : IIngredientService
    {
        public IUnitOfWork _unitOfWork;

        public IngredientService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Create(IngredientDTO ingredient)
        {
            _unitOfWork.IngredientRepository.Add(Mapper.Map<IngredientDTO, Ingredient>(ingredient));
            _unitOfWork.SaveChanges();
        }

        public void Delete(IngredientDTO ingredient)
        {
            
            _unitOfWork.IngredientRepository.DeleteById(ingredient.Id);
            _unitOfWork.SaveChanges();
        }

        public void Update(IngredientDTO ingredient)
        {
            _unitOfWork.IngredientRepository.Update(Mapper.Map<IngredientDTO, Ingredient> (ingredient));
            _unitOfWork.SaveChanges();
        }
        public IngredientDTO Find(Guid Id) {

            
            return Mapper.Map<Ingredient, IngredientDTO>(_unitOfWork.IngredientRepository.FindById(Id)); 
        }
        public IEnumerable<IngredientDTO> GetAll(System.Linq.Expressions.Expression<Func<Ingredient, bool>> predicate) {


            return Mapper.Map<IEnumerable<Ingredient>, IEnumerable<IngredientDTO>>(_unitOfWork.IngredientRepository.GetAll(predicate));
             
        }
        public IEnumerable<IngredientDTO> GetAll()
        {
            return Mapper.Map<IEnumerable<Ingredient>, IEnumerable<IngredientDTO>>(_unitOfWork.IngredientRepository.GetAll());
        }
    }
}
