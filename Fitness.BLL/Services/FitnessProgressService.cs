﻿
using Fitness.DAL.Infrastructure.Repository;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.ViewModels;
using Fitness.Entities;
using Fitness.ViewModels.FitnessProgress;

namespace Fitness.BLL.Services
{

    public class FitnessProgressService : IFitnessProgressService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FitnessProgressService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public void CreateOrUpdate(FitnessProgressWeekViewModel week, string userId)
        {


            var weekentity = new FitnessProgressWeek()
            {
                FitnessUserId = userId,
                Id =  week.Id == Guid.Empty ? Guid.NewGuid(): week.Id,
                Title = week.Title,
                WeekNumber =  week.WeekNumber,
                Date = DateTime.UtcNow,
                UrlImage = week.UrlImage
            };
            _unitOfWork.FitnessProgressRepository.CreateOrUpdate(weekentity);
            _unitOfWork.SaveChanges();
        }

        public void Delete(Guid id)
        {
            _unitOfWork.FitnessProgressRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<FitnessProgressWeekViewModel> GetAllFitnessProgressWeeks(string userId)
        {
            return _unitOfWork.FitnessProgressRepository.GetAllProgressForUser(userId).Select(x=> new FitnessProgressWeekViewModel()
            {
                Id = x.Id,
                Title = x.Title,
                WeekNumber = x.WeekNumber,
                UrlImage = x.UrlImage + ".jpg",
                UrlSmallImage = x.UrlImage+ "-small.jpg",
                Date = x.Date
            });

        }

        
    }
}
