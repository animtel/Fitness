﻿using Fitness.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Fitness.DAL.Infrastructure;
using AutoMapper;
using Fitness.Entities;
using Fitness.DTO;
using Fitness.ViewModels.Shopping;

namespace Fitness.BLL.Services
{
    public class ShoppingService : IShoppingService
    {
        public IUnitOfWork _unitOfWork;

        public ShoppingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddToList(ShoppingItemVModel shop, FitnessUser user)
        {

            if (shop == null)
            {
                throw new ArgumentNullException(nameof(shop));
            }

            this._unitOfWork.ShoppingListRepository.Add(new ShoppingItem()
            {
                Id = shop.Id,
                FitnessUserId = user.Id,
                QuantitativeValue = shop.QuantitativeValue,
                Count = shop.Count,
                IngredientId = shop.IngredientId
            });
            this._unitOfWork.SaveChanges();


        }


        public void DeleteFromListById(Guid id)
        {


            this._unitOfWork.ShoppingListRepository.Delete(_unitOfWork.ShoppingListRepository.FindById(id));

            this._unitOfWork.SaveChanges();
        }

        public ShoppingItemsViewModel GetFromList(FitnessUser user)
        {

            
            var healthyShopping = Mapper.Map<IEnumerable<ShoppingItem>, IEnumerable<ShoppingItemVModel>>(this._unitOfWork.ShoppingListRepository.GetAllShoppingItemsByUser(user.Id));
            var nutritionsShopping = Mapper.Map<IEnumerable<ShoppingNutritionItem>, IEnumerable<ShoppingItemVModel>>(this._unitOfWork.ShoppingNutritionRepository.GetAllShoppingItemsByUser(user.Id));
            return new ShoppingItemsViewModel { Healthy = healthyShopping,Nutrition = nutritionsShopping }; 
        }

        public ShoppingItemVModel Get(FitnessUser user, Guid shopingItemId)
        {
            var food = this._unitOfWork.ShoppingListRepository.GetShoppingItemByUser(user.Id, shopingItemId);

            return Mapper.Map<ShoppingItem, ShoppingItemVModel>(food);
        }
        public ShoppingItemVModel ContainsIngredient(FitnessUser user, Guid ingredientId, Guid shopingItemId)
        {
            var shop = this._unitOfWork.ShoppingListRepository.ContainsIngredient(user.Id, ingredientId, shopingItemId);

            return Mapper.Map<ShoppingItemVModel>(shop);
        }


        public void Update(FitnessUser user, ShoppingItemVModel shopingitem)
        {
            var shop = this._unitOfWork.ShoppingListRepository.GetShoppingItemByUser(user.Id, shopingitem.Id);
            shop.Count = shopingitem.Count;
            shop.QuantitativeValue = shopingitem.QuantitativeValue;
            this._unitOfWork.ShoppingListRepository.Update(shop);
            this._unitOfWork.SaveChanges();


        }










        #region Nutrition
        public void DeleteById(Guid id)
        {
            _unitOfWork.ShoppingNutritionRepository.Delete(_unitOfWork.ShoppingNutritionRepository.FindById(id));
            _unitOfWork.SaveChanges();
        }

        public void AddShoppingNutritionItem(ShoppingNutritionItem shopDTO, FitnessUser user)
        {
            _unitOfWork.ShoppingNutritionRepository.Add(new ShoppingNutritionItem()
            {
                Id = shopDTO.Id,
                FitnessUserId = user.Id,
                QuantitativeValue = shopDTO.QuantitativeValue,
                Count = shopDTO.Count,
                IngredientId = shopDTO.Ingredient.Id
            });
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<ShoppingNutritionItem> GetAllShoppingNutritionItems(FitnessUser currentUser)
        {
            return _unitOfWork.ShoppingNutritionRepository.GetAllShoppingItemsByUser(currentUser.Id);
        }

        public ShoppingNutritionItem GetById(FitnessUser user, Guid shopingItemId)
        {
            return _unitOfWork.ShoppingNutritionRepository.GetShoppingNutritionItemByUser(user.Id, shopingItemId);
        }

        public void Update(FitnessUser user, ShoppingNutritionItem shopingitem)
        {
            var shop = this._unitOfWork.ShoppingNutritionRepository.GetShoppingNutritionItemByUser(user.Id, shopingitem.Id);
            shop.Count = shopingitem.Count;
            shop.QuantitativeValue = shopingitem.QuantitativeValue;

            this._unitOfWork.ShoppingNutritionRepository.Update(shop);
            this._unitOfWork.SaveChanges();
        }

        public ShoppingNutritionItem ContainsIngredientFormShoppingNutritionItem(FitnessUser user, Guid ingredientId,
            Guid shopingItemId)
        {
            return this._unitOfWork.ShoppingNutritionRepository.ContainsIngredient(user.Id, ingredientId, shopingItemId);
        }

        #endregion
    }
}
