﻿using Fitness.Entities;
using System;

namespace Fitness.BLL.Services
{
    public class ProfileDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }



        public int Age { get; set; }

        public decimal Height { get; set; }

        public HeightMeasureUnit HeightMeasuareUnit { get; set; }

        public decimal Weight { get; set; }

        public WeightMeasureUnit WeightMeasuareUnit { get; set; }

        public Sex Sex { get; set; }

        public Goal Goal { get; set; }

        public Activity ActivityLvl { get; set; }

        public decimal Calories { get; set; }
    }
}