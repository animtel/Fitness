﻿using Fitness.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.DTO;
using Fitness.DAL.Infrastructure;
using AutoMapper;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using Fitness.DTO;

namespace Fitness.BLL.Services
{
    public class HealthyFoodService : IHealthyFoodService
    {
        private readonly IUnitOfWork _unitOfWork;
        public HealthyFoodService(IUnitOfWork unitOfWork)
              {

            this._unitOfWork = unitOfWork;
        }

        #region Category

        public void AddCategory(HealthyFoodCategoryDTO categoryDto)
        {
            var category
                = Mapper.Map<HealthyFoodCategoryDTO, HealthyFoodCategory>(categoryDto);
            category.Id = Guid.NewGuid();
            this._unitOfWork.HealthyFoodBlockRepository.Add(category);
            this._unitOfWork.SaveChanges();
        }

        public IEnumerable<HealthyFoodCategoryDTO> GetAllCategories()
        {
            IEnumerable<HealthyFoodCategory> categories =
                this._unitOfWork.HealthyFoodBlockRepository.GetAll();

            IEnumerable<HealthyFoodCategoryDTO> categoriesDto =
                Mapper.Map<IEnumerable<HealthyFoodCategory>, IEnumerable<HealthyFoodCategoryDTO>>(categories);

            return categoriesDto;
        }

        public void UpdateCategory(HealthyFoodCategoryDTO categoryDto)
        {
            HealthyFoodCategory category =
                Mapper.Map<HealthyFoodCategoryDTO, HealthyFoodCategory>(categoryDto);

            this._unitOfWork.HealthyFoodBlockRepository.Update(category);
            this._unitOfWork.SaveChanges();
        }

        public void DeleteCategoryById(Guid guid)
        {
            this._unitOfWork.HealthyFoodBlockRepository.Delete(this._unitOfWork.HealthyFoodBlockRepository.FindById(guid));
            this._unitOfWork.SaveChanges();
        }
        #endregion

        #region HealthyFood
        public void AddFood(HealthyFoodDTO foodDto)
        {
            HealthyFood food = Mapper.Map<HealthyFoodDTO, HealthyFood>(foodDto);
            food.Id = Guid.NewGuid();

            var temp = new List<IngredientDescription>();

            foreach (var item in food.IngredientDescriptions.ToList())
            {
                temp.Add(

                   new IngredientDescription {
                       //Ingredient = _unitOfWork.IngredientRepository.FindById(item.Ingredient.Id),
                       Count = item.Count,
                       HealthyFoodId = item.HealthyFoodId,
                       Id = Guid.NewGuid(),
                       IngredientId = item.Ingredient.Id,
                       MeasureType = item.MeasureType
                   });
                    
            };

            food.IngredientDescriptions = temp;
            var category = _unitOfWork.HealthyFoodBlockRepository.FindByName(foodDto.MealCategory);
            food.HealthyFoodCategory = _unitOfWork.HealthyFoodBlockRepository.FindByName(foodDto.MealCategory);
            food.HealthyFoodCategoryId = category.Id;
            this._unitOfWork.HealthyFoodRepository.Add(food);
            _unitOfWork.SaveChanges();
        }

        public void Edit(HealthyFoodDTO foodDto)
        {
            HealthyFood food = this._unitOfWork.HealthyFoodRepository.FindById(foodDto.Id);
            this._unitOfWork.HealthyFoodRepository.Update(food);

            _unitOfWork.SaveChanges();
        }
        public void AddRangeIngredientToFood(IEnumerable<Guid> nutritionGuids, Guid nutritionId)
        {
            var nutrition = _unitOfWork.HealthyFoodRepository.FindById(nutritionId);
            foreach (var newId in nutritionGuids)
            {
                if (newId != Guid.Empty)
                {
                    nutrition.IngredientDescriptions.Add(_unitOfWork.IngredientDescriptionRepository.FindById(newId));
                }
            }
            _unitOfWork.HealthyFoodRepository.AddIngredientToFood(nutrition);
            _unitOfWork.SaveChanges();
        }
        public void DeleteFoodById(Guid guid)
        {
            this._unitOfWork.HealthyFoodRepository.Delete(_unitOfWork.HealthyFoodRepository.FindById(guid));
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<HealthyFoodDTO> GetAllHealthyFoods()
        {
            IEnumerable<HealthyFood> foods = this._unitOfWork.HealthyFoodRepository.GetAll();
            var dto = Mapper.Map<IEnumerable<HealthyFood>, IEnumerable<HealthyFoodDTO>>(foods);
            return dto;
        }
        public IEnumerable<HealthyFoodDTO> GetAllNotPremiumHealthyFoods()
        {
            IEnumerable<HealthyFood> foods = this._unitOfWork.HealthyFoodRepository.GetAll(x=>!x.Premium);
            var dto = Mapper.Map<IEnumerable<HealthyFood>, IEnumerable<HealthyFoodDTO>>(foods);
            return dto;
        }

        public HealthyFoodDTO GetFoodById(Guid guid)
        {
            
            HealthyFood food = this._unitOfWork.HealthyFoodRepository.FindById(guid);
            return Mapper.Map< HealthyFood,HealthyFoodDTO>(food);
        }

        public void ToCategory(HealthyFoodDTO foodDto, HealthyFoodCategoryDTO category)
        {
            var food = Mapper.Map<HealthyFoodDTO, HealthyFood>(foodDto);
            food.HealthyFoodCategory = this._unitOfWork.HealthyFoodBlockRepository.FindById(category.Id);

            this._unitOfWork.HealthyFoodRepository.Update(food);
            _unitOfWork.SaveChanges();
        }

        public void UpdateFood(HealthyFoodDTO foodDto)
        {
            HealthyFood food =
                 Mapper.Map<HealthyFoodDTO, HealthyFood>(foodDto);

            if (food.HealthyFoodCategoryId == Guid.Empty)
            {
                food.HealthyFoodCategoryId = _unitOfWork.HealthyFoodBlockRepository.FindByName(foodDto.MealCategory).Id;
                
            }

            
            this._unitOfWork.HealthyFoodRepository.Update(food);
            this._unitOfWork.SaveChanges();

        }

        public void AddPictureToFood(Guid guid, string fileName)
        {
            var food = this._unitOfWork.HealthyFoodRepository.FindById(guid);
            food.ImageName = "Content/" + fileName;
            _unitOfWork.HealthyFoodRepository.Update(food);
            _unitOfWork.SaveChanges();
        }

        public string GetImageNameById(Guid guid)
        {
            return this._unitOfWork.HealthyFoodRepository.FindById(guid).ImageName;
        }
        public IEnumerable<dynamic> GetAllFoodByCategories(bool premium)
        {
            IEnumerable<dynamic> foods = this._unitOfWork.HealthyFoodBlockRepository.GetCategoriesAllHealthyFoodLessIngredients(premium).Select(x => new {
                Id = x.Id,
                Name = x.Name,
                CategoryImage = x.Image ?? $"Content/{x.Name}.jpg",
                Foods = x.HealthyFoods.Select(r=> new { r.Id, r.Name, r.ImageName } )
            });

            return foods;
        }

        public IEnumerable<dynamic> GetFoodByCategoryId(Guid guid)
        {
            IEnumerable<dynamic> foods = this._unitOfWork.HealthyFoodRepository.GetAll(x => x.HealthyFoodCategory.Id == guid).Select(x=> new {
                Id = x.Id,
                Name = x.Name,
                Image = x.ImageName
            });

            return foods;
        }

        public IEnumerable<dynamic> GetFoodByCategoryIdNotPremium(Guid guid)
        {
            IEnumerable<dynamic> foods = this._unitOfWork.HealthyFoodRepository.GetAll(x => x.HealthyFoodCategory.Id == guid && !x.Premium).Select(x => new {
                Id = x.Id,
                Name = x.Name,
                Image = x.ImageName
            });

            return foods;
        }
        
        #endregion
    }
}
