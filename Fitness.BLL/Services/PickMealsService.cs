﻿using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.BLL.Services
{
    public class PickMealsService : IPickmealsService
    {
        public IUnitOfWork _unitOfWork;

        public PickMealsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(string userId, Guid mealId)
        {
            var allmeals = (List<NutritionMeal>)_unitOfWork.UserManager.PickMeals(userId);
            allmeals.Add(new NutritionMeal { Id = mealId });
            _unitOfWork.SaveChanges();

        }

        public void Delete(string userId, Guid mealId)
        {
            throw new NotImplementedException();
        }

        public void Get(string userId, Guid mealId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<NutritionMeal> GetAll(string userId)
        {
            return _unitOfWork.UserManager.PickMeals(userId);
        }
    }
}
