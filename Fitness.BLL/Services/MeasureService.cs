﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.Entities.Measure;

namespace Fitness.BLL.Services
{
    public class MeasureService: IMeasureService
    {

        private readonly IUnitOfWork _unitOfWork;
        public MeasureService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(Measure measure)
        {

            _unitOfWork.MeasureRepository.AddOrUpdate(measure);
            _unitOfWork.SaveChanges();
        }
        public void Update(Measure measure)
        {

            _unitOfWork.MeasureRepository.AddOrUpdate(measure);
            _unitOfWork.SaveChanges();
        }

        public Measure Get(Guid id)
        {
           return _unitOfWork.MeasureRepository.GetById(id);
        }

        public void DeleteById(Guid id)
        {
            _unitOfWork.MeasureRepository.DeleteById(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<Measure> GetAll()
        {
            return _unitOfWork.MeasureRepository.GetAll();
        }
    }
}
