﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;

namespace Fitness.BLL.Services
{
    public class ErrorService : IErrorService
    {
        private IUnitOfWork _unitOfWork;

        public ErrorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void LogError(Exception ex)
        {
            Error error = new Error()
            {
                Id = Guid.NewGuid(),
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                DateCreated = DateTime.UtcNow
            };

            _unitOfWork.ErrorRepository.Add(error);
            _unitOfWork.SaveChanges();
        }
    }
}
