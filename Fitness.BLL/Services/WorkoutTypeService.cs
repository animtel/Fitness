﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Services
{
    public class WorkoutTypeService: IWorkoutTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        public WorkoutTypeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Typeof(WorkoutType)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public WorkoutTypeViewModel GetById(Guid id)
        {
            
            return Mapper.Map<WorkoutType, WorkoutTypeViewModel>(_unitOfWork.WorkoutTypeRepository.FindById(id));
        }
        public IEnumerable<WorkoutTypeViewModel> GetAllWorkoutTypes()
        { 

            return Mapper.Map<IEnumerable<WorkoutType>, IEnumerable<WorkoutTypeViewModel>>(_unitOfWork.WorkoutTypeRepository.GetAll());
        }
    }
}
