﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using AutoMapper;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Services
{
    public class WorkoutCategoryService: IWorkoutCategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public WorkoutCategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(WorkoutCategoryViewModel workout)
        {
            workout.Id = Guid.NewGuid();

            _unitOfWork.WorkoutCategoryRepository.Add(Mapper.Map<WorkoutCategoryViewModel, WorkoutCategory>(workout));
            _unitOfWork.SaveChanges();
        }

        public void Update(WorkoutCategoryViewModel workout)
        {
            _unitOfWork.WorkoutCategoryRepository.Update(Mapper.Map<WorkoutCategoryViewModel, WorkoutCategory>(workout));
            _unitOfWork.SaveChanges();
        }

        public void Delete(Guid id)
        {
            _unitOfWork.WorkoutCategoryRepository.DeleteById(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<WorkoutCategory> GetAllCategories()
        {
            return _unitOfWork.WorkoutCategoryRepository.GetAll();
        }

        public WorkoutExtCategoryViewModel GetSingle(string workoutCategoryName)
        {

            return Mapper.Map<WorkoutCategory, WorkoutExtCategoryViewModel>(_unitOfWork.WorkoutCategoryRepository.GetAllSubcategory(workoutCategoryName.Trim().ToLower()));

            //return _unitOfWork.WorkoutBlockRepository.GetAllSubcategory(workoutCategoryName.Trim());
        }

        public IEnumerable<WorkoutCategory> GetAllOnlyCategories()
        {
            return _unitOfWork.WorkoutCategoryRepository.GetAllOnlyCategories();
        }
    }
}
