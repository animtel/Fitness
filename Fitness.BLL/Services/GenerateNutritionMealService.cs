﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using Fitness.ViewModels.Workout;
using Fitness.ViewModels;
using Microsoft.AspNet.Identity;

namespace Fitness.BLL.Services
{
    public class GenerateNutritionMealService : IGenerateNutritionMealService
    {
        private readonly IUnitOfWork _unitOfWork;
        public GenerateNutritionMealService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Check(Guid id, bool check)
        {
            _unitOfWork.GenerateNutritionMealRepository.Check(id, check);
            _unitOfWork.SaveChanges();
        }

        public void Create(GenerateNutritionMeal meal)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(Guid[] arr)
        {
            _unitOfWork.OffValidation();
            _unitOfWork.GenerateNutritionMealRepository.DeleteRange(arr);
            _unitOfWork.SaveChanges();
            _unitOfWork.OnValidation();
        }
        //public void Delete(WorkoutViewModel workout)
        //{
        //    _unitOfWork.WorkoutRepository.Delete(Mapper.Map<WorkoutViewModel, Workout>(workout));
        //    _unitOfWork.SaveChanges();
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Id">Workout Id</param>
        ///// <returns></returns>
        //public WorkoutViewModel FindById(Guid id)
        //{

        //   return Mapper.Map<Workout, WorkoutViewModel>(_unitOfWork.WorkoutRepository.FindById(id));
        //}

        public IEnumerable<GenerateNutritionMealViewModel> GetAll(FitnessUser user)
        {
            //var result = _unitOfWork.GenerateNutritionMealRepository
            //    .GetAll(user.Id);
            return Mapper.Map<IEnumerable<GenerateNutritionMeal>, IEnumerable<GenerateNutritionMealViewModel>>(_unitOfWork.GenerateNutritionMealRepository
                .GetAll(user.Id));
        }



        private Random _random;
        public IEnumerable<GenerateNutritionMeal> GenerateMealPlan(FitnessUser user)
        {

            _random = new Random();
            //var result = new List<IDictionary<string, dynamic>>();
            var pickmialsTemp = _unitOfWork.UserManager.PickMeals(user.Id);
            var calories = CalculateBmr(user);


            List<GenerateNutritionMeal> generateNutritionMealList = new List<GenerateNutritionMeal>();
            //Dictionary<int, List<GenerateNutritionMeal>> dict = new Dictionary<int, List<GenerateNutritionMeal>>();
            for (var i = 0; i < 7; i++)
            {
                List<GenerateNutritionMeal> generateNutritionMeals = new List<GenerateNutritionMeal>();
                GenerateByDay(ref i, _random, pickmialsTemp, user.Id, calories, ref generateNutritionMeals);
                generateNutritionMealList.AddRange(generateNutritionMeals);
                //dict.Add(i, generateNutritionMeals);
            }

            _unitOfWork.OffValidation();


            _unitOfWork.ShoppingNutritionRepository.DeleteRange(_unitOfWork.ShoppingNutritionRepository.GetAllShoppingItemsByUser(user.Id));


            foreach (var generateMeal in generateNutritionMealList)
            {
                _unitOfWork.GenerateNutritionMealRepository.AddOrUpdate(generateMeal);

                foreach (var generateMealCustomNutritionIngredientDescription in generateMeal.CustomNutritionIngredientDescriptions)
                {


                    _unitOfWork.ShoppingNutritionRepository.Add(
                    new ShoppingNutritionItem()
                    {
                            FitnessUserId = user.Id,
                            Id = Guid.NewGuid(),
                            Count = Convert.ToInt32(generateMealCustomNutritionIngredientDescription.Count * generateMealCustomNutritionIngredientDescription.Measure?.EqualsGram ?? 1),
                            IngredientId = generateMealCustomNutritionIngredientDescription.IngredientId
                    });
                }
            }
            _unitOfWork.UserManager.WeekExpiresMealPlan(user.Id);

            _unitOfWork.SaveChanges();
            _unitOfWork.OnValidation();

            //unitOfWork.NutritionMealRepository.Add(meal);
            //user.WeekExpiresMealPlan = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(DateTime.UtcNow, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
            //_unitOfWork.UserManager.Update(user);
            return generateNutritionMealList;
        }



        private NutritionMeal RandomMeal(Random random, List<NutritionMeal> pickmials)
        {
            return pickmials.ElementAt(random.Next(0, pickmials.Count()));
        }

        /// <summary>
        /// Увелич размер порции
        /// </summary>
        /// <param name="pmeal"></param>
        /// <param name="needcaloriesforfoodintake"></param>
        /// <param name="caloriesInSelectFood"></param>
        private double Stretch(ref NutritionMeal pmeal, double needcaloriesforfoodintake)
        {
            ////x.Count <-------------- x.MeasureType.EqualsGram
            var caloriesInSelectFood = (double)pmeal.NutritionIngredientDescriptions.Select(x => x.Count * (x.Measure?.EqualsGram ?? 1) * (double)x.Ingredient.Calories).Sum() / 100;

            var tempCalories = 0D;
            var rate = needcaloriesforfoodintake / caloriesInSelectFood;

            pmeal = new NutritionMeal
            {
                CookTime = pmeal.CookTime,
                Id = pmeal.Id,
                ImageName = pmeal.ImageName,
                Name = pmeal.Name,
                Premium = pmeal.Premium,
                PrepTime = pmeal.PrepTime,
                Yield = pmeal.Yield,
                NutritionMealCategoryId = pmeal.NutritionMealCategoryId,
                UsersLike = null,
                NutritionMealCategory = pmeal.NutritionMealCategory,
                NutritionIngredientDescriptions = pmeal.NutritionIngredientDescriptions.Select((ingredient) =>
                {
                    var calc = (double)((ingredient.Count * (ingredient.Measure?.EqualsGram ?? 1) * (double)ingredient.Ingredient.Calories) / 100) * rate;
                    tempCalories += calc;
                    return new NutritionIngredientDescription()
                    {
                        Id = ingredient.Id,
                        Ingredient = ingredient.Ingredient,
                        Count = Convert.ToInt32(calc),
                        Measure = ingredient.Measure,
                        MeasureId = ingredient.MeasureId,
                        MeasureType = ingredient.MeasureType,
                        IngredientId = ingredient.IngredientId
                    };
                }).ToList()
            };



            pmeal.NutritionMealCategory.NutritionMeals = null;


            return tempCalories;

        }


        private int GetIndex(string categoryName)
        {
            Dictionary<string, int> arr = new Dictionary<string, int>()
            {
                {"Breakfast",0},
                {"Snack 1",1},
                {"Lunch",2},
                {"Snack 2",3},
                {"Dinner",4}
            };
            return arr[categoryName];
        }

        private void GenerateByDay(ref int dayofweek, Random random, IEnumerable<NutritionMeal> pickmials, string userId, double calories, ref List<GenerateNutritionMeal> generateNutritionMeals)
        {

            List<NutritionMeal> temp = new List<NutritionMeal>(pickmials);

            IDictionary<string, dynamic> generatebyday = new Dictionary<string, dynamic>() {
                {"Breakfast", new { Calories = 0, Meal = new { } } },
                {"Snack 1", new { Calories = 0, Meal = new { } } },
                {"Lunch", new { Calories = 0, Meal = new { } } },
                {"Snack 2", new { Calories = 0, Meal = new { } } },
                {"Dinner", new { Calories = 0, Meal = new { } } }

            };

            var genkeys = generatebyday.Keys;
            for (var k = 0; k < genkeys.Count(); k++)
            {
                List<NutritionMeal> ttemp = new List<NutritionMeal>(temp);
                ttemp = ttemp.Where(x => x.NutritionMealCategory.Name == genkeys.ElementAt(k)).ToList();

                if (!ttemp.Any())
                {
                    break;

                }
                NutritionMeal pmeal = RandomMeal(random, ttemp);
                double procentsCalories = 0.10;
                switch (pmeal.NutritionMealCategory.Name.ToLower())
                {
                    case "breakfast":
                        procentsCalories = 0.25;
                        break;
                    case "snack 1":
                        break;
                    case "lunch":
                        procentsCalories = 0.25;
                        break;
                    case "snack 2":
                        break;
                    case "dinner":
                        procentsCalories = 0.30;
                        break;


                }
                var tempCalories = 0D;
                //Увелич размер порции
                tempCalories = Stretch(ref pmeal, calories * procentsCalories);

                //generatebyday[pmeal.NutritionMealCategory.Name] = new
                //{
                //    Calories = tempCalories,
                //    Meal = new NutritionMealViewModelMeta
                //    {
                //        Id = pmeal.Id,
                //        Name = pmeal.Name,
                //        ImageName = pmeal.ImageName
                //    }
                //};
                var generateMeal = new GenerateNutritionMeal
                {
                    NutritionMealId = pmeal.Id,
                    Calories = tempCalories,
                   
                    DayOfWeek = (DayOfWeek)dayofweek,
                    FitnessUserId = userId,
                    NutritionMealCategory = GetIndex(pmeal.NutritionMealCategory.Name),
                    Id = Guid.NewGuid(),
                    Name = pmeal.Name,
                    ImageName = pmeal.ImageName,
                    Premium = pmeal.Premium,
                    CookTime = pmeal.CookTime,
                    CreditTo = pmeal.CreditTo ?? "http://example.com",
                    Eat = false,
                    Yield = pmeal.Yield,
                    PrepTime = pmeal.PrepTime,
                    CustomNutritionIngredientDescriptions = pmeal.NutritionIngredientDescriptions.Select(
                        (x) =>


                            new CustomNutritionIngredientDescription()
                            {
                                Id = Guid.NewGuid(),
                                Count = x.Count,
                                MeasureType = x.MeasureType,
                                IngredientId = x.IngredientId,
                                MeasureId = x.MeasureId
                                //Ingredient = x.Ingredient

                            }).ToList()

                };




                //unitOfWork.GenerateNutritionMealRepository.AddOrUpdate(generateMeal);
                generateNutritionMeals.Add(generateMeal);
                tempCalories = 0;
            }
        }


        private static double CalculateBmr(FitnessUser user)
        {
            var activ = 1.2;
            int goal = 0;
            switch (user.ActivityLvl)
            {
                case Activity.Little:
                    activ = 1.2;
                    break;
                case Activity.Light:
                    activ = 1.375;
                    break;
                case Activity.Moderate:
                    activ = 1.55;
                    break;
                case Activity.Hard:
                    activ = 1.95;
                    break;
                default:
                    break;
            }
            switch (user.Goal)
            {
                case Goal.Loss:
                    goal = -500;
                    break;
                case Goal.Maintenanse:
                    goal = 0;
                    break;
                case Goal.Gain:
                    goal = 500;
                    break;

                default:
                    break;
            }
            if (user.Sex == Entities.Sex.Male)
            {


                return goal + (10.0 * user.Weight + 6.25 * user.Height - 5.0 * user.Age + 5.0) * activ;
            }
            else
            {
                return goal + (10.0 * user.Weight + 6.25 * user.Height - 5.0 * user.Age - 161.0) * activ;
            }
        }

        //public void Update(WorkoutViewModel workout)
        //{
        //    _unitOfWork.WorkoutRepository.Update(Mapper.Map<WorkoutViewModel, Workout>(workout));
        //    _unitOfWork.SaveChanges();
        //}
    }
}
