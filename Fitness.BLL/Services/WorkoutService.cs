﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.BLL.Interfaces;
using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using Fitness.ViewModels.Workout;

namespace Fitness.BLL.Services
{
    public class WorkoutService: IWorkoutService
    {
        private readonly IUnitOfWork _unitOfWork;
        public WorkoutService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Create(WorkoutViewModel workout)
        {
            workout.Id = Guid.NewGuid();

            _unitOfWork.WorkoutRepository.Add(Mapper.Map<WorkoutViewModel, Workout>(workout));
            _unitOfWork.SaveChanges();
        }

        public void Delete(Guid id)
        {
            _unitOfWork.WorkoutRepository.DeleteById(id);
            _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id">Workout Id</param>
        /// <returns></returns>
        public WorkoutViewModel FindById(Guid id)
        {
            
           return Mapper.Map<Workout, WorkoutViewModel>(_unitOfWork.WorkoutRepository.FindById(id));
        }

        public IEnumerable<WorkoutViewModel> GetAllWorkouts()
        {
            
            return Mapper.Map<IEnumerable<Workout>, IEnumerable<WorkoutViewModel>>(_unitOfWork.WorkoutRepository.GetAll());
        }

        public void Update(WorkoutViewModel workout)
        {
            _unitOfWork.WorkoutRepository.Update(Mapper.Map<WorkoutViewModel, Workout>(workout));
            _unitOfWork.SaveChanges();
        }
    }
}
