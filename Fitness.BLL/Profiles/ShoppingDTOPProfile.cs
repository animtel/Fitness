﻿using AutoMapper;
using Fitness.DTO;
using Fitness.Entities;
using Fitness.ViewModels.Shared;
using Fitness.ViewModels.Shopping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels;

namespace Fitness.BLL.Profiles
{
    public class ShoppingDTOPProfile : Profile
    {
        public ShoppingDTOPProfile()
        {
            //this.CreateMap<ShoppingItemViewModel, ShoppingItemDTO>();

            //CreateMap<ShoppingItemDTO, IngredientDescriptionViewModel>()
            //    .ForMember(src => src. IngredientDesc, opt => opt.MapFrom(src => src.First().Ingredient))
            //    .ForMember(src => src.Count, opt => opt.MapFrom(src => src.First().Count))
            //    .ForMember(src => src.QuantitativeValue, opt => opt.MapFrom(src => src.First().QuantitativeValue))
            //    .ForMember(x => x.IngredientId, opt => opt.MapFrom(src => src.First().IngredientId));

            CreateMap<ShoppingItemDTO, IngredientDescriptionViewModel>().ReverseMap();

            this.CreateMap<ShoppingItem, ShoppingItemDTO>().ReverseMap();


            this.CreateMap<ShoppingItem, ShoppingItemVModel>().ReverseMap();
            this.CreateMap<ShoppingNutritionItem, ShoppingItemVModel>().ReverseMap();

            //.ForMember(src => src.IngredientDescription,
            //           opt => opt.MapFrom(src => src.IngredientDescription))
            // .ForMember(t => t.FitnessUserId, opt => opt.MapFrom(x => x.FitnessUserId));
        }
    }
}
