﻿using AutoMapper;
using Fitness.BLL.DTO;
using Fitness.DTO;
using Fitness.Entities;
using Fitness.Entities.HealthyFoodBlock;
using Fitness.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels.Healthy;

namespace Fitness.BLL.Profiles
{
    public class HealthyFoodDTOProfile : Profile
    {
        public HealthyFoodDTOProfile()
        {
            this.CreateMap<HealthyFoodCategoryDTO, HealthyFoodCategory>()
                .ForMember(src => src.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(src => src.Image, opt => opt.MapFrom(src => src.ImageUrl))
                .ForMember(src => src.Id, opt => opt.MapFrom(src => src.Id));

            this.CreateMap<HealthyFoodCategory, HealthyFoodCategoryDTO>()
                .ForMember(src=>src.Name, opt=>opt.MapFrom(src=>src.Name))
                .ForMember(src => src.ImageUrl, opt => opt.MapFrom(src => src.Image))
                .ForMember(src => src.Id, opt => opt.MapFrom(src => src.Id));

            //CreateMap<IngredientDTO, Ingredient>();
            //CreateMap<Ingredient, IngredientDTO>();

            //this.CreateMap<Ingredient, IngredientVModel>().ReverseMap();
            CreateMap<IngredientDescription, IngredientDescriptionDTO>().ForMember(src=>src.MeasureTypeabbreviation, opt=>opt.MapFrom(src=>src.Measure.Abbreviation));
            CreateMap<IngredientDescriptionDTO,IngredientDescription>();
            CreateMap<HealthyFood, HealthyFoodDTO>()
                .ForMember(x => x.MealCategory, opt => opt.MapFrom(src => src.HealthyFoodCategory.Name));


            CreateMap<HealthyFoodDTO, HealthyFood>();

            CreateMap<HealthyFoodDTO, HealthyFoodViewModel>();

        }
    }
}
