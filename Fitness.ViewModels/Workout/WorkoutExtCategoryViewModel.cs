﻿using Fitness.ViewModels.Workout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Workout
{
    public class WorkoutExtCategoryViewModel:ViewModel
    {
        public string Name { get; set; }

        public ICollection<WorkoutTypeViewModel> WorkoutTypes { get; set; }
    }
}
