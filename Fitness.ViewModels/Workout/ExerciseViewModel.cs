﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Workout
{
    public class ExerciseViewModel: ViewModel
    {
        public string ExerciseName { get; set; }
        public string YouTubeLink { get; set; }
    }
}
