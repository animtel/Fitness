﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Workout
{
    public class WorkoutViewModel : ViewModel
    {
        public bool Premium { get; set; }
        public string StatusWorkout { get; set; }
        public string Name { get; set; }
        public TimeSpan WorkoutTimeAbc { get; set; }
        public string WorkoutDescription { get; set; }

        public Guid WorkoutTypeId { get; set; }
        public WorkoutTypeViewModel WorkoutType { get; set; }
    }
}
