﻿using System.Collections.Generic;
using Fitness.Entities;

namespace Fitness.ViewModels.Workout
{
    public class WorkoutTypeViewModel: ViewModel
    {

        public string Type { get; set; }

        public string Image { get; set; }

        public ICollection<WorkoutCategoryViewModel> WorkoutCategories { get; set; }

        public ICollection<WorkoutViewModel> Workouts { get; set; }
    }
}