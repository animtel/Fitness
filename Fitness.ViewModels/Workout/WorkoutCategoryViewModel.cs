﻿using System.Collections.Generic;
using Fitness.Entities;

namespace Fitness.ViewModels.Workout
{
    public class WorkoutCategoryViewModel : ViewModel
    {
        public string Name { get; set; }
        public string PreviewImageName { get; set; }
        public string ScreenImageName { get; set; }
    }
}