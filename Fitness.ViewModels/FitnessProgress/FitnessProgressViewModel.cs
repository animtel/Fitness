﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.FitnessProgress
{

    public class FitnessProgressWeekViewModel : ViewModel
    {
        public string Title { get; set; }
        public double WeekNumber { get; set; }
        public DateTime Date { get; set; }
        public string UrlImage { get; set; }
        public string UrlSmallImage { get; set; }
    }
}
