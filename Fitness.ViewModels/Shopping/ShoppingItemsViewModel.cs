﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Shopping
{
    public class ShoppingItemsViewModel
    {
        public IEnumerable<ShoppingItemVModel> Healthy { get; set; }
        public IEnumerable<ShoppingItemVModel> Nutrition { get; set; }
    }
}
