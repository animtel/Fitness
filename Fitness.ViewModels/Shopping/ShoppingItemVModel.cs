﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels.Shared;

namespace Fitness.ViewModels.Shopping
{

    public class ShoppingItemVModel : ViewModel
    {
        public Guid IngredientId { get; set; }
        public IngredientVModel Ingredient { get; set; }
        public int QuantitativeValue { get; set; }
        public int Count { get; set; }
        public string FitnessUserId { get; set; }
    }
}
