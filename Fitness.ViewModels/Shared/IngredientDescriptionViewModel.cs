﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.ViewModels.Nutrition;

namespace Fitness.ViewModels.Shared
{
    public class IngredientDescriptionViewModel : ViewModel
    {
        public IngredientViewModel Ingredient { get; set; }
        public Guid MeasureId { get; set; }
        public string MeasureTypeabbreviation { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }
    public class NutritionIngredientDescriptionViewModel : ViewModel
    {
        public NutritionIngredientViewModel NutritionIngredient { get; set; }
        public Guid MeasureId { get; set; }
        public string MeasureTypeabbreviation { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }
    public class CustomNutritionIngredientDescriptionViewModel : ViewModel
    {
        //public IngredientViewModel Ingredient { get; set; }
        public GenerateNutritionMealViewModel GenerateNutritionMeal { get; set; }
        public Guid MeasureId { get; set; }
        public string MeasureTypeabbreviation { get; set; }
        public MeasureType MeasureType { get; set; }
        public int Count { get; set; }
    }
}
