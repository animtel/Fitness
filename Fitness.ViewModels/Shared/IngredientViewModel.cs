﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Shared
{
    public class IngredientViewModel : ViewModel
    {
        public string Name { get; set; }
        public decimal Calories { get; set; }

        #region Characteristics of ingredient
        public decimal Fats { get; set; }
        public decimal Polyunsaturatedfattyacids { get; set; }
        public decimal Monounsaturatedfattyacids { get; set; }
        public decimal SaturatedFat { get; set; }

        //mg Натрий
        public decimal Sodium { get; set; }

        //mg Калий
        public decimal Potassium { get; set; }
        public decimal Carbohydrates { get; set; }

        //g Пищевые волокна
        public decimal Dietaryfiber { get; set; }
        public decimal Sugar { get; set; }

        public decimal Cholesterol { get; set; }

        public decimal Proteins { get; set; }
        #endregion
    }
    public class NutritionIngredientViewModel : IngredientViewModel
    {
  
    }
}
