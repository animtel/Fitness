﻿using Fitness.Entities;
using System;
using System.Collections.Generic;

namespace Fitness.ViewModels
{
    // Models returned by AccountController actions.
    public class ProfileViewModel
    {

        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }



        public int Age { get; set; }

        public double Height { get; set; }

        public HeightMeasureUnit HeightMeasuareUnit { get; set; }

        public double Weight { get; set; }

        public WeightMeasureUnit WeightMeasuareUnit { get; set; }

        public Sex Sex { get; set; }

        public Goal Goal { get; set; }

        public Activity ActivityLvl { get; set; }

        public double Calories { get; set; }
    }

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string UserId { get; set; }
        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
