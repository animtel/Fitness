﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;
using Fitness.ViewModels.Shared;

namespace Fitness.ViewModels
{
    public class ShoppingItemViewModel : ViewModel
    {
        public IngredientDescriptionViewModel Ingredient { get; set; }
    }
}
