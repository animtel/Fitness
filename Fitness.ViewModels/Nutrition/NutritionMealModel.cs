﻿using Fitness.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Nutrition
{
    public class NutritionMealViewModel : ViewModel
    {
        public NutritionMealViewModel() : base()
        {}
        public string CreditTo { get; set; }
        public bool Premium { get; set; }
        public string Name { get; set; }
        public int Yield { get; set; }
        //public string[] Directions { get; set; }
        public string ImageName { get; set; }
        public int PrepTime { get; set; }
        public int CookTime { get; set; }
        public string Directions { get; set; }
        public IEnumerable<NutritionIngredientDescriptionViewModel> NutritionIngredientDescriptions { get; set; }
        public string NutritionMealCategory { get; set; }
        public Guid NutritionMealCategoryId { get; set; }
    }



    public class GenerateNutritionMealViewModel : ViewModel
    {
        public DayOfWeek DayOfWeek { get; set; }
        public string FitnessUserId { get; set; }
        //public FitnessUser FitnessUser { get; set; }

        public string CreditTo { get; set; }


        public double Calories { get; set; }
        public bool Premium { get; set; }

        public string Name { get; set; }

        public int Yield { get; set; }

        public string ImageName { get; set; }

        public int PrepTime { get; set; }

        public int CookTime { get; set; }


        public bool Eat { get; set; }

        public int MealIndex { get; set; }

        public Guid NutritionMealId { get; set; }

        public ICollection<CustomNutritionIngredientDescriptionViewModel> CustomNutritionIngredientDescriptions { get; set; }

        //public ICollection<FitnessUser> GenerateForUsers { get; set; }
    }
}
