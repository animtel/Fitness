﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Nutrition
{
    public class NutritionMealCategoryModel : ViewModel
    {
        public NutritionMealCategoryModel() : base()
        { }

        public string Name { get; set; }
    }
}
