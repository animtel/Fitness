﻿using Fitness.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels.Healthy
{
    public class HealthyFoodViewModel : ViewModel
    {
        public HealthyFoodViewModel(): base() {
        }
        public string CreditTo { get; set; }
        public bool Premium { get; set; }
        public string Name { get; set; }

        public int Yield { get; set; }

        public string HealthyFoodDirections { get; set; }

        public int PrepTime { get; set; }

        public int CookTime { get; set; }

        public string ImageName { get; set; }

        public IEnumerable<IngredientDescriptionViewModel> IngredientDescriptions { get; set; }

        public string MealCategory { get; set; }
    }
}
