﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.ViewModels
{
    public class NutritionMealViewModelMeta
    {
        public string Name { get; set; }
        public string ImageName { get; set; }
        public Guid Id { get; set; }
    }
}
