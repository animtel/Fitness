﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Data.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Infrastructure.Repository;

namespace Fitness.DAL.Infrastructure
{
    public interface IUnitOfWork
    {
        AppUserManager UserManager { get; set; }

        AppRoleManager RoleManager { get; set; }

        IHealthyFoodBlockRepository HealthyFoodBlockRepository { get; set; }
        INutritionMealBlockRepository NutrtionBlockRepository { get; set; }

        IHealthyFoodRepository HealthyFoodRepository { get; set; }
        INutritionMealRepository NutritionMealRepository { get; set; }

        IIngredientRepository IngredientRepository { get; set; }
        IIngredientDescriptionRepository IngredientDescriptionRepository { get; set; }
        IMineralRepository MineralRepository { get; set; }
        IVitaminRepository VitaminRepository { get; set; }
        IFitnessProgressRepository FitnessProgressRepository { get; set; }
        IWorkoutBlockRepository WorkoutCategoryRepository { get; set; }
        IWorkoutRepository WorkoutRepository { get; set; }
        IWorkoutTypeRepository WorkoutTypeRepository { get; set; }

        IShoppingRepository ShoppingListRepository { get; set; }
        IShoppingNutritionRepository ShoppingNutritionRepository { get; set; }
        //IPickmealsRepository PickmealsRepository { get; set; }

        IErrorRepository ErrorRepository { get; set; }

        IGenerateNutritionMealRepository GenerateNutritionMealRepository { get; set; }

        IMeasureRepository MeasureRepository { get; set; }

        int SaveChanges();



        void OffValidation();

        void OnValidation();

    }
}
