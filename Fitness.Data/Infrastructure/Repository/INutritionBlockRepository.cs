﻿using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface INutritionMealBlockRepository : IRepository<NutritionMealCategory>
    {
        NutritionMealCategory FindByName(string name);
    }
}
