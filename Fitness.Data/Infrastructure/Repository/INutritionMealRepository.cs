﻿using Fitness.Entities;
using Fitness.ViewModels.Nutrition;
using System;
using System.Collections.Generic;
using Fitness.ViewModels;

namespace Fitness.DAL.Infrastructure
{
    public interface INutritionMealRepository : IRepository<NutritionMeal>
    {
        //void Add(NutritionMealModel model);
        //void Update(NutritionMealModel model);
        //IEnumerable<NutritionMealModel> GetMealsByCategory(NutritionMealCategoryModel model);
        //IEnumerable<NutritionMealModel> GetAll();

        //void AddToCategory(NutritionMealCategoryModel model, Guid guid);
        IEnumerable<NutritionMealViewModel> GetAllMeals();
        IEnumerable<NutritionMealViewModel> GetAllMeals(System.Linq.Expressions.Expression<Func<NutritionMeal, bool>> predicate);
        NutritionMealViewModel GetMealById(Guid Id);
        Dictionary<string, IEnumerable<NutritionMealViewModelMeta>> GetOnlyNameImageId();
        IEnumerable<NutritionMeal> GetMealsByCategory(NutritionMealCategory model);
        void AddToCategory(NutritionMealCategory model, Guid guid);
        void DeleteById(Guid id);
    }
}