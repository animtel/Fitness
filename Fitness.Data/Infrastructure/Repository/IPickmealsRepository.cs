﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IPickmealsRepository : IRepository<Pickmeals>
    {
        new IEnumerable<Pickmeals> GetAll(System.Linq.Expressions.Expression<Func<Pickmeals, bool>> predicate);
    }
}
