﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IShoppingRepository : IRepository<ShoppingItem>
    {
        IEnumerable<ShoppingItem> GetAllShoppingItemsByUser(string userId);
        ShoppingItem GetShoppingItemByUser(string userId, Guid shopingItemId);
        ShoppingItem ContainsIngredient(string userId, Guid ingredientId, Guid shopingItemId);
    }
    public interface IShoppingNutritionRepository : IRepository<ShoppingNutritionItem>
    {
        IEnumerable<ShoppingNutritionItem> GetAllShoppingItemsByUser(string userId);
        ShoppingNutritionItem GetShoppingNutritionItemByUser(string userId, Guid shopingnutritionItemId);
        ShoppingNutritionItem ContainsIngredient(string userId, Guid ingredientId, Guid shopingNutritionItemId);
    }
}
