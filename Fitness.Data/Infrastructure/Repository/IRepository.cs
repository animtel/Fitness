﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure
{
    public interface IRepository<T> where T: IEntity
    {
        void Add(T item);
        T FindById(Guid item);

        IEnumerable<T> GetAll(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        void AddRange(IEnumerable<T> item);
        void Update(T item);
        void Delete(T item);
        void DeleteRange(IEnumerable<T> item);
    }
}
