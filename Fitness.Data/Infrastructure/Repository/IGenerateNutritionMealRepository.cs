﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;

namespace Fitness.Data.Infrastructure.Repository
{
    public interface IGenerateNutritionMealRepository
    {
        IEnumerable<GenerateNutritionMeal> GetAll(string userId);
        void AddOrUpdate(GenerateNutritionMeal generateNutritionMeal);
        void DeleteRange(Guid[] guids);
        void Check(Guid id, bool check);
    }
}
