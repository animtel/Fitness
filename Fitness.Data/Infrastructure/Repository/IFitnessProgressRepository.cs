﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Data.Infrastructure.Repository
{
    public interface IFitnessProgressRepository
    {

        IEnumerable<FitnessProgressWeek> GetAllProgressForUser(string userId);
        void CreateOrUpdate(FitnessProgressWeek week);


        void Delete(Guid id);



    }
}
