﻿using Fitness.Entities;

namespace Fitness.DAL.Infrastructure
{
    public interface IMineralRepository : IRepository<Mineral>
    {
    }
}