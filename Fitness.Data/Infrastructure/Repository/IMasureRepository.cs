﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.Measure;

namespace Fitness.Data.Infrastructure.Repository
{
    public interface IMeasureRepository
    {
        void DeleteById(Guid id);
        IEnumerable<Measure> GetAll();
        Measure GetById(Guid id);
        void AddOrUpdate(Measure measure);
    }
}
