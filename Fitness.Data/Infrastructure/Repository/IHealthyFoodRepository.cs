﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using Fitness.Entities;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Infrastructure
{
    public interface IHealthyFoodRepository : IRepository<HealthyFood>
    {
        HealthyFood FindByName(string name);
        void AddIngredientToFood(HealthyFood hfood);
        void Update(HealthyFood item);

    }
}