﻿using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.DAL.Infrastructure.Repository
{
    public interface IWorkoutBlockRepository : IRepository<WorkoutCategory>
    {
        IEnumerable<WorkoutCategory> GetAllOnlyCategories();
        WorkoutCategory GetAllSubcategory(string workoutCategory);
        void DeleteById(Guid id);
        //WorkoutCategory GetAllSubcategory(string workoutCategory, bool getworkouts);
    }
}
