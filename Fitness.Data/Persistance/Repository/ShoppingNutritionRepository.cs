﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Fitness.DAL;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;

namespace Fitness.Data.Persistance.Repository
{
    public class ShoppingNutritionRepository : Repository<ShoppingNutritionItem>, IShoppingNutritionRepository
    {
        public IEnumerable<ShoppingNutritionItem> GetAllShoppingItemsByUser(string userId)
        {
            var userShoppingItems = DbContext.ShoppingNutritionItems
                        .Include(x => x.FitnessUser)
                        .Include(x => x.Ingredient)
                        //.Include(x => x.FitnessUser.ShoppingItems)
                        .Where(x => x.FitnessUserId == userId);
                        //.GroupBy(x => x.Ingredient.Id).ToDictionary(x => x.FirstOrDefault().Ingredient.Name);
                        

            return userShoppingItems;
        }
        public ShoppingNutritionItem GetShoppingNutritionItemByUser(string userId, Guid shopingItemId)
        {
            var userShoppingItems = DbContext.ShoppingNutritionItems
                        .Include(x => x.FitnessUser)
                        .Include(x => x.Ingredient)
                .FirstOrDefault(x=>x.Id == shopingItemId && x.FitnessUserId == userId);

            return userShoppingItems;
        }
        public ShoppingNutritionItem ContainsIngredient(string userId, Guid ingredientId, Guid shopingItemId)
        {
            return DbContext.ShoppingNutritionItems.Include(x => x.Ingredient).SingleOrDefault(x => x.Id == shopingItemId && x.FitnessUserId == userId && x.IngredientId == ingredientId);
        }
        
        public ShoppingNutritionRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
