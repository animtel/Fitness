﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Fitness.DAL;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Data.Entity.Migrations;
using Fitness.Data.Infrastructure.Repository;
using Fitness.Entities.Measure;

namespace Fitness.Data.Persistance.Repository
{
    public class MeasureRepository : Repository<Measure>, IMeasureRepository
    {
        public override IEnumerable<Measure> GetAll()
        {
            return DbContext.Measures;
        }

        public Measure GetById(Guid id)
        {
            return DbContext.Measures.Find(id);
        }
        public void DeleteById(Guid id)
        {
            DbContext.Measures.Remove(DbContext.Measures.Find(id));
        }
        public void AddOrUpdate(Measure measure)
        {
            DbContext.Measures.AddOrUpdate(measure);
        }
        public MeasureRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
