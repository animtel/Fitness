﻿
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using AutoMapper;
using Fitness.Data.Persistance.Repository;
using Fitness.ViewModels.Shared;

namespace Fitness.DAL.Persistance.Repository
{
    public class IngredientRepository : Repository<Ingredient>, IIngredientRepository
    {
        public Ingredient FindByName(string name)
        {
            return GetAll(x=>x.Name == name).SingleOrDefault();
        }

        public void DeleteById(Guid id)
        {

            DbContext.Set<Ingredient>().Remove(DbContext.Set<Ingredient>().Find(id));
        }

        public IngredientRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
