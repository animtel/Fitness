﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.Data.Persistance.Repository;
using Fitness.ViewModels;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Persistance.Repository
{
    public class HealthyFoodRepository : Repository<HealthyFood>, IHealthyFoodRepository
    {
        public HealthyFood FindByName(string name)
        {
            return DbContext.Set<HealthyFood>().FirstOrDefault(x => x.Name == name);
        }

        public void AddIngredientToFood(HealthyFood hfood) {

            DbContext.Set<HealthyFood>().Attach(hfood);
        }

        public override IEnumerable<HealthyFood> GetAll()
        {
            return DbContext.Set<HealthyFood>()
                //.Include(x => x.HealthyFoodDirections)
                .Include(x => x.IngredientDescriptions)
                .Include(x=>x.IngredientDescriptions.Select(s=>s.Measure))
                .Include(x => x.IngredientDescriptions.Select(r=>r.Ingredient))
                .Include(x => x.HealthyFoodCategory);
        }
        public override IEnumerable<HealthyFood> GetAll(System.Linq.Expressions.Expression<Func<HealthyFood, bool>> predicate)
        {
            return DbContext.Set<HealthyFood>()
                //.Include(x => x.HealthyFoodDirections)
                .Include(x => x.IngredientDescriptions)
                .Include(x => x.IngredientDescriptions.Select(r => r.Ingredient))
                .Include(x => x.HealthyFoodCategory).Where(predicate);
        }


        public override HealthyFood FindById(Guid id)
        {
            return DbContext.Set<HealthyFood>()
                //.Include(x => x.HealthyFoodDirections)
                .Include(x => x.IngredientDescriptions)
                .Include(x=>x.IngredientDescriptions.Select(r=>r.Ingredient))
                .Include(x => x.HealthyFoodCategory).FirstOrDefault(xd=>xd.Id == id);
        }

        /// <summary>
        /// TODO
        /// Does not work correctly
        /// </summary>
        /// <param name="item"></param>
        public override void Update(HealthyFood item)
        {

            //DbContext.Entry(item).Property(x => x.HealthyFoodCategoryId).IsModified = true;
            //DbContext.Entry(item.HealthyFoodCategory).State = EntityState.Detached;
            //DbContext.Entry(item.HealthyFoodCategory).State = EntityState.Detached;
            //DbContext.Set<HealthyFood>().Attach(item);
            //DbContext.Set<HealthyFood>().Attach(item);


            item.IngredientDescriptions = item.IngredientDescriptions.Select((x) =>
            {
                x.HealthyFoodId = item.Id;
                if (x.Id == Guid.Empty)
                {
                    //x.HealthyFood = item;
                    x.Ingredient = null;
                    x.Id = Guid.NewGuid();
                }
                DbContext.IngredientDescriptions.AddOrUpdate(x);
                return x;
            }).ToList();


            DbContext.HealthyFoods.AddOrUpdate(item);
            //DbContext.Entry(item).State = EntityState.Modified;
    

        }

        public HealthyFoodRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
