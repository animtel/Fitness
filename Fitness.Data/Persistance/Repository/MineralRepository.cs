﻿using Fitness.DAL.Infrastructure;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Persistance.Repository;

namespace Fitness.DAL.Persistance.Repository
{
    public class MineralRepository : Repository<Mineral>, IMineralRepository
    {
        public MineralRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
