﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using  System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Persistance.Repository;
using Fitness.DAL.Infrastructure;

namespace Fitness.DAL.Persistance.Repository
{
    public class WorkoutCategoryRepository : Repository<WorkoutCategory>, IWorkoutBlockRepository
    {
        public override IEnumerable<WorkoutCategory> GetAll()
        {
            var list = DbContext.WorkoutCategories.ToList();
            return list;
            //return DbContext
            //    .Set<WorkoutCategory>()
            //    .Include(x=>x.WorkoutTypes)
            //    .AsEnumerable()
            //    .Select(x => new WorkoutCategory() {
            //       Id =  x.Id,
            //       Name = x.Name,
            //       WorkoutTypes = x.WorkoutTypes
            //       .Select(r => new WorkoutType() {
            //           Id = r.Id,
            //           Type = r.Type
            //       }).ToList()
            //    });


        }

        public WorkoutCategory GetAllSubcategory(string workoutCategory)
        {
            return DbContext.Set<WorkoutCategory>().Include(x => x.WorkoutTypes).AsEnumerable().SingleOrDefault(x => x.Name.ToLower() == workoutCategory);
        }

        public void DeleteById(Guid id)
        {
            DbContext.Set<WorkoutCategory>().Remove(DbContext.Set<WorkoutCategory>().Find(id));
        }

        //public WorkoutCategory GetAllSubcategory(string workoutCategory, bool getworkouts)
        //{
        //    return getworkouts 
        //        ?
        //        DbContext.Set<WorkoutCategory>().Include(x => x.WorkoutTypes).AsEnumerable().SingleOrDefault(x => x.Name.ToLower() == workoutCategory)
        //        :
        //        DbContext.Set<WorkoutCategory>().Include(x => x.WorkoutTypes).Include(x => x.WorkoutTypes).AsEnumerable().SingleOrDefault(x => x.Name.ToLower() == workoutCategory);
        //}
        public IEnumerable<WorkoutCategory> GetAllOnlyCategories()
        {
            return DbContext.Set<WorkoutCategory>();
        }
        public WorkoutCategoryRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
