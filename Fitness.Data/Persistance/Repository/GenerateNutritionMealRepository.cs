﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Infrastructure.Repository;
using Fitness.DAL;
using Fitness.Entities;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Fitness.Data.Persistance.Repository
{
    public class GenerateNutritionMealRepository :Repository<GenerateNutritionMeal>, IGenerateNutritionMealRepository

    {
        public GenerateNutritionMealRepository(Context dbContext) : base(dbContext)
        {
            
        }

        public void Check(Guid id,bool check)
        {
        

            var item = DbContext.Set<GenerateNutritionMeal>().FirstOrDefault(x => x.Id == id);
            item.Eat = check;

            //DbContext.Set<GenerateNutritionMeal>().AddOrUpdate(item);
            //DbContext.Set<GenerateNutritionMeal>().Attach(item);
            DbContext.Entry(item)
                .Property(c => c.Eat).IsModified = true;
        }

        public IEnumerable<GenerateNutritionMeal> GetAll(string userid)
        {
            var result = DbContext.Set<GenerateNutritionMeal>()
                .Include(x=>x.CustomNutritionIngredientDescriptions)
                .Include(x=>x.CustomNutritionIngredientDescriptions.Select(er=>er.GenerateNutritionMeal))
                .Include(x=>x.CustomNutritionIngredientDescriptions.Select(rt=>rt.Ingredient))
                .Include(x => x.CustomNutritionIngredientDescriptions.Select(rt => rt.Measure)).
                Where(x=>x.FitnessUserId == userid).OrderBy(x=>x.DayOfWeek).ThenBy(qw=>qw.NutritionMealCategory).ToList();

            return result;
        }
        public void AddOrUpdate(GenerateNutritionMeal generateNutritionMeal)
        {

            DbContext.GenerateNutritionMeals.AddOrUpdate(x=>x.FitnessUserId, generateNutritionMeal);
        }

        public void DeleteRange(Guid[] guids)
        {

            foreach (var id in guids)
            {
                //var item = DbContext.GenerateNutritionMeals.Find(id);
                //DbContext.Entry(item).State = EntityState.Unchanged;
                //DbContext.GenerateNutritionMeals.Attach(item);

                DbContext.GenerateNutritionMeals.Remove(DbContext.GenerateNutritionMeals.Find(id));
                
                //DbContext.Entry(item).State = EntityState.Deleted;
            }
        }
    
    }
}
