﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Fitness.DAL;
using Fitness.DAL.Infrastructure;
using Fitness.Entities;

namespace Fitness.Data.Persistance.Repository
{
    public class Repository<T> : IRepository<T> where T: Entity
    {
        protected Context _dbContext;

        public Repository(Context dbContext)
        {
            _dbContext = dbContext;
        }
        
        protected Context DbContext
        {
            get => _dbContext;
            set => _dbContext = value;
        }

        public virtual void Add(T item)
        {

            DbContext.Set<T>().Add(item);
        }

        public virtual T FindById(Guid item)
        {
            return DbContext.Set<T>().FirstOrDefault(x => x.Id == item);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbContext.Set<T>();
        }
        public virtual IEnumerable<T> GetAll(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public virtual void AddRange(IEnumerable<T> item)
        {
            DbContext.Set<T>().AddRange(item);
        }

        public virtual void Delete(T item)
        {

            DbContext.Set<T>().Remove(item);

            //DbContext.Entry(item).State = EntityState.Deleted;
        }

        //public void DeleteById(Guid id)
        //{
        //    DbContext.Entry(new Entity(id)).State = EntityState.Deleted;
        //}
        public virtual void DeleteRange(IEnumerable<T> items)
        {
            DbContext.Set<T>().RemoveRange(items);
        }

        public virtual void Update(T item)
        {

            //var result = DbContext.Entry(item).State;

            //if(DbContext.Entry(item).State != EntityState.Added)
            //    DbContext.Set<T>().Attach(item);
                DbContext.Entry(item).State = EntityState.Modified;
        }
    }
}
