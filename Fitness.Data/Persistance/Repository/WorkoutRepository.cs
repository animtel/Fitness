﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Persistance.Repository;
using Fitness.DAL.Infrastructure;

namespace Fitness.DAL.Persistance.Repository
{
    public class WorkoutRepository : Repository<Workout>, IWorkoutRepository
    {
        public WorkoutRepository(Context dbContext) : base(dbContext)
        {
        }

        public void DeleteById(Guid id)
        {
            DbContext.Set<Workout>().Remove(DbContext.Set<Workout>().Find(id));
        }

        public override void Update(Workout item)
        {

            if (DbContext.Entry(item).State == EntityState.Unchanged)
                DbContext.Set<Workout>().Attach(item);

            DbContext.Entry(item).State = EntityState.Modified;
        }
    }
}
