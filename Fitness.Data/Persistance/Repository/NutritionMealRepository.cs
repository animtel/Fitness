﻿using Fitness.Entities;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.ViewModels.Nutrition;
using AutoMapper;
using Fitness.Data.Persistance.Repository;
using Fitness.ViewModels;
using Fitness.ViewModels.Shared;
using System.Data.Entity;
namespace Fitness.DAL.Persistance.Repository
{

    public class NutritionMealRepository : Repository<NutritionMeal>, INutritionMealRepository
    {
        public NutritionMealRepository(Context dbContext) : base(dbContext)
        {
        }
        public void AddToCategory(NutritionMealCategory model, Guid mealId)
        {
            var category = DbContext.Set<NutritionMealCategory>().SingleOrDefault(n => n.Name == model.Name);
            if (category == null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            var meal = base.FindById(mealId); 
            meal.NutritionMealCategoryId = category.Id;
            base.Update(meal);
        }

        public override void Add(NutritionMeal model)
        {
            model.Id = Guid.NewGuid();
            model.NutritionIngredientDescriptions = model.NutritionIngredientDescriptions.Select((x) =>
            {

                x.Id = Guid.NewGuid();
                return x;
            }).ToList();

            base.Add(model);
        }



        public Dictionary<string, IEnumerable<NutritionMealViewModelMeta>> GetOnlyNameImageId()
        {
            var r = DbContext.Set<NutritionMealCategory>().Select(x => new
            {
                CategoryName = x.Name,
                CategoryId = x.Id,
                Nutritions = x.NutritionMeals
            }).AsEnumerable().ToDictionary(x=>x.CategoryName, s =>s.Nutritions.Select(rr=> new NutritionMealViewModelMeta
            {
                Id = rr.Id,
                Name= rr.Name,
                ImageName = "http://duxteamfitnessapplication.azurewebsites.net/"+ rr.ImageName,
            }));

            return r;
            //return Mapper.Map<IQueryable<NutritionMeal>, IEnumerable<NutritionMealModel>>(meals);

        }
        public IEnumerable<NutritionMealViewModel> GetAllMeals()
        {

            var meals = DbContext.Set<NutritionMeal>()
                .Include(x => x.NutritionMealCategory)
                .Include(x => x.NutritionIngredientDescriptions)
                .Include(x => x.NutritionIngredientDescriptions.Select(rt => rt.Measure))
                .Include(x => x.NutritionIngredientDescriptions.Select(r => r.Ingredient));
       
            return Mapper.Map<IEnumerable<NutritionMeal>, IEnumerable<NutritionMealViewModel>>(meals);

        }
        public IEnumerable<NutritionMealViewModel> GetAllMeals(System.Linq.Expressions.Expression<Func<NutritionMeal, bool>> predicate)
        {

            var meals = DbContext.Set<NutritionMeal>()
                .Include(x => x.NutritionMealCategory)
                .Include(x => x.NutritionIngredientDescriptions)
                      .Include(x => x.NutritionIngredientDescriptions.Select(rt => rt.Measure))
                .Include(x => x.NutritionIngredientDescriptions.Select(r => r.Ingredient)).Where(predicate);


            return Mapper.Map<IEnumerable<NutritionMeal>, IEnumerable<NutritionMealViewModel>>(meals);

        }
        public NutritionMealViewModel GetMealById(Guid Id)
        {

            return Mapper.Map<NutritionMeal, NutritionMealViewModel>(DbContext.Set<NutritionMeal>()
                .Include(x => x.NutritionMealCategory)
                .Include(x => x.NutritionIngredientDescriptions)
                      .Include(x => x.NutritionIngredientDescriptions.Select(rt => rt.Measure))
                .Include(x => x.NutritionIngredientDescriptions.Select(r => r.Ingredient)).FirstOrDefault(x => x.Id == Id));

        }
        public IEnumerable<NutritionMeal> GetMealsByCategory(NutritionMealCategory model)
        {
            return DbContext.Set<NutritionMealCategory>()
                .Include(x=>x.NutritionMeals).


                SingleOrDefault(x => x.Name == model.Name).NutritionMeals;
            //return Mapper.Map<ICollection<NutritionMeal>, IEnumerable<NutritionMealModel>>(meals);
        }

        public override void Update(NutritionMeal model)
        {
            //var meal = Mapper.Map<NutritionMealModel, NutritionMeal>(model);
            
            DbContext.Set<NutritionMeal>().AddOrUpdate(model);
        }

        public void DeleteById(Guid id)
        {

            var deleteitem = DbContext.NutritionMeals.Include(x=>x.NutritionIngredientDescriptions).FirstOrDefault(x=>x.Id == id);

            DbContext.Entry(deleteitem).State = EntityState.Deleted;     
        }

        //private NutritionMeal ToEntity(NutritionMealModel model)
        //{
        //    var entity = Mapper.Map<NutritionMealModel, NutritionMeal>(model);

        //    Func<IngredientDescriptionViewModel, IngredientDescription> modelToDesc = (ingred) => new IngredientDescription()
        //    {
        //        Ingredient = DbContext.Set<Ingredient>().FirstOrDefault(i => i.Name == ingred.Ingredient.Name),
        //        Count = ingred.Count,
        //        MeasureType = ingred.MeasureType
        //    };

        //    entity.IngredientDescriptions = (from ingred in model.Ingredients select modelToDesc(ingred)).ToList();
        //    entity.NutritionMealDirections = (from el in model.Directions select new NutritionMealDirection() { Name = el }).ToList();
        //    entity.NutritionMealCategory = DbContext.Set<NutritionMealCategory>().SingleOrDefault(c => c.Name == model.Name);

        //    return entity;
        //}

        //private static NutritionMealModel ToModel(NutritionMeal meal)
        //{
        //    var model = Mapper.Map<NutritionMeal, NutritionMealModel>(meal);
        //    var stringDirections = (from el in meal.NutritionMealDirections select el.Name).ToArray();

        //    var ingredients = (from el in meal.IngredientDescriptions
        //                       select new IngredientDescriptionViewModel
        //                       {
        //                           Ingredient = Mapper.Map<IngredientViewModel>(el.Ingredient),
        //                           Count = el.Count,
        //                           MeasureType = el.MeasureType,
        //                       }).ToArray();

        //    model.MealCategory = meal.NutritionMealCategory.Name;
        //    model.Ingredients = ingredients;
        //    model.Directions = stringDirections;

        //    return model;
        //}


    }
}
