﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Data.Persistance.Repository;
using Fitness.DAL.Infrastructure;
using System.Data.Entity;
namespace Fitness.DAL.Persistance.Repository
{
    public class IngredientDescriptionRepository : Repository<IngredientDescription>, IIngredientDescriptionRepository
    {

        public override IngredientDescription FindById(Guid Id) {

            return DbContext.Set<IngredientDescription>().Include(x => x.Ingredient).Single(x=>x.Id == Id);
        }

        public override IEnumerable<IngredientDescription> GetAll()
        {
            return DbContext.Set<IngredientDescription>().Include(x=>x.Ingredient);
        }


        //public override void Update(IngredientDescription item)
        //{
             
        //    DbContext.Set<IngredientDescription>().Attach(item);
            
        //    DbContext.Entry(item).State = EntityState.Modified;
        //}

        public IngredientDescriptionRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
