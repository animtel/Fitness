﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Fitness.DAL;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;

namespace Fitness.Data.Persistance.Repository
{
    public class ShoppingRepository : Repository<ShoppingItem>, IShoppingRepository
    {
        public IEnumerable<ShoppingItem> GetAllShoppingItemsByUser(string userId)
        {
            var userShoppingItems = DbContext.ShoppingItems
                        .Include(x => x.FitnessUser)
                        .Include(x => x.Ingredient)
                        //.Include(x => x.FitnessUser.ShoppingItems)
                        .Where(x => x.FitnessUserId == userId);
                        //.GroupBy(x => x.Ingredient.Id).ToDictionary(x => x.FirstOrDefault().Ingredient.Name);
                        

            return userShoppingItems;
        }
        public ShoppingItem GetShoppingItemByUser(string userId, Guid shopingItemId)
        {
            var userShoppingItems = DbContext.ShoppingItems
                        .Include(x => x.FitnessUser)
                        .Include(x => x.Ingredient)
                .FirstOrDefault(x=>x.Id == shopingItemId && x.FitnessUserId == userId);

            return userShoppingItems;
        }
        public ShoppingItem ContainsIngredient(string userId, Guid ingredientId, Guid shopingItemId)
        {
            return DbContext.ShoppingItems.Include(x => x.Ingredient).SingleOrDefault(x => x.Id == shopingItemId && x.FitnessUserId == userId && x.IngredientId == ingredientId);
        }
        
        public ShoppingRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
