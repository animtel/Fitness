﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure;
using Fitness.ViewModels.Nutrition;
using AutoMapper;
using Fitness.Data.Persistance.Repository;

namespace Fitness.DAL.Persistance.Repository
{
    public class NutritionMealCategoryRepository : Repository<NutritionMealCategory>, INutritionMealBlockRepository
    {
       

        public void Add(NutritionMealCategoryModel model)
        {
            var category = Mapper.Map<NutritionMealCategoryModel, NutritionMealCategory>(model);
            
            base.Add(category);
        }

        public NutritionMealCategory FindByName(string name)
        {
            return DbContext.Set<NutritionMealCategory>().FirstOrDefault(x => x.Name == name);
        }
        public override IEnumerable<NutritionMealCategory> GetAll()
        {
            return DbContext.Set<NutritionMealCategory>();
        }

        public override void Update(NutritionMealCategory category)
        {
            base.Update(category);
        }


        public NutritionMealCategoryRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
