﻿using Fitness.DAL.Infrastructure;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Fitness.Data.Persistance.Repository;
using Fitness.ViewModels.Healthy;

namespace Fitness.DAL.Persistance.Repository
{
    public class HealthyFoodBlockRepository
        : Repository<HealthyFoodCategory>, IHealthyFoodBlockRepository
    {
        public HealthyFoodCategory FindByName(string name)
        {
            return DbContext.Set<HealthyFoodCategory>().FirstOrDefault(x => x.Name == name);
        }

        public IEnumerable<HealthyFoodCategory> GetCategoriesAllHealthyFoodLessIngredients(bool premium)
        {
            if (premium)
            {
                return DbContext.Set<HealthyFoodCategory>()
                    .Include(x => x.HealthyFoods);
            }

            return DbContext.Set<HealthyFoodCategory>()
                .Include(x => x.HealthyFoods.Where(r=>!r.Premium));

        }

        public HealthyFoodBlockRepository(Context dbContext) : base(dbContext)
        {
        }
    }
}
