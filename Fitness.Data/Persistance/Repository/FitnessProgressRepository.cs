﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL;
using Fitness.Entities;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Fitness.Data.Infrastructure.Repository;

namespace Fitness.Data.Persistance.Repository
{
    public class FitnessProgressRepository : Repository<FitnessProgressWeek> ,IFitnessProgressRepository
    {
        public FitnessProgressRepository(Context dbContext) : base(dbContext)
        {
        }

        public void CreateOrUpdate(FitnessProgressWeek week)
        {
            DbContext.Set<FitnessProgressWeek>().AddOrUpdate(week);
        }

        public void Delete(Guid id)
        {
            DbContext.Set<FitnessProgressWeek>().Remove(DbContext.Set<FitnessProgressWeek>().Find(id));
        }

        public IEnumerable<FitnessProgressWeek> GetAllProgressForUser(string userId)
        {
            var result = DbContext.Set<FitnessUser>().Include(x => x.UsersProgressWeekly).SingleOrDefault(x=>x.Id == userId).UsersProgressWeekly.OrderBy(x=>x.Date);
            return result;
        }
    }
}
