﻿using Fitness.DAL.Infrastructure.Repository;
using Fitness.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using  System.Data.Entity;
using Fitness.Data.Persistance.Repository;
using Fitness.DAL.Infrastructure;

namespace Fitness.DAL.Persistance.Repository
{
    public class WorkoutTypeRepository : Repository<WorkoutType>, IWorkoutTypeRepository
    {
        public WorkoutTypeRepository(Context dbContext) : base(dbContext)
        {
        }


        public override IEnumerable<WorkoutType> GetAll()
        {
            return DbContext.Set<WorkoutType>().Include(x => x.WorkoutCategories).Include(x => x.Workouts);
        }
    }
}
