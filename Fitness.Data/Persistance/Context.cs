﻿using Fitness.Entities;
using Fitness.Entities.HealthyFoodBlock;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities.Measure;

namespace Fitness.DAL
{
    public class Context : IdentityDbContext<FitnessUser>
    {
        
        public Context()
            : base("FitnessDB", throwIfV1Schema: false)
        {
        }
        public IDbSet<Error> Errors { get; set; }

        public IDbSet<HealthyFoodCategory> HealthyFoodCategories { get; set; }

        public IDbSet<HealthyFood> HealthyFoods { get; set; }

        public IDbSet<NutritionMealCategory> NutritionMealCategories { get; set; }
        public IDbSet<NutritionMeal> NutritionMeals { get; set; }

        public IDbSet<ShoppingItem> ShoppingItems { get; set; }
        public IDbSet<ShoppingNutritionItem> ShoppingNutritionItems { get; set; }

        public IDbSet<WorkoutCategory> WorkoutCategories { get; set; }
        public IDbSet<Workout> Workouts { get; set; }
        public IDbSet<Exercise> Exercises { get; set; }
        public IDbSet<DifficultyLevel> DifficultyLevels { get; set; }
        public IDbSet<WorkoutType> WorkoutTypes { get; set; }

        public IDbSet<IngredientDescription> IngredientDescriptions { get; set; }
        public IDbSet<NutritionIngredientDescription> NutritionIngredientDescriptions { get; set; }
        public IDbSet<Ingredient> Ingredients { get; set; }

        public IDbSet<Measure> Measures { get; set; }
        public IDbSet<Vitamin> Vitamins { get; set; }
        public IDbSet<Mineral> Minerals { get; set; }

        public IDbSet<GenerateNutritionMeal> GenerateNutritionMeals { get; set; }

        public IDbSet<CustomNutritionIngredientDescription> CustomNutritionIngredientDescriptions { get; set; }

        public IDbSet<FitnessProgressWeek> FitnessProgressWeeks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<FitnessUser>()
                .HasMany(x => x.ShoppingItems);



            //modelBuilder.Entity<CustomUserNutritionIngredientDescription>().HasKey(x => x.Id);
            //modelBuilder.Entity<CustomUserNutritionIngredientDescription>().HasKey(x => x.IngredientId).HasRequired(x => x.GenerateNutritionMeal)
            //    .WithMany(x => x.CustomUserNutritionIngredientDescriptions);
            //modelBuilder.Entity<CustomUserNutritionIngredientDescription>().HasRequired(c => c.Ingredient)
            //    .WithRequiredPrincipal(c => c.CustomUserNutritionIngredientDescription);
            base.OnModelCreating(modelBuilder);
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        public static Context Create()
        {
            return new Context();
        }
    }
}
