﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL.Infrastructure.Repository;
using Fitness.Data.Identity;
using Fitness.Data.Infrastructure.Repository;

namespace Fitness.DAL.Infrastructure
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        private Context _dbContext;

        public IHealthyFoodBlockRepository HealthyFoodBlockRepository { get; set; }
        public IHealthyFoodRepository HealthyFoodRepository { get; set; }
        public INutritionMealBlockRepository NutrtionBlockRepository { get; set; }
        public INutritionMealRepository NutritionMealRepository { get; set; }
        public IIngredientRepository IngredientRepository { get; set; }
        public IIngredientDescriptionRepository IngredientDescriptionRepository { get; set; }
        public IMineralRepository MineralRepository { get; set; }
        public IVitaminRepository VitaminRepository { get; set; }
        public IWorkoutBlockRepository WorkoutCategoryRepository { get; set; }
        public IWorkoutRepository WorkoutRepository { get; set; }
        public IWorkoutTypeRepository WorkoutTypeRepository { get; set; }
        public IMeasureRepository MeasureRepository { get; set; }
        public IShoppingRepository ShoppingListRepository { get; set; }
        public IShoppingNutritionRepository ShoppingNutritionRepository { get; set; }
        //public IPickmealsRepository PickmealsRepository { get; set; }
        public IErrorRepository ErrorRepository { get; set; }

        public IFitnessProgressRepository FitnessProgressRepository { get; set; }
        public IGenerateNutritionMealRepository GenerateNutritionMealRepository { get; set; }

        public UnitOfWork()
        {}

        public UnitOfWork(IHealthyFoodBlockRepository healthyFoodBlockRepository,
            IHealthyFoodRepository healthyFoodRepository,
            INutritionMealBlockRepository nutrtionBlockRepository,
            INutritionMealRepository nutritionMealRepository,
            IIngredientRepository ingredientRepository,
            IIngredientDescriptionRepository ingredientDescriptionRepository,
            IMineralRepository mineralRepository,
            IVitaminRepository vitaminRepository,
            IWorkoutBlockRepository workoutBlockRepository,
            IWorkoutRepository workoutRepository,
            IWorkoutTypeRepository workoutTypeRepository,
            IShoppingRepository shoppingListRepository,
            //IPickmealsRepository pickmealsRepository,
            IErrorRepository errorRepository,
            IShoppingNutritionRepository shoppingNutritionRepository,
            IGenerateNutritionMealRepository generateNutritionMealRepository,
            IMeasureRepository measureRepository,
            IFitnessProgressRepository fitnessProgressRepository,
            Context context)
        {
            this._dbContext = context;

            GenerateNutritionMealRepository = generateNutritionMealRepository;
            HealthyFoodBlockRepository = healthyFoodBlockRepository;
            HealthyFoodRepository = healthyFoodRepository;
            NutrtionBlockRepository = nutrtionBlockRepository;
            NutritionMealRepository = nutritionMealRepository;
            IngredientRepository = ingredientRepository;
            IngredientDescriptionRepository = ingredientDescriptionRepository;
            MineralRepository = mineralRepository;
            VitaminRepository = vitaminRepository;
            WorkoutCategoryRepository = workoutBlockRepository;
            WorkoutRepository = workoutRepository;
            WorkoutTypeRepository = workoutTypeRepository;
            ShoppingListRepository = shoppingListRepository;
            //PickmealsRepository = pickmealsRepository;
            ErrorRepository = errorRepository;
            ShoppingNutritionRepository = shoppingNutritionRepository;
            MeasureRepository = measureRepository;
            FitnessProgressRepository = fitnessProgressRepository;
        }


        public void OffValidation()
        {
            _dbContext.Configuration.AutoDetectChangesEnabled = false;
            _dbContext.Configuration.ValidateOnSaveEnabled = false;
        }
        public void OnValidation()
        {
            _dbContext.Configuration.AutoDetectChangesEnabled = true;
            _dbContext.Configuration.ValidateOnSaveEnabled = true;
        }
        public Context DbContext
        {

            get { return _dbContext ?? new Context(); }
        }

        private AppUserManager _userManager;
        private AppRoleManager _roleManager;

        public AppUserManager UserManager
        {
            get { return _userManager ?? new AppUserManager(new AppUserStore(DbContext)); }
            set { _userManager = value; }
        }

        public AppRoleManager RoleManager
        {
            get { return _roleManager ?? new AppRoleManager(new AppRoleStore(DbContext)); }
            set { _roleManager = value; }
        }

        // UserManager = new AppUserManager(new AppUserStore(DbContext));
        //    RoleManager = new AppRoleManager(new AppRoleStore(DbContext));

        public int SaveChanges()
        {
            return DbContext.SaveChanges();
        }

        protected override void DisposeCore()
        {
            _dbContext.Dispose();
            base.DisposeCore();
        }
    }
}
