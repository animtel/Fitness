﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL;
using Fitness.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Fitness.Data.Identity
{
    public class AppUserManager : UserManager<FitnessUser>
    {
        public AppUserManager(AppUserStore store)
            : base(store)
        {
            this.UserValidator = new UserValidator<FitnessUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 5,
                RequireDigit = false,
                RequireUppercase = false,
                RequireNonLetterOrDigit = false,
                RequireLowercase = false
            };

            UserLockoutEnabledByDefault = true;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            MaxFailedAccessAttemptsBeforeLockout = 5;

        }

        public IEnumerable<NutritionMeal> PickMeals(string userId)
        {
            return (Store as AppUserStore)?.Pickmeals(userId);

        }
        public void WeekExpiresMealPlan(string userId)
        {

            (Store as AppUserStore)?.WeekExpiresMealPlanUpdate(userId);

        }

        public bool Pickmeal(string userId, Guid mealId, bool picked)
        {
            (Store as AppUserStore)?.Pickmeal(userId, mealId, picked);
            return picked;
        }

    }
}
