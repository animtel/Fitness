﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL;
using Fitness.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Fitness.Data.Identity
{
    public class AppRoleManager : RoleManager<AppRole>
    {
        public AppRoleManager(AppRoleStore store) : base(store)
        {}
    }
}
