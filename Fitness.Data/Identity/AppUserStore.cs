﻿using Fitness.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.DAL;
using Microsoft.AspNet.Identity;

namespace Fitness.Data.Identity
{
    public class AppUserStore : UserStore<FitnessUser>
    {
        public AppUserStore(DbContext context) : base(context)
        {}

        public AppUserStore()
        {}

        //public IQueryable<NutritionMeal> Pickmeals(string userId) {

        //    return Context.Set<NutritionMeal>().Where(e=>e.UsersLike.FirstOrDefault().Id == userId);
        //}
        public IEnumerable<NutritionMeal> Pickmeals(string userId)
        {
            var result = Context.Set<FitnessUser>()
                .Include(x=>x.Pickmeals.Select(r=>r.NutritionMealCategory))
                .Include(x => x.Pickmeals.Select(qw=>qw.NutritionIngredientDescriptions.Select(me=>me.Measure)))
                .Include(x => x.Pickmeals.Select(er=>er.NutritionIngredientDescriptions.Select(e=>e.Ingredient))).SingleOrDefault(x=>x.Id == userId).Pickmeals.ToList();

            return result;
        }


        public void WeekExpiresMealPlanUpdate(string userId)
        {
            var user = Context.Set<FitnessUser>().Single(x => x.Id == userId);

            user.WeekExpiresMealPlan = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(DateTime.UtcNow,
                CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);

            Context.Entry(user).State = EntityState.Modified;
        }

        public void Pickmeal(string userId, Guid mealId, bool picked)
        {
            var set = Context.Set<FitnessUser>().Include(x => x.Pickmeals).Single(x => x.Id == userId).Pickmeals;

            if(picked)
                set.Add(Context.Set<NutritionMeal>().Find(mealId));
            else
            {
                set.Remove(set.FirstOrDefault(x => x.Id == mealId));
            }
            
        }
    }
}
