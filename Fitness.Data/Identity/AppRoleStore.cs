﻿using Fitness.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitness.Data.Identity
{
    public class AppRoleStore : RoleStore<AppRole>
    {
        public AppRoleStore(DbContext context) : base(context)
        { }

        public AppRoleStore()
        {}
    }
}
