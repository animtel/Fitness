namespace Fitness.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomNutritionIngredientDescriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        GenerateNutritionMealId = c.Guid(nullable: false),
                        MeasureId = c.Guid(),
                        MeasureType = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GenerateNutritionMeals", t => t.GenerateNutritionMealId, cascadeDelete: true)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.Measures", t => t.MeasureId)
                .Index(t => t.IngredientId)
                .Index(t => t.GenerateNutritionMealId)
                .Index(t => t.MeasureId);
            
            CreateTable(
                "dbo.GenerateNutritionMeals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Calories = c.Double(nullable: false),
                        DayOfWeek = c.Int(nullable: false),
                        FitnessUserId = c.String(maxLength: 128),
                        CreditTo = c.String(),
                        Premium = c.Boolean(nullable: false),
                        Name = c.String(),
                        Yield = c.Int(nullable: false),
                        ImageName = c.String(),
                        PrepTime = c.Int(nullable: false),
                        CookTime = c.Int(nullable: false),
                        Eat = c.Boolean(nullable: false),
                        NutritionMealCategory = c.Int(nullable: false),
                        NutritionMealId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUserId)
                .Index(t => t.FitnessUserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Age = c.Int(nullable: false),
                        Height = c.Double(nullable: false),
                        HeightMeasuareUnit = c.Int(nullable: false),
                        Weight = c.Double(nullable: false),
                        WeightMeasuareUnit = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                        Goal = c.Int(nullable: false),
                        ActivityLvl = c.Int(nullable: false),
                        WeekExpiresMealPlan = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.NutritionMeals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreditTo = c.String(),
                        Directions = c.String(),
                        Premium = c.Boolean(nullable: false),
                        Name = c.String(),
                        Yield = c.Int(nullable: false),
                        ImageName = c.String(),
                        PrepTime = c.Int(nullable: false),
                        CookTime = c.Int(nullable: false),
                        NutritionMealCategoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NutritionMealCategories", t => t.NutritionMealCategoryId, cascadeDelete: true)
                .Index(t => t.NutritionMealCategoryId);
            
            CreateTable(
                "dbo.NutritionIngredientDescriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        NutritionMealId = c.Guid(),
                        MeasureId = c.Guid(),
                        MeasureType = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.Measures", t => t.MeasureId)
                .ForeignKey("dbo.NutritionMeals", t => t.NutritionMealId)
                .Index(t => t.IngredientId)
                .Index(t => t.NutritionMealId)
                .Index(t => t.MeasureId);
            
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Calories = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fats = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Polyunsaturatedfattyacids = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Monounsaturatedfattyacids = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaturatedFat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sodium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Potassium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Carbohydrates = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Dietaryfiber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sugar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cholesterol = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Proteins = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Minerals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Ingredient_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id)
                .Index(t => t.Ingredient_Id);
            
            CreateTable(
                "dbo.Vitamins",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ingredient_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id)
                .Index(t => t.Ingredient_Id);
            
            CreateTable(
                "dbo.Measures",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MeasureName = c.String(),
                        EqualsGram = c.Double(nullable: false),
                        Abbreviation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NutritionMealCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ImageName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ShoppingItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        Count = c.Int(nullable: false),
                        QuantitativeValue = c.Int(nullable: false),
                        FitnessUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUserId)
                .Index(t => t.IngredientId)
                .Index(t => t.FitnessUserId);
            
            CreateTable(
                "dbo.ShoppingNutritionItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        Count = c.Int(nullable: false),
                        QuantitativeValue = c.Int(nullable: false),
                        FitnessUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUserId)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .Index(t => t.IngredientId)
                .Index(t => t.FitnessUserId);
            
            CreateTable(
                "dbo.FitnessProgressWeeks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FitnessUserId = c.String(maxLength: 128),
                        Title = c.String(),
                        WeekNumber = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                        UrlImage = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUserId)
                .Index(t => t.FitnessUserId);
            
            CreateTable(
                "dbo.DifficultyLevels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DifficultName = c.String(),
                        Sets = c.Int(nullable: false),
                        Repo = c.String(),
                        Rest = c.String(),
                        Temp = c.String(),
                        Exercise_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercises", t => t.Exercise_Id)
                .Index(t => t.Exercise_Id);
            
            CreateTable(
                "dbo.Errors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Message = c.String(),
                        StackTrace = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Exercises",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ExerciseName = c.String(),
                        YouTubeLink = c.String(),
                        Workout_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Workouts", t => t.Workout_Id)
                .Index(t => t.Workout_Id);
            
            CreateTable(
                "dbo.HealthyFoodCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HealthyFoods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreditTo = c.String(),
                        Premium = c.Boolean(nullable: false),
                        Name = c.String(),
                        Yield = c.Int(nullable: false),
                        ImageName = c.String(),
                        PrepTime = c.Int(nullable: false),
                        CookTime = c.Int(nullable: false),
                        HealthyFoodDirections = c.String(),
                        HealthyFoodCategoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HealthyFoodCategories", t => t.HealthyFoodCategoryId, cascadeDelete: true)
                .Index(t => t.HealthyFoodCategoryId);
            
            CreateTable(
                "dbo.IngredientDescriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IngredientId = c.Guid(nullable: false),
                        HealthyFoodId = c.Guid(nullable: false),
                        MeasureId = c.Guid(),
                        MeasureType = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HealthyFoods", t => t.HealthyFoodId, cascadeDelete: true)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.Measures", t => t.MeasureId)
                .Index(t => t.IngredientId)
                .Index(t => t.HealthyFoodId)
                .Index(t => t.MeasureId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.WorkoutCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        PreviewImageName = c.String(),
                        ScreenImageName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Workouts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        StatusWorkout = c.String(),
                        WorkoutTimeAbc = c.Time(nullable: false, precision: 7),
                        WorkoutDescription = c.String(),
                        Premium = c.Boolean(nullable: false),
                        WorkoutTypeId = c.Guid(nullable: false),
                        WorkoutCategory_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkoutTypes", t => t.WorkoutTypeId, cascadeDelete: true)
                .ForeignKey("dbo.WorkoutCategories", t => t.WorkoutCategory_Id)
                .Index(t => t.WorkoutTypeId)
                .Index(t => t.WorkoutCategory_Id);
            
            CreateTable(
                "dbo.WorkoutTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NutritionMealFitnessUsers",
                c => new
                    {
                        NutritionMeal_Id = c.Guid(nullable: false),
                        FitnessUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.NutritionMeal_Id, t.FitnessUser_Id })
                .ForeignKey("dbo.NutritionMeals", t => t.NutritionMeal_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.FitnessUser_Id, cascadeDelete: true)
                .Index(t => t.NutritionMeal_Id)
                .Index(t => t.FitnessUser_Id);
            
            CreateTable(
                "dbo.WorkoutTypeWorkoutCategories",
                c => new
                    {
                        WorkoutType_Id = c.Guid(nullable: false),
                        WorkoutCategory_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkoutType_Id, t.WorkoutCategory_Id })
                .ForeignKey("dbo.WorkoutTypes", t => t.WorkoutType_Id, cascadeDelete: true)
                .ForeignKey("dbo.WorkoutCategories", t => t.WorkoutCategory_Id, cascadeDelete: true)
                .Index(t => t.WorkoutType_Id)
                .Index(t => t.WorkoutCategory_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workouts", "WorkoutCategory_Id", "dbo.WorkoutCategories");
            DropForeignKey("dbo.Workouts", "WorkoutTypeId", "dbo.WorkoutTypes");
            DropForeignKey("dbo.WorkoutTypeWorkoutCategories", "WorkoutCategory_Id", "dbo.WorkoutCategories");
            DropForeignKey("dbo.WorkoutTypeWorkoutCategories", "WorkoutType_Id", "dbo.WorkoutTypes");
            DropForeignKey("dbo.Exercises", "Workout_Id", "dbo.Workouts");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.IngredientDescriptions", "MeasureId", "dbo.Measures");
            DropForeignKey("dbo.IngredientDescriptions", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.IngredientDescriptions", "HealthyFoodId", "dbo.HealthyFoods");
            DropForeignKey("dbo.HealthyFoods", "HealthyFoodCategoryId", "dbo.HealthyFoodCategories");
            DropForeignKey("dbo.DifficultyLevels", "Exercise_Id", "dbo.Exercises");
            DropForeignKey("dbo.CustomNutritionIngredientDescriptions", "MeasureId", "dbo.Measures");
            DropForeignKey("dbo.CustomNutritionIngredientDescriptions", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.FitnessProgressWeeks", "FitnessUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppingNutritionItems", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.ShoppingNutritionItems", "FitnessUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppingItems", "FitnessUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppingItems", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.NutritionMealFitnessUsers", "FitnessUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.NutritionMealFitnessUsers", "NutritionMeal_Id", "dbo.NutritionMeals");
            DropForeignKey("dbo.NutritionMeals", "NutritionMealCategoryId", "dbo.NutritionMealCategories");
            DropForeignKey("dbo.NutritionIngredientDescriptions", "NutritionMealId", "dbo.NutritionMeals");
            DropForeignKey("dbo.NutritionIngredientDescriptions", "MeasureId", "dbo.Measures");
            DropForeignKey("dbo.NutritionIngredientDescriptions", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.Vitamins", "Ingredient_Id", "dbo.Ingredients");
            DropForeignKey("dbo.Minerals", "Ingredient_Id", "dbo.Ingredients");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.GenerateNutritionMeals", "FitnessUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomNutritionIngredientDescriptions", "GenerateNutritionMealId", "dbo.GenerateNutritionMeals");
            DropIndex("dbo.WorkoutTypeWorkoutCategories", new[] { "WorkoutCategory_Id" });
            DropIndex("dbo.WorkoutTypeWorkoutCategories", new[] { "WorkoutType_Id" });
            DropIndex("dbo.NutritionMealFitnessUsers", new[] { "FitnessUser_Id" });
            DropIndex("dbo.NutritionMealFitnessUsers", new[] { "NutritionMeal_Id" });
            DropIndex("dbo.Workouts", new[] { "WorkoutCategory_Id" });
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.IngredientDescriptions", new[] { "MeasureId" });
            DropIndex("dbo.IngredientDescriptions", new[] { "HealthyFoodId" });
            DropIndex("dbo.IngredientDescriptions", new[] { "IngredientId" });
            DropIndex("dbo.HealthyFoods", new[] { "HealthyFoodCategoryId" });
            DropIndex("dbo.Exercises", new[] { "Workout_Id" });
            DropIndex("dbo.DifficultyLevels", new[] { "Exercise_Id" });
            DropIndex("dbo.FitnessProgressWeeks", new[] { "FitnessUserId" });
            DropIndex("dbo.ShoppingNutritionItems", new[] { "FitnessUserId" });
            DropIndex("dbo.ShoppingNutritionItems", new[] { "IngredientId" });
            DropIndex("dbo.ShoppingItems", new[] { "FitnessUserId" });
            DropIndex("dbo.ShoppingItems", new[] { "IngredientId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.Vitamins", new[] { "Ingredient_Id" });
            DropIndex("dbo.Minerals", new[] { "Ingredient_Id" });
            DropIndex("dbo.NutritionIngredientDescriptions", new[] { "MeasureId" });
            DropIndex("dbo.NutritionIngredientDescriptions", new[] { "NutritionMealId" });
            DropIndex("dbo.NutritionIngredientDescriptions", new[] { "IngredientId" });
            DropIndex("dbo.NutritionMeals", new[] { "NutritionMealCategoryId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.GenerateNutritionMeals", new[] { "FitnessUserId" });
            DropIndex("dbo.CustomNutritionIngredientDescriptions", new[] { "MeasureId" });
            DropIndex("dbo.CustomNutritionIngredientDescriptions", new[] { "GenerateNutritionMealId" });
            DropIndex("dbo.CustomNutritionIngredientDescriptions", new[] { "IngredientId" });
            DropTable("dbo.WorkoutTypeWorkoutCategories");
            DropTable("dbo.NutritionMealFitnessUsers");
            DropTable("dbo.WorkoutTypes");
            DropTable("dbo.Workouts");
            DropTable("dbo.WorkoutCategories");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.IngredientDescriptions");
            DropTable("dbo.HealthyFoods");
            DropTable("dbo.HealthyFoodCategories");
            DropTable("dbo.Exercises");
            DropTable("dbo.Errors");
            DropTable("dbo.DifficultyLevels");
            DropTable("dbo.FitnessProgressWeeks");
            DropTable("dbo.ShoppingNutritionItems");
            DropTable("dbo.ShoppingItems");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.NutritionMealCategories");
            DropTable("dbo.Measures");
            DropTable("dbo.Vitamins");
            DropTable("dbo.Minerals");
            DropTable("dbo.Ingredients");
            DropTable("dbo.NutritionIngredientDescriptions");
            DropTable("dbo.NutritionMeals");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.GenerateNutritionMeals");
            DropTable("dbo.CustomNutritionIngredientDescriptions");
        }
    }
}
