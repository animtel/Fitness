﻿using System;

namespace Fitness.Exceptions
{
    public class DomainLogicException : Exception
    {
        public DomainLogicException(string message)
                : base(message)
        {}        
    }
}