﻿using System;
using System.Collections.Generic;
using Fitness.BLL.DTO.Identity;
namespace Fitness.Exceptions.BusinessLoginExceptions
{
    public class IdentityException : DomainLogicException
    {
        public IdentityException(string message) : base(message)
        {
        }

        public static IdentityException IdentityRegisterFailure(
            RegisterDTO dto, IEnumerable<string> errors)
        {
            var message = $"Failure register user {dto.Email}"; 
            
            var err = errors != null ? String.Join(";", errors) : " " ; 

            return new IdentityException(
                message + err
            );
        }

        public static IdentityException IdentityRegisterUserIsExist(RegisterDTO dto)
        {
            return new IdentityException(
                $"Register user {dto.Email} is exist"
            );
        }
    }
}