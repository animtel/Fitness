﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Fitness.Entities;

namespace Fitness.Test.Mocks
{
    class CategoryMocks
    {
        private List<NutritionMealCategory> categories = new List<NutritionMealCategory>()
        {
            new NutritionMealCategory() { Name = "Dinner" },
            new NutritionMealCategory() { Name = "Breakfast" },
            new NutritionMealCategory() { Name = "Supper" }
        };

        public List<NutritionIngredientDescription> GetIngredients()
        {
            List<NutritionIngredientDescription> ingredients = new List<NutritionIngredientDescription>();
            Random r = new Random();
            int next = r.Next(1, 10);

            for (int i = 0; i < next; i++)
            {
                var temp = new Ingredient() {Calories = 10, Name = Guid.NewGuid().ToString("N") + " Ingredient"};
                var multipleIngredients = new NutritionIngredientDescription(){Ingredient = temp, Count = r.Next(1, 5)}; 
                ingredients.Add(multipleIngredients);
            }

            return ingredients;
        }

        public List<NutritionMeal> GetMeals()
        {
            //List<NutritionMeal> meals = new List<NutritionMeal>();
            //Random r = new Random();
            //int next = r.Next(1, 10);

            //for (int i = 0; i < next; i++)
            //{
            //    var temp = new NutritionMeal()
            //    {
            //        Name = Guid.NewGuid().ToString("N") + " Meal",
            //        MultipleIngredients = GetIngredients()
            //    };
            //    meals.Add(temp);
            //}
            //return meals;

            List<NutritionMeal> meals = new List<NutritionMeal>();

            var temp1 = new NutritionMeal()
            {
                Name = 1.ToString(),
                NutritionIngredientDescriptions = GetIngredients()
            };
            var temp2 = new NutritionMeal()
            {
                Name = 2.ToString(),
                NutritionIngredientDescriptions = GetIngredients()
            };
            var temp3 = new NutritionMeal()
            {
                Name = 3.ToString(),
                NutritionIngredientDescriptions = GetIngredients()
            };

            meals.Add(temp1);
            meals.Add(temp2);
            meals.Add(temp3);

            return meals;
        }

        public List<NutritionMealCategory> GetCategories()
        {
            foreach (var category in categories)
            {

                category.NutritionMeals= GetMeals();
            }

            return categories;
        }
    }
}
