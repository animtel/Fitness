﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Fitness.Entities;
using Fitness.Test.Mocks;
using NUnit.Framework;

namespace Fitness.Test
{
    [TestFixture]
    public class UnitTest1
    {

        public IEnumerable<IEnumerable<T>> CartesianProduct<T>(
            IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };
            return sequences.Aggregate(
                emptyProduct,
                (accumulator, sequence) =>
                    from accseq in accumulator
                    from item in sequence
                    select accseq.Concat(new[] { item }));
        }

        public List<NutritionMeal> ClosestTo(List<List<NutritionMeal>> collection, int target)
        {
            List<NutritionMeal> closest = null;
            var minDiffrence = int.MaxValue;

            foreach (var element in collection)
            {
                var diff = Math.Abs(element
                    .Sum(x => x.NutritionIngredientDescriptions.Sum(l => l.Count * l.Ingredient.Calories)
                ) - target);

                if (minDiffrence > diff)
                {
                    minDiffrence = (int)diff;
                    closest = element;
                }
            }

            return closest;
        }


        [Test]
        public void TestMethod1()
        {
            CategoryMocks mocks = new CategoryMocks();
            var temp = mocks.GetCategories();
            var count = CartesianProduct<NutritionMeal>(from i in temp select i.NutritionMeals).ToList();
            Assert.AreEqual(27, count.Count);
            var res = ClosestTo(count.Select(x => x.ToList()).ToList(), 160);
        }
    }
}
