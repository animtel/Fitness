﻿import { AppSettings } from './../app-settings';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { FileUploader } from "ng2-file-upload";
import { Category } from "../models/category";
import { CommonCategory } from "../common/common-category";
import { Ng2ImgMaxService } from "ng2-img-max/dist/ng2-img-max";

@Component({
  selector: 'app-healthy-category',
  templateUrl: 'healthy-category.component.html',
  styles: [`
  .image-zone{
    height:240px;
  }
  `]
})

export class HealthyCategoryComponent extends CommonCategory implements OnInit {

  host: string = AppSettings.serverUrl;
  mealCategory: Category;
  image: File;
  uploader: FileUploader = new FileUploader(
    {
      method: 'POST',
      url: AppSettings.serverUrl + `healthyfood/category/createimage/`,
      itemAlias: 'image'
    }
  );
  allCategories: Array<Category>;
  constructor(protected http: Http,private ng2ImgMaxService: Ng2ImgMaxService) {
    super('healthyfood/category', http);
    this.createUrl += '/create';
    this.getUrl += '/getall';
    this.updateUrl += '/update';
    this.deleteUrl += '/delete';
  }
  ngOnInit() {
    this.mealCategory = {
      id: "",
      imageUrl: "",
      name: ""
    };
    this.allCategories = [{
      id: "",
      imageUrl: "",
      name: ""
    }];

    this.getCategories();

    this.uploader.onCompleteItem = (item, response, status, header) => {
      if (status === 200) {
        var image = response.replace('"', '').replace('"', '');
        console.log(image);
        this.setImage(image);

      }
    }
  }
  select(category: Category): void {
    this.mealCategory = category;
    console.log(category);
  }
  uploadImageByCategory(cat: Category): void {

    this.ng2ImgMaxService.resize([this.uploader.queue[0]._file], 500, 1000).subscribe((result)=>{
      this.uploader.queue[0]._file = result;
      this.uploader.options.url = AppSettings.serverUrl + `healthyfood/category/createimage/${this.mealCategory.name}`;
      console.log(this.uploader.options.url);
      this.uploader.uploadAll();
    }, error => {
      //something went wrong 
      //use result.compressedFile or handle specific error cases individually
      console.log("Error file resize");
   });

  }
  private setImage(image: string): void {
    this.mealCategory.imageUrl = `${image}`;
  }
  getCategories() {
    super.getObservable().subscribe(res => this.allCategories = res.json());

  }
  update() {
    this.getCategories();
  }
  createCategory(): void {
    console.log(this.mealCategory);
    super.createObservable(this.mealCategory).subscribe();
  }
  updateCategory(data): void {
    console.log(data);
    super.updateObservable(data).subscribe();
  }
  deleteCategory(id: string): void {
    super.deleteObservable(id).subscribe();
  }

}
