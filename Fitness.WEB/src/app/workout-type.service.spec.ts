import { TestBed, inject } from '@angular/core/testing';

import { WorkoutTypeService } from './workout-type.service';

describe('WorkoutTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkoutTypeService]
    });
  });

  it('should be created', inject([WorkoutTypeService], (service: WorkoutTypeService) => {
    expect(service).toBeTruthy();
  }));
});
