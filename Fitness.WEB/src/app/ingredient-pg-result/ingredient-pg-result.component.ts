import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PagerService } from "../services/pager.service";
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import * as _ from 'underscore';
import { AppSettings } from "../app-settings";
import { HttpHelper } from "../common/http-helper.common";
import { IngredientService } from '../ingredient.service';
import { ExtentedIngredient } from '../models/ingredient';

@Component({
  selector: 'app-ingredient-pg-result',
  templateUrl: './ingredient-pg-result.component.html',
  styleUrls: ['./ingredient-pg-result.component.css']
})
export class IngredientPgResultComponent implements OnInit {
  private _name: string = '';
  private page:number = 0;

  @Output() editIngredient: EventEmitter<string>;
  constructor(
    private pagerService: PagerService,
    private ingredientService: IngredientService) 
    {
      this.editIngredient = new EventEmitter<string>();
    }



   @Input()
   set name(name: string) {
     
     this._name = (name && name.trim());
   }
  
   get name(): string { return this._name; }

    //  // array of all items to be paged
    private allItems: Array<any>;
    
    range: any[];
    //  // pager object
    pager: any = {};
    //  // paged items
    pagedItems: Array<any>;


    init(){
      this.ingredientService.getAll().subscribe((data:any) => {
        this.pager = data;
        this.range = Array.from(new Array(data.totalPages),(val,index)=>index);
        this.pager.totalPages = data.totalPages;
        this.pagedItems = data.items;
        console.info(this.pager);
      }, err => {console.log("Error " +  err)});
    }
     ngOnInit() {

      this.init();

     }

     navigate() {
      this.page += 1;
      this.setPage(this.page);
     }
     navigatePrew() {
      this.page -= 1;
      this.setPage(this.page);
     }
     
     setPage(page: number) {
      if(page !== undefined){
        this.page = page;
      }
      this.ingredientService.setSearchUrl(`ingredient/search/${this.page}/9/${this._name}`);
      this.init();
      //  this.ingredientService.init();
     };

     edit(ingredient:any){
      this.editIngredient.emit(ingredient);
       //console.log(ingredient, event);
     }
    //  setPage(page: number) {
    //      if (page < 0 || page > this.pager.totalPages) {
    //          return;
    //      }
  
    //      // get pager object from service
    //      this.pager = this.pagerService.getPager(this.allItems.length, page);
  
    //      // get current page of items
    //      this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
         
    //  }
}
