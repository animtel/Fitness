import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientPgResultComponent } from './ingredient-pg-result.component';

describe('IngredientPgResultComponent', () => {
  let component: IngredientPgResultComponent;
  let fixture: ComponentFixture<IngredientPgResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientPgResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientPgResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
