﻿import { Http } from '@angular/http';
import { Meal } from './../models/meal';
// import { foods } from './../mocks/foods-mocks';
import { Component, OnInit } from '@angular/core';
import { AppSettings } from './../app-settings';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
})
export class ResultComponent implements OnInit {

  constructor(private http: Http) {
  }

  foods: Meal[];

  ngOnInit() {
    this.getAllFood();
  }

  getAllFood() {
    this.http.get(AppSettings.serverUrl + 'nutrition/meal/getall').subscribe(res => {
      console.log(res.json());
      this.foods = res.json();
    });
  }
}
