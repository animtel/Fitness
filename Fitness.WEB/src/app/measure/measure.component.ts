import { Component, OnInit } from '@angular/core';
import { MeasureService } from '../measure.service';
import { Measure } from '../models/measure';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.component.html',
  styleUrls: ['./measure.component.css']
})
export class MeasureComponent implements OnInit {

  createMode:boolean=true;
  measure:Measure;
  measures:Measure[];
  constructor(private MeasureService:MeasureService) { }

  ngOnInit() {

    this.measure = {
      EqualsGram:1,
      id:undefined,
      measureName:undefined
    };

    this.MeasureService.getall().subscribe(result=>{
      this.measures = result as Measure[];
      console.log("Measures load ",this.measures);
    }, error=>{
      console.log("Error load ",error);
    })
  }
  deleteMeasure(id){
    this.MeasureService.delete(id).subscribe((result) => { 
      //this.measures.push(this.measure);
    
      for (var index = 0; index < this.measures.length; index++) {
        if (this.measures[index].id === id) {
          this.measures.splice(index, 1);
        }
      }
    console.log("Deleted " + id,result);
    }, error => {
      console.log("Error delete ",error );
    });
  }
  createMeasure() {
     
    this.MeasureService.create(this.measure).subscribe((result) => { 
      this.measures.push(this.measure);
    console.log("Ok ",this.measure);
    }, error => {
      console.log("Error ",error );
    });
  }

}
