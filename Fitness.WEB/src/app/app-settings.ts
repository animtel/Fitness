﻿import { IngredientComponent } from './ingredient/ingredient.component';
import { HealthyFoodEditorComponent } from './healthy-food-editor/healthy-food-editor.component';
import { HealthyFoodResultComponent } from './healthy-food-result/healthy-food-result.component';
import { NutritionResultComponent } from './nutrition-result/nutrition-result.component';
import { CanActivate, Routes } from '@angular/router/';
import { AuthGuardService } from './auth/auth-guard.service';
import { IdentityComponent } from './auth/identity/identity.component';
import { NutritionCategoryComponent } from './nutrition-category/nutrition-category.component';
import { HealthyCategoryComponent } from './healthy-category/healthy-category.component';
import { Component } from '@angular/core';
import { ResultComponent } from './result/result.component';
import { NutritionEditorComponent } from "./nutrition-editor/nutrition-editor.component";
import { WorkoutResultComponent } from './workout-result/workout-result.component';
import { WorkoutAddComponent } from './workout-add/workout-add.component';
import { MeasureComponent } from './measure/measure.component';



export class AppSettings {

    static routes:Routes = [{
        path: 'nutrition-result',
        component: NutritionResultComponent
    },
    {
        path: 'healthy-result',
        component: HealthyFoodResultComponent
    },  {
        path: 'nutrition-editor',
        component: NutritionEditorComponent
    },
    {
        path: 'nutrition-editor/:id',
        component: NutritionEditorComponent
    },
     {
        path: 'healthy-editor',
        component: HealthyFoodEditorComponent
    },
    {
        path: 'add-healthy-category',
        component: HealthyCategoryComponent
    },
     {
        path: 'add-nutrition-category',
        component: NutritionCategoryComponent,
    },
    {
        path: 'identity',
        component: IdentityComponent
    },
     {
        path: 'ingredient',
        component: IngredientComponent
    },
    {
        path: 'ingredient/:id',
        component: IngredientComponent
    },
    {
        path: 'measures',
        component: MeasureComponent
    },

    
    {
       path: 'workout-add',
       component: WorkoutAddComponent
   },
     {
        path: '**',
        redirectTo: '/identity',
        pathMatch: 'full'
    }
];

    //static serverUrl = `http://localhost:61171/`;
    //static serverUrl = 'http://localhost:59999/';
    static serverUrl = 'http://duxteamfitnessapplication.azurewebsites.net/';
    static loginUrl = AppSettings.serverUrl + 'Token';
    static registerUrl = AppSettings.serverUrl + 'api/Account/Register';
    static GRANT_TYPE = 'password';

    // Common component htmlvc
    static categoryComponentHtml =  '../common-html/common-category.html';
}
