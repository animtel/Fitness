import { AppSettings } from './../app-settings';
import { tokenNotExpired } from 'angular2-jwt';
import { IdentityService } from './identity.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public identity: IdentityService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      console.log(localStorage.getItem('token'));
      if (localStorage.getItem('token')) {
        return true;
      }

      this.router.navigate([ AppSettings.serverUrl + '/signin']);
      return false;
    }
}
