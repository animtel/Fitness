import { IdentityService } from './../identity.service';
import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http/http";
import { AppSettings } from "../../app-settings";
import { Router } from '@angular/router';
import { AlertService } from '../../alert.service';

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['./identity.component.css']
})
export class IdentityComponent implements OnInit {
  registerMode:boolean;
  user: any;
  registerUser: any;

   constructor(private identity: IdentityService,private router: Router,private alertService: AlertService) { }   
   ngOnInit() {
    // this.http.post(AppSettings.serverUrl + 'api/Account/ExternalLogins?returnUrl=%2F&generateState=true',null).map(res=>res.json()).subscribe(
    //   res => { console.log(res); },
    //   err => err
    // );
    
     if (this.identity.isAuth()) {
       this.alertService.success("Redirect in 3 sec...")
       setTimeout(() => {
         this.router.navigate(['/nutrition-result']);
       }, 3000);
     }
    this.user =  {
      username: 'TestTest@gmail.com',
      password: 'Test1234',
    };
    this.registerUser = {
      email: '',
      password: ''
    };
  }

  signin(): void {
    console.log(this.user);

    
    this.identity.signin(this.user.username, this.user.password)
      .subscribe(()=>{
        this.router.navigate(['/nutrition-result']);
      });
  }

  register(): void {
    console.log(this.registerUser);
    this.identity.register(this.registerUser.email, this.registerUser.password)
      .subscribe();
   }

   isAuth() {
     alert('Authenticated ' + this.identity.isAuth());
   }
}
