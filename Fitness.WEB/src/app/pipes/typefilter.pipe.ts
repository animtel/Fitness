import { Pipe , PipeTransform} from '@angular/core';


@Pipe({ name: 'measureType'})
export class MeasureType implements PipeTransform{

  transform(value:number):string {
    return value === 1 ? "pcs.": "gr.";
  }
}