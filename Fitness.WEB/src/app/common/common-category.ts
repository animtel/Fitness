import { Category } from './../models/category';
import { Http } from '@angular/http';
import { AppSettings } from './../app-settings';
import { OnInit } from '@angular/core';
import { HttpHelper } from './http-helper.common';
import { FileUploader } from "ng2-file-upload";

export class CommonCategory extends HttpHelper {

   constructor(protected prefix: string, protected http: Http) {
     super(prefix, http);
   }

}
