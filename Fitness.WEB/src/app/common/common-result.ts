import { HttpHelper } from './http-helper.common';
import { Http } from '@angular/http';
import { Meal } from './../models/meal';
// import { foods } from './../mocks/foods-mocks';
import { Component, OnInit } from '@angular/core';
import { AppSettings } from './../app-settings';


export class CommonResult extends HttpHelper implements OnInit {

  meals: Meal[];

  constructor(protected prefix: string, protected http: Http) {
    super(prefix, http);
  }

  ngOnInit() {
    this.getObservable().subscribe(res => {this.meals = res.json(); console.log(res.json()); });
  }

  deleteFood(index) {
    this.meals.splice(index, 1);
  }

}
