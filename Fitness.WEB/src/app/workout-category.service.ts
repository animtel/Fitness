import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHelper } from './common/http-helper.common';
import { Observable } from 'rxjs/Observable';
import { WorkoutCategory } from './models/Workout/workoutcategory';

@Injectable()
export class WorkoutCategoryService extends HttpHelper{

  constructor(protected http: Http) { 
    super('workout-category', http);
    this.getUrl += '/all';

  }

  getall():Observable<Array<WorkoutCategory>>{
    
        return super.getObservable().map(res => res.json());
    
      }


}
