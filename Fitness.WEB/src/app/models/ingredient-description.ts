import { ExtentedIngredient} from "./ingredient";
import { Measure } from "./measure";


export interface NutritionIngredientDescription {
    nutritionIngredient: ExtentedIngredient;
    measureType: number;
    measure: Measure;
    measureTypeabbreviation:string;    
    measureId:string;
    count: number;
    check: boolean;
}

export interface IngredientHealthyFoodDescription {
    ingredient: ExtentedIngredient;
    measureType: number;
    measureTypeabbreviation:string;
    count: number;
    check: boolean;
}
export class IngredientHealthyFoodDescriptionFactory {
    public static initDescriptionFood(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): IngredientHealthyFoodDescription {
        return {
            ingredient: ingredient,
            measureType: measureType,
            measureTypeabbreviation:"gr",
            count: count,
            check:true
        };
    }

    public static initNutritionDescriptionMeal(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): NutritionIngredientDescription {
        return {
            nutritionIngredient: ingredient,
            measureType: measureType,
            measure:undefined,
            measureId:undefined,
            count: count,
            check:true,
            measureTypeabbreviation:""
        };
    }
    public static initNutritionDescriptionMealArray(name: string, measureType:number, count:number, ingredient: ExtentedIngredient): Array<NutritionIngredientDescription> {
        return [{
            nutritionIngredient: ingredient,
            measureType: measureType,
            count: count,
            check:true,
            measureTypeabbreviation:"",
            measure:undefined,
            measureId:undefined
        }];
    }
}
export interface IngredientNutritionMealDescription {
    nutritionIngredient : ExtentedIngredient;
    measureType: number;
    measureId: string;
    count: number;
    id: string;
}
