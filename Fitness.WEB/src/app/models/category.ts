import { Meal } from './meal';

export interface Category {
    id:string;
    name: string;
    imageUrl: string;
}
export interface ExtCategory extends Category {
    id:string;
    name: string;
    food: Meal[];
}

export interface NutritionCategory {
    id:string;
    name: string;
    imageUrl: string;
    nutritionMeals: Meal[];

}
