
import { IngredientHealthyFoodDescription } from "./ingredient-description";

export interface Food {
    id:string;
    premium:boolean;
    name?: string|undefined; 
    yield: number;
    directions? : string;
    prepTime: number;
    cookTime: number;
    mealCategory: string;
    image: File; 
    imageName:string;

    creditto:string;
}
export interface HealthyFoodGrouped {
    group:string;
    arr:any[];
}
export interface HealthyFood extends Food {
    
   ingredientDescriptions: Array<IngredientHealthyFoodDescription>;
};

export class HealthyFoodFactory {
    public static initHealthyFood(): HealthyFood {
        return {
            creditto:"",
            premium: false,
            id: "",
            name: "", 
            yield: 0,
            directions : "",
            prepTime: 0,
            cookTime: 0,
            ingredientDescriptions: [{
                count:0,
                measureTypeabbreviation:"",
                ingredient:{
                    id: '',
                    name: '',
                    calories: 0,
                    fats: 0,
                    polyunsaturatedfattyacids: 0,
                    monounsaturatedfattyacids: 0,
                    saturatedFat: 0,
                    sodium: 0,
                    potassium: 0,
                    carbohydrates: 0,
                    dietaryfiber: 0,
                    sugar: 0,
                    cholesterol: 0,
                    proteins: 0
                },
                measureType:0,
                check:true
            }],
            mealCategory: "",
            image: null,
            imageName: ""
        };
    }
}