import { WorkoutType } from "./Workout/workouttype";

export interface Workout {
    id:string;
    name: string;
    workoutTimeAbc: string;
    workoutDescription: string;
    workoutTypeId: string;
    workoutType: WorkoutType;
    premium: boolean;
}
