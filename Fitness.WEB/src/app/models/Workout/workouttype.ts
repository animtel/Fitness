import { WorkoutCategory } from "./workoutcategory";

export interface WorkoutType {
    id:string;
    type: string;
    workouts:any[];    
}