﻿import { CommonCategory } from './../common/common-category';
import { AppSettings } from './../app-settings';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { NutritionCategory } from "../models/category";
import { FileUploader } from "ng2-file-upload";
import { Ng2ImgMaxService } from "ng2-img-max/dist/ng2-img-max";

@Component({
  selector: 'app-nutrition-category',
  templateUrl: './nutrition-category.component.html',
})
export class NutritionCategoryComponent extends CommonCategory {
  
  constructor(protected http: Http,private ng2ImgMaxService: Ng2ImgMaxService) {
    super('nutritioncategories', http);

    // this.createUrl += '/create';
    this.getUrl += '/getall';
    // this.updateUrl += '/update';
    // this.deleteUrl += '/delete';
  }

  host: string = AppSettings.serverUrl;
  nutritionCategory: NutritionCategory;
  image: File;
  uploader: FileUploader = new FileUploader(
    {
      method: 'POST',
      url: AppSettings.serverUrl + `nutritionmeal/category/createimage/`,
      itemAlias: 'image'
    }
  );
  allCategories: Array<NutritionCategory>;
  ngOnInit() {
    this.nutritionCategory = {
      id: "",
      imageUrl: "",
      name: "",
      nutritionMeals: null
    };
    this.allCategories = [{
      id: "",
      imageUrl: "",
      name: "",
      nutritionMeals: null
    }];

    this.getCategories();

    this.uploader.onCompleteItem = (item, response, status, header) => {
      if (status === 200) {
        var image = response.replace('"', '').replace('"', '');
        console.log(image);
        this.setImage(image);

      }
    }
  }
  select(category: NutritionCategory): void {
    this.nutritionCategory = category;
    console.log(category);
  }
  uploadImageByCategory(cat: NutritionCategory): void {

    this.ng2ImgMaxService.resize([this.uploader.queue[0]._file], 500, 1000).subscribe((result)=>{
      this.uploader.queue[0]._file = result;
      this.uploader.options.url = AppSettings.serverUrl + `nutritionmeal/category/createimage/${this.nutritionCategory.name}`;
      console.log(this.uploader.options.url);
      this.uploader.uploadAll();
    }, error => {
      //something went wrong 
      //use result.compressedFile or handle specific error cases individually
      console.log("Error file resize");
   });

  }
  private setImage(image: string): void {
    this.nutritionCategory.imageUrl = `${image}`;
  }
  getCategories() {
    super.getObservable().subscribe(res => this.allCategories = res.json());

  }
  update() {
    this.getCategories();
  }
  createCategory(): void {
    console.log(this.nutritionCategory);
    super.createObservable({ name: this.nutritionCategory }).subscribe();
  }
  updateCategory(data): void {
    console.log(data);
    super.updateObservable(data).subscribe();
  }
  deleteCategory(id: string): void {
    super.deleteObservable(id).subscribe();
  }
}
