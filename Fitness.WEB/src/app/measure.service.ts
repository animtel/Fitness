import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHelper } from './common/http-helper.common';
import { Measure } from './models/measure';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MeasureService extends HttpHelper{

  constructor(protected http: Http) {
    super('measures', http);
    this.getUrl += '/getall';
    this.createUrl += '/create';
    this.deleteUrl += '/delete';
  }
  getall(): Observable<Measure[]> {
    return super.getObservable().map(res => res.json());
  }
  create(measure:Measure):Observable<Measure>{
    return super.createObservable(measure);
  }
  delete(measureid:string):Observable<Measure>{
    return super.deleteObservable(measureid);
  }
}
