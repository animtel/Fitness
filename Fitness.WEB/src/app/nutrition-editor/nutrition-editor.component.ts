import { AppSettings } from './../app-settings';
import { Http, RequestOptions } from '@angular/http';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpHelper } from '../common/http-helper.common';
import { HealthyFood, HealthyFoodFactory } from '../models/healthy-food';
import { IngredientHealthyFoodDescription, IngredientNutritionMealDescription, IngredientHealthyFoodDescriptionFactory } from '../models/ingredient-description';
import { FileUploader } from "ng2-file-upload";
import { Observable } from 'rxjs/Observable';
// import { foods } from '../mocks/foods-mocks';
import { DndModule } from "ng2-dnd";
import { Ingredient, ExtentedIngredient, IngredientFactory } from "../models/ingredient";
import { Meal, MealFactory } from "../models/meal";
import { Category, NutritionCategory } from "../models/category";
import { NutritionResultComponent } from "../nutrition-result/nutrition-result.component";
import { ActivatedRoute, Params } from "@angular/router";
import { AlertService } from '../alert.service';
import { MeasureService } from '../measure.service';
import { Measure } from '../models/measure';

export interface FoodVM {
  id: string;
  measureType: string;
  count: number;
}

@Component({
  selector: 'app-nutrition-editor',
  templateUrl: './nutrition-editor.component.html',

  styles: [`
  .list-group-item{
    
        float:left;
      }

  .valid-error {
    color:red;
  }

  `]
})
export class NutritionEditorComponent extends HttpHelper implements OnInit {


  measures: Measure[];
  createMode: boolean = true;
  editMode: boolean = false;
  categories: Array<NutritionCategory>;
  ingredients: Array<ExtentedIngredient>;
  meal: Meal;
  imageUrl: string;//TODO second image for Meal
  uploader: FileUploader = new FileUploader(
    {
      method: 'POST',
      url: AppSettings.serverUrl + `nutritionmeal/createimage`,
      itemAlias: 'image'
    }
  );
  constructor(protected http: Http, private route: ActivatedRoute, private alertService: AlertService, private MeasureService: MeasureService) {
    super('nutritionmeal', http);
    this.createUrl += '/create';
    this.updateUrl += '/update';
    this.deleteUrl += '/delete';
    this.getUrlById += '/getbyid/';
  }


  ngOnInit(): void {
    // this.getCategories();

    this.route.params.switchMap((params: Params) => {
      if (params['id'] === undefined) return;
      return this.getBydIdObservable(params['id']);
    }).map(x => x.json() as Meal).subscribe(meal => {
      this.createMode = false;
      this.editMode = true;
      this.meal = meal;
      this.setImage(meal.imageName);
      console.info(this.meal);
    },
      err => console.log("Error" + err));

    this.MeasureService.getall().subscribe(x => {

      this.measures = x as Measure[];
      console.log("Measure ", this.measures);
    }, error => {

      console.log(error);
    });
    // headerName:'Authorization', tokenName: 'Bearer ',tokenGetter: (() => localStorage.getItem('token')

    this.uploader.onCompleteItem = (item, response, status, header) => {
      if (status === 200) {
        var image = response.replace('"', '').replace('"', '');
        //this.imageUrl = `${AppSettings.serverUrl}${image}`;
        this.setImage(image);
      }
    }
    this.getCategories();
    this.meal = MealFactory.createMeal();
    console.log(this.meal);
    this.http.get(AppSettings.serverUrl + `ingredient/getall`, new RequestOptions({ headers: this.getAuthHeaders() })).subscribe(
      (res) => {
        console.log(res.json());
        this.ingredients = (res.json() as ExtentedIngredient[]);
      },
      err => console.log(err));

  }
  private setImage(image: string): void {
    this.imageUrl = `${AppSettings.serverUrl}${image}`;
    this.meal.imageName = image;
  }
  getCategories(): void {

    this.http.get(AppSettings.serverUrl + 'nutritioncategories/getall', new RequestOptions({ headers: this.getAuthHeaders() }))

      .subscribe(res => {
        this.categories = res.json();
        console.log(res.json());
      },
      err => err);
  }

  conversionToGram(ev,measureId){
    //console.log(ev);
      // description.count = description.count * this.measures.find(x=>x.id === description.measureId).EqualsGram;
      // console.log("convert after",description);
      // description.measureId = measureId;
      var measure = this.measures.find(x=>x.id == measureId);

      let value:number = ev.data ;
      console.log(measure);
      return measure.EqualsGram || 1 * value;


      // return count * measure.EqualsGram ;

  };

  addDescriptionToFood(): boolean {
    // let index = this.food.ingredientDescriptions.length-1;
    var descriptions = this.meal.nutritionIngredientDescriptions;
    if (descriptions === null) {
      this.meal.nutritionIngredientDescriptions = IngredientHealthyFoodDescriptionFactory.initNutritionDescriptionMealArray(undefined, 0, 0, IngredientFactory.initHealthyFood());
    }
    this.meal.nutritionIngredientDescriptions.push(IngredientHealthyFoodDescriptionFactory.initNutritionDescriptionMeal(undefined, 0, 0, IngredientFactory.initHealthyFood()));
    return true;
  }
  addToDescription($event: any, data: any) {
    let newProduct: ExtentedIngredient = $event.dragData;
    console.log("Data>>",data);
    if (data.nutritionIngredient.id !== newProduct.id) {
      data.nutritionIngredient = newProduct
    }
  }
  //  addIngredient() {
  //    this.ingredientDescriptions.push(JSON.parse(JSON.stringify(this.tempIngredient)));
  //    this.resetIngredient();
  //    console.log(this.ingredientDescriptions);
  //  }
  //  getCategories(): void {
  //   this.http.get(AppSettings.serverUrl + 'healthyfood/category/getall')
  //     .map(res => res.json())
  //     .subscribe(res => { console.log(res); this.categories = res; },
  //     err => err);
  // }
  //  resetIngredient () {
  //    this.tempIngredient = {
  //      count:0,
  //      id: ``,
  //      measureType: ``
  //    }
  //  }

  //  addDirection() {
  //    console.log(this.tempDirection);
  //    this.food.directions.push(this.tempDirection);
  //    console.log(this.food.directions);
  //  }

  //  deleteIngredient(index) {
  //    console.log(index);
  //    console.log(this.food.ingredientDescriptions.splice(index, 1));
  //  }

  saveMeal() {
    // this.meal.nutritionIngredientDescriptions.forEach((x,index)=>{
      
    //         console.log("description for food ",x);
    //         console.log("measure for save",this.measures);
    //         let findMealGram =  this.measures.find(re=>re.id === x.measureId);
    //         let findMealGramNumber =  this.measures.find(re=>re.id == x.measureId).EqualsGram || 1;
    //         console.log("findMealGram",findMealGram);
    //         x.count = findMealGramNumber * x.count;
    //         x.measureType = 1;
    //         x.measure = null;
    //             });
    this.updateObservable(this.meal).subscribe(x => {
      console.log("Saved", this.meal);

      this.alertService.success("Saved");
    },
      err => { this.alertService.error("Error update") })

  }
  addMeal() {

    // this.meal.nutritionIngredientDescriptions.forEach((x,index)=>{
      
    //         console.log("description for food ",x);
    //         console.log("measure for save",this.measures);
    //         let findMealGram =  this.measures.find(re=>re.id === x.measureId);
    //         let findMealGramNumber =  this.measures.find(re=>re.id == x.measureId).EqualsGram || 1;
    //         console.log("findMealGram",findMealGram);
    //         x.count = findMealGramNumber * x.count;
    //         x.measureType = 1;
    //         x.measure = null;
    //             });
    this.createObservable(this.meal).subscribe(
      res => {
        // console.log("Create", this.meal);
        this.alertService.success("Create");
        // this.meal = MealFactory.createMeal();
      }, err => {
        this.alertService.error("Error create");
      });
  }

}
