import { CommonCategory } from './../common/common-category';
import { Http } from '@angular/http';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { AppSettings } from "../app-settings";
import { Ingredient, ExtentedIngredient, IngredientFactory } from "../models/ingredient";
import { IngredientService } from '../ingredient.service';
import { AlertService } from '../alert.service';


@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent extends CommonCategory implements OnInit {


  createMode:boolean = false;
  editMode:boolean = false;
  ingred: ExtentedIngredient;
  constructor(protected http: Http, private ingredientService: IngredientService, private alertService:AlertService) {
      super('ingredient', http);
      this.createUrl += '/create';
      this.updateUrl += '/update';
      this.deleteUrl += '/delete';
      this.getUrl += '/getall';
  }

  Load(model:any){
    this.ingred = model;
    console.log(this.ingred);
    this.createMode = true;
    this.editMode = true;
  }

  ngOnInit() {
    this.ingred = IngredientFactory.initHealthyFood();
    //var ingr = this.ingredientService.getTemporaryStorage();
 

  }
  cleanIngredientForm():void {
    this.ingred = IngredientFactory.initHealthyFood();
    this.editMode = false;
    this.createMode = true;
    this.alertService.info("Cleaned");
  }
  showAddIngredientForm():void{
    if(!this.createMode)
      this.createMode = true;
      else{
        this.createMode = false;
      }
  }
  addIngredient() {
    //const url = AppSettings.serverUrl + `/ingredient/create`;
    this.createObservable(this.ingred).subscribe(
      res => {
        this.alertService.success("Added");
      }, err => this.alertService.error("Error when adding"));
  }
  editIngredient() {
    //const url = AppSettings.serverUrl + `/ingredient/update`;
    this.updateObservable(this.ingred).subscribe(
      res => {
        this.cleanIngredientForm();
        this.alertService.success("Saved");
      }, err => err => this.alertService.error("Error when saved"));
  }
  deleteIngredient() {
    //const url = AppSettings.serverUrl + `/ingredient/delete`;
    this.deleteObservable(this.ingred.id).subscribe(
      res => {
        this.cleanIngredientForm();
        this.alertService.success("Deleted");
      }, err => this.alertService.error("Error when deleted"));
  }
}
