﻿import { AppSettings } from './../app-settings';
import { Http, RequestOptions } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { HttpHelper } from '../common/http-helper.common';
import { HealthyFood, HealthyFoodFactory } from '../models/healthy-food';
import { IngredientHealthyFoodDescription, IngredientNutritionMealDescription } from '../models/ingredient-description';
import { FileUploader } from 'ng2-file-upload';
import { Observable } from 'rxjs/Observable';
// import { foods } from '../mocks/foods-mocks';
import { Ingredient, ExtentedIngredient, IngredientFactory } from "../models/ingredient";


export interface FoodVM {
  id: string;
  measureType: string;
  count: number;
  Ingredient: ExtentedIngredient;
}

@Component({
  selector: 'app-healthy-food-editor',
  templateUrl: './healthy-food-editor.component.html',
})
export class HealthyFoodEditorComponent extends HttpHelper implements OnInit {
  food: HealthyFood;
  ingredients: Array<ExtentedIngredient>;

  //tempDescriptionIngredients: Array<IngredientHealthyFoodDescription>;
  //tempIngredients: ExtentedIngredient[];
  //ingredientDescriptions: Array<FoodVM>;

  image: File;
  uploader: FileUploader = new FileUploader(
    { method: 'POST', itemAlias: 'image' });

  constructor(protected http: Http) {
    super('healthyfood/food', http);
    this.createUrl += '/create';
    this.getUrl += '/getall';
    this.updateUrl += '/update';
    this.deleteUrl += '/delete';
  }

  ngOnInit(): void {
    this.food = HealthyFoodFactory.initHealthyFood();
    this.ingredients = IngredientFactory.initHealthyFoodArray();

    let url = AppSettings.serverUrl + `ingredient/getall`;
    this.http.get(url).subscribe(
      (res) => {
        console.log(res.json());
        this.ingredients = (res.json() as Array<ExtentedIngredient>);
      },
      err => console.log(err));

  }

  // addIngredient() {
  //   this.ingredientDescriptions.push(JSON.parse(JSON.stringify(this.food.ingredientDescriptions)));
  //   this.resetIngredient();
  //   console.log(this.ingredientDescriptions);
  // }

  resetIngredient() {
    this.ingredients = [{
      id: '',
      name:'',
      calories: 0,
      fats: 0,
      polyunsaturatedfattyacids: 0,
      monounsaturatedfattyacids: 0,
      saturatedFat: 0,
      sodium: 0,
      potassium:0,
      carbohydrates: 0,
      dietaryfiber: 0,
      sugar: 0,
      cholesterol: 0,
      proteins: 0
    }]
  }

  // addDirection() {
  //   console.log(this.tempDirection);
  //   this.food.directions.push(this.tempDirection);
  //   console.log(this.food.directions);
  // }

  deleteIngredient(index) {
    console.log(index);
    console.log(this.food.ingredientDescriptions.splice(index, 1));
  }



  createFood() {
    this.createObservable(this.food).subscribe(
      res => {
        console.log(res.json());
        const url = AppSettings.serverUrl + `healthyfood/food/createimage/` + res.json();
        this.uploader.queue[0].url = url;
        this.uploader.uploadItem(this.uploader.queue[0]);
        // this.uploader.queue[0].upload();
      });
  }
}
