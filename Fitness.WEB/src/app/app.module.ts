﻿import { SafeHtml } from './pipes/safe-html.pipe';
import { MeasureType } from "./pipes/typefilter.pipe";
import { IdentityService } from './auth/identity.service';
import { Guard } from './app-auth';
import { AuthGuardService } from './auth/auth-guard.service';
import { AuthModule } from './auth/auth.module';
import { NutritionCategoryComponent } from './nutrition-category/nutrition-category.component';
import { HealthyCategoryComponent } from './healthy-category/healthy-category.component';

import { AppSettings } from './app-settings';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AppComponent } from './app.component';
import { ResultComponent } from './result/result.component';
import { NutritionResultComponent } from './nutrition-result/nutrition-result.component';
import { HealthyFoodResultComponent } from './healthy-food-result/healthy-food-result.component';
import { HealthyFoodEditorComponent } from './healthy-food-editor/healthy-food-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { IngredientComponent } from './ingredient/ingredient.component';

import { FileUploadModule } from 'ng2-file-upload';
import { NutritionEditorComponent } from "./nutrition-editor/nutrition-editor.component";

import {DndModule} from 'ng2-dnd';
import { IngredientPgResultComponent } from './ingredient-pg-result/ingredient-pg-result.component';
import { PagerService } from './services/index';
import { WorkoutAddComponent } from './workout-add/workout-add.component';
import { WorkoutResultComponent } from './workout-result/workout-result.component';
import { WorkoutServiceService } from './workout-service.service';
import { WorkoutCategoryService } from './workout-category.service';
import { WorkoutTypeService } from './workout-type.service';
import { IngredientService } from './ingredient.service';
import { AlertService } from './alert.service';
import { AlertComponent } from './alert/alert.component';
import { MeasureService } from './measure.service';
import { MeasureComponent } from './measure/measure.component';
@NgModule({
  declarations: [ 
    AppComponent,
    ResultComponent,
    HealthyCategoryComponent,
    NutritionCategoryComponent,
    NutritionResultComponent,
    HealthyFoodResultComponent,
    HealthyFoodEditorComponent,
    FileUploadComponent,
    IngredientComponent,
    NutritionEditorComponent,
    SafeHtml,
    MeasureType,
    IngredientPgResultComponent,
    WorkoutAddComponent,
    WorkoutResultComponent,
    AlertComponent,
    MeasureComponent,
    
],
  imports: [
    DndModule.forRoot(),
    BrowserModule,
    FileUploadModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(AppSettings.routes),
    AuthModule,
    Ng2ImgMaxModule,
  ],
  providers: [
    Guard,
    PagerService,WorkoutServiceService,
    WorkoutCategoryService,WorkoutTypeService,
    IngredientService,
    AlertService,
    MeasureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
