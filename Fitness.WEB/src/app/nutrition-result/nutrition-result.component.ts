import { CommonResult } from './../common/common-result';
import { Http } from '@angular/http';
import { ResultComponent } from './../result/result.component';
import { Component, OnInit } from '@angular/core';
import { Meal } from "../models/meal";
import { NutritionEditorComponent } from "../nutrition-editor/nutrition-editor.component";
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-nutrition-result',
  templateUrl: './../common-html/common-result.html',
  styleUrls: ['./nutrition-result.component.css']
})
export class NutritionResultComponent extends CommonResult {


  constructor(protected http: Http, private alertService: AlertService) {
    super('nutritionmeal', http);
    this.getUrl += '/getall';
    this.deleteUrl += '/delete';
  }

  deleteMeal(id:string, index){
    this.deleteObservable(id).subscribe(x=> {
    super.deleteFood(index);
      console.log("Delete ", id);

      this.alertService.success("Deleted");
    }, err => {
      this.alertService.error("Not Deleted");});
  }
}
