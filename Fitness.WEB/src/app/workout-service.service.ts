import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHelper } from './common/http-helper.common';
import { Workout } from './models/workout';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkoutServiceService extends HttpHelper {


  constructor(protected http: Http) {
    super('workout', http);
    this.createUrl += '/add';
    this.getUrl += '/getall';
    this.getUrlById += '/getbyid';
    this.updateUrl += '/update';
    this.deleteUrl += '/delete';
  }

  getall(): Observable<Workout[]> {
    return super.getObservable().map(res => res.json());
  }
  create(workout): Observable<Workout>{

    return super.createObservable(workout);

  }
  delete(id): Observable<Workout>{
    return super.deleteObservable(id);
  }
  updateWorkout(workout): Observable<Workout> {
    return super.updateObservable(workout);
  }
}
