import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpHelper } from './common/http-helper.common';
import { AppSettings } from './app-settings';
import { Observable } from 'rxjs/Observable';
import { ExtentedIngredient } from './models/ingredient';

@Injectable()
export class IngredientService extends HttpHelper {

  constructor(protected http: Http) {
    super('ingredient', http);
    this.searchUrl += `/search/0/9`;
  }

  getAll(): Observable<ExtentedIngredient[]> {
    // get dummy data
    return this.http.get(this.searchUrl)
      .map((response: Response) => response.json());
  }

  setSearchUrl(url: string) {
    this.searchUrl = AppSettings.serverUrl + url;
  }
}
