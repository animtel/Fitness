import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WorkoutServiceService } from '../workout-service.service';
import { Workout } from '../models/workout';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-workout-result',
  templateUrl: './workout-result.component.html',
  styleUrls: ['./workout-result.component.css']
})
export class WorkoutResultComponent implements OnInit {

  workouts: Array<Workout>;
  @Output() editWorkout: EventEmitter<Workout>;
  constructor(private workoutServiceService:WorkoutServiceService, private alertService:AlertService) { 
    this.editWorkout = new EventEmitter<Workout>();
  }

  ngOnInit() {
    this.workoutServiceService.getall().subscribe((workouts: Workout[])=> {
      this.workouts = workouts;
      this.alertService.success("Workouts load!")
    }, err => {this.alertService.warn("fail load")});
  }
  edit(any:Workout){
    this.editWorkout.emit(any);
  }
  delete(workoutId, index) {
    this.workoutServiceService.delete(workoutId).subscribe(r => {
      this.workouts.splice(index,1);
      this.alertService.success("Deleted");
      
    }, err => {
      this.alertService.warn("Failed deleted");
    });
  }

}
