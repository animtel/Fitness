import { Http } from '@angular/http';
import { CommonResult } from './../common/common-result';
import { Component, OnInit } from '@angular/core';
import { HttpHelper } from "../common/http-helper.common";
import { HealthyFood, HealthyFoodFactory, HealthyFoodGrouped } from "../models/healthy-food";
import { MeasureType } from "../pipes/typefilter.pipe";
import { DndModule } from "ng2-dnd";
import { AppSettings } from "../app-settings";
import { Ingredient, ExtentedIngredient, IngredientFactory } from "../models/ingredient";
import { Category } from "../models/category";
import { IngredientHealthyFoodDescriptionFactory, IngredientHealthyFoodDescription } from "../models/ingredient-description";
import { FileUploader } from "ng2-file-upload";
import { IngredientComponent } from "../ingredient/ingredient.component";
import { Measure } from '../models/measure';
import { MeasureService } from '../measure.service';

@Component({
  selector: 'app-healthy-food-result',
  templateUrl: './healthy-food-result.component.html',
  styles: [`

.label-custom{

  width: 100px;
}
  .card-title .list-group-item{

    float:left;
  }
  .list-group-item {
    height:100%;
    
  }
  .panel-body {
    padding: 15px;
  }
  .drag-panel {
content: "Ingredient";
  }
  .food .btn-group {
    
        display: flex; 
      }
    
  .food .btn-group button {

    width: 100%; 
  }


  `]
})
export class HealthyFoodResultComponent extends HttpHelper implements OnInit {

  createMode: boolean = true;
  editMode:boolean = false;
  imageUrl: string;
  image: File;
  uploader: FileUploader = new FileUploader(
    {
      method: 'POST',
      url: AppSettings.serverUrl + `healthyfood/food/createimage`,
      itemAlias: 'image'
    }
  );
  ingredientdescr: IngredientHealthyFoodDescription;
  food: HealthyFood;
  ingredients: Array<ExtentedIngredient>;
  categories: Array<Category>;
  host: string;
  spinner: boolean;
  foods: Array<HealthyFood>;
  measures: Measure[];

  groupedFoods: Array<HealthyFoodGrouped>;
  images: string[];

  constructor(protected http: Http, private MeasureService:MeasureService) {
    super('healthyfood/food', http);
    this.createUrl += '/create/';
    this.getUrl += '/getall';
    this.deleteUrl += '/delete';
    this.updateUrl += '/update';
  }
  private setImage(image:string):void {
    this.imageUrl = `${AppSettings.serverUrl}${image}`;
    this.food.imageName = image;
  }
  ngOnInit(): void {
    this.uploader.onCompleteItem = (item, response, status, header) => {
      if (status === 200) {
        var image = response.replace('"', '').replace('"', '');
        //this.imageUrl = `${AppSettings.serverUrl}${image}`;
        this.setImage(image);
      }
    }
    this.MeasureService.getall().subscribe(x=>{

      this.measures = x as Measure[];
      console.log(this.measures);
    }, error=>{

      console.log(error);
    })
    
    ;
    this.host = AppSettings.serverUrl;
    this.spinner = false;
    this.food = HealthyFoodFactory.initHealthyFood();

    this.getIngredients();
    this.getCategories();
    this.getFoods();
  }


  addDescriptionToFood(): boolean {
    // let index = this.food.ingredientDescriptions.length-1;
    this.ingredientdescr = IngredientHealthyFoodDescriptionFactory.initDescriptionFood(undefined, 0, 0, IngredientFactory.initHealthyFood());
    // IngredientHealthyFoodDescriptionFactory.initDescriptionFood(this.food.ingredientDescriptions[index].name, this.food.ingredientDescriptions[index].measureType, this.food.ingredientDescriptions[index].count, this.food.ingredientDescriptions[index].ingredient)
    this.food.ingredientDescriptions.push(this.ingredientdescr)
    return true;
  }

  getAllMeasures():void {
    
    }
  addToDescription($event: any, data: any) {
    let newProduct: ExtentedIngredient = $event.dragData;
    // for (let indx in this.food.ingredientDescriptions) {
    //   let ingredient = this.food.ingredientDescriptions[indx].ingredient;
    //console.log($event.index);
    //console.log($event. dragData);
    console.log(data);
    if (data.ingredient.id !== newProduct.id) {
      data.ingredient = newProduct
    }
    // this.food.ingredientDescriptions[indx].ingredient = newProduct;

    //   }
    // }
    // this.food.ingredientDescriptions.push(IngredientHealthyFoodDescriptionFactory.initDescriptionFood("TEST12346",666,999,newProduct));
    // console.log(this.food);
  }

  getIngredients(): void {

    this.rotateSpinner();
    this.http.get(AppSettings.serverUrl + 'ingredient/getall')
      .map(res => res.json())
      .subscribe(res => { console.log(res); this.ingredients = res; },
      err => err, () => this.spinner = this.stopSpinner());
  }
  getCategories(): void {
    this.rotateSpinner();
    this.http.get(AppSettings.serverUrl + 'healthyfood/category/getall')
      .map(res => res.json())
      .subscribe(res => { console.log(res); this.categories = res; },
      err => err, () => this.spinner = this.stopSpinner());
  }

  rotateSpinner(): boolean {
    return true;
  }

  stopSpinner(): boolean {
    return false;
  }

  waitResponse(observable): void {
    observable
      .map(res => res.json())
      .subscribe(res => res, err => err, () => this.spinner = this.stopSpinner());
  }
  groupBy(myArray:any) {

    var groups = {};
    for (var i = 0; i < myArray.length; i++) {
      var groupName = myArray[i].mealCategory;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      groups[groupName].push(myArray[i]);
    }
    myArray = [];
    for (let groupName in groups) {
      myArray.push({group: groupName, arr: groups[groupName]});
    }
    this.groupedFoods = myArray;

    // this.groupedFoods = arr.reduce(
    //   (sum, item) => {
    //   	const groupByVal = item[key];
    //     this.groupedFoods = sum.get(groupByVal) || [];
    //     this.groupedFoods.push(item);
    //   	return sum.set(groupByVal, this.groupedFoods);
    //     },
    //   new Map()
    //   );
}

  getFoods(): void {
    this.spinner = this.rotateSpinner();
    this.getObservable()
      .map(res => res.json())
      .subscribe(res => { this.foods = res; this.groupBy(this.foods); console.log(this.groupedFoods); this.images = res.images; },
      err => err, () => this.spinner = this.stopSpinner());
  }
  // getImage(index) {
  //   return this.images[index];
  // }
  getImage(index) {
    const url = AppSettings.serverUrl + `healthyfood/food/create/`;
    return this.images[index];
  }
  updateFood(data): void {
    this.spinner = this.rotateSpinner();
    this.waitResponse(this.updateObservable(data));
  }
  addHealthyFood() {
    const url = AppSettings.serverUrl + `healthyfood/food/create`;
    this.createObservable(this.food).subscribe(
      res => {

        this.foods.push(this.food);
        this.groupBy(this.foods);
        console.log(this.foods);
        console.log("Create");
        this.food = HealthyFoodFactory.initHealthyFood();
      }, err => console.log("ERRROR Create food"));
  }

  editFood(food: HealthyFood) {

    if (food !== null) {
      this.food = food;
    }
    this.setImage(food.imageName);
    console.log(food);
    this.editMode = true;
    this.createMode = false;
  }
  saveFood():void {
    this.updateFood(this.food);
        this.editMode = true;
        this.createMode = false;
      }
    
  deleteFood(id: string, name: string) {
    for (var index = 0; index < this.foods.length; index++) {
      if (this.foods[index].name === name) {
        this.foods.splice(index, 1);
      }
    }
    
    this.spinner = this.rotateSpinner();
    this.deleteObservable(id).subscribe();
    this.groupBy(this.foods);
  }

  deleteIngredientDescription(element:any) {
    for (var index = 0; index < this.food.ingredientDescriptions.length; index++) {
      if (this.food.ingredientDescriptions[index] === element) {
        this.food.ingredientDescriptions.splice(index, 1);
      }
    }
  }
}
